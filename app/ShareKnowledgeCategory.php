<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareKnowledgeCategory extends Model
{
    public $timestamps = true;

    protected $table = 'share_knowledge_categories';

    protected $fillable = [
        'id_user', 'name', 'slug'
    ];
}

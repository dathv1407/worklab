<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Register extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'registers';

    protected $fillable = [
        'id_open_schedule', 'student_name', 'email', 'phone', 'message', 'status'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class OpeningSchedule extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'opening_schedules';

    protected $fillable = [
        'id_course_detail', 'start_date', 'local', 'time', 'schedule', 'original_tuition', 'discount',
        'expire_date', 'note'
    ];
}

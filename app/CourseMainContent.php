<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CourseMainContent extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'course_main_contents';

    protected $fillable = [
        'id_course_detail','url_image_content' , 'title_content'
    ];
    protected function courseDetailContent() {
        return $this->hasMany(CourseDetailContent::class, 'id_course_main_content');
    }
}

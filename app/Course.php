<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Course extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'courses';

    protected $fillable = [
        'name'
    ];

    protected function courseDetail() {
        return $this->hasMany(CourseDetail::class, 'id_course');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareKnowledgeDetail extends Model
{
    public $timestamps = true;

    protected $table = 'share_knowledge_detail';

    protected $fillable = [
        'id_user', 'id_share_knowledge_category', 'headline', 'slug', 'url_img', 'summary', 'content'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'id_user');
    }
}

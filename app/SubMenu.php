<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SubMenu extends Model
{
    use Notifiable;
    public $timestamps = true;
    const DAO_TAO_CHUYEN_DE = 1;
    const LUYEN_THI_CHUNG_CHI = 2;
    protected $table = 'sub_menu';

    protected $fillable = [
        'id_menu_client', 'name', 'route'
    ];
}

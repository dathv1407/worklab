<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Message extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'messages';

    protected $fillable = [
        'name', 'email', 'phone', 'title', 'content', 'status'
    ];
}

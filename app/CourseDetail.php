<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CourseDetail extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'course_details';

    protected $fillable = [
        'id_course', 'name', 'url_homepage_avatar', 'url_image_top', 'image_information', 'information', 'note', 'teacher', 'document', 'number_student',
        'day_study', 'hour_study'
    ];

    protected function courseMainContent() {
        return $this->hasMany(CourseMainContent::class, 'id_course_detail');
    }
}

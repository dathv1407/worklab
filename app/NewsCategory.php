<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class NewsCategory extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'news_categories';

    protected $fillable = [
        'id_user', 'name', 'slug'
    ];

    protected function news() {
        return $this->hasMany(News::class, 'id_news_category');
    }
}

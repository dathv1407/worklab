<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CourseDetailContent extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'course_detail_contents';

    protected $fillable = [
        'id_course_main_content', 'main_content' , 'detail_content'
    ];
}

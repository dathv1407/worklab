<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class News extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'news';

    protected $fillable = [
        'id_user','id_news_category', 'name', 'headline', 'url_img', 'summary', 'content'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'id_user');
    }
}

<?php

namespace App\Http\Controllers\Client;

use App\Course;
use App\CourseDetail;
use App\CourseDetailContent;
use App\CourseMainContent;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRegisterRequest;
use App\Http\Requests\SendMessageRequest;
use App\MenuClient;
use App\Message;
use App\News;
use App\NewsCategory;
use App\OpeningSchedule;
use App\Register;
use App\ShareKnowledgeCategory;
use App\ShareKnowledgeDetail;
use App\SubMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends Controller
{

    protected $menuDaoTao;
    protected $menuCertification;

    public function __construct()
    {
        $this->menuDaoTao = MenuClient::find(1)->subMenu;
        $this->menuCertification = MenuClient::find(2)->subMenu;
    }

    public function index() {
        $daoTaoChuyenDe = SubMenu::where('id_menu_client', SubMenu::DAO_TAO_CHUYEN_DE)->get();
        $luyenThiChungChi = SubMenu::where('id_menu_client', SubMenu::LUYEN_THI_CHUNG_CHI)->get();
        $listIdCouseDaoTao = [];
        $listIdChungChi = [];
        foreach ($daoTaoChuyenDe as $item) {
            if(is_numeric( $item->route)) {
                array_push($listIdCouseDaoTao, $item->route);
            }
        }
        foreach ($luyenThiChungChi as $item2) {
            if(is_numeric( $item2->route)) {
                array_push($listIdChungChi, $item2->route);
            }
        }
        $listCourseDaoTao = CourseDetail::whereIn('id_course', $listIdCouseDaoTao)->get();
        $listDiscount = [];
        foreach ($listCourseDaoTao as $item) {
            $open = OpeningSchedule::where('id_course_detail', $item->id)
                    ->where('start_date', '>=', now())->first();
            if($open){
                $listDiscount[$item->id] = $open->discount;
            }else {
                $listDiscount[$item->id] = 0;
            }
        }
        $listCourseChungChi = CourseDetail::whereIn('id_course', $listIdChungChi)->get();
        $latestNews = News::orderBy('id', 'desc')->take(3)->get();
        $latestShareKnowledge = ShareKnowledgeDetail::orderBy('id', 'desc')->take(3)->get();
        return view('client-new.pages.home.index')->with([
            'listCourseDaoTao'          => $listCourseDaoTao,
            'listCourseChungChi'        => $listCourseChungChi,
            'latestNews'                => $latestNews,
            'latestShareKnowledge'      => $latestShareKnowledge,
            'listDiscount'              => $listDiscount
        ]);
    }

    public function daotaochuyende($id) {
        $name_page = SubMenu::DAO_TAO_CHUYEN_DE;
        if($id == 'dao-tao') {
            $daoTaoChuyenDe = SubMenu::where('id_menu_client', SubMenu::DAO_TAO_CHUYEN_DE)->get();
            $listIdCouseDaoTao = [];
            foreach ($daoTaoChuyenDe as $item) {
                if(is_numeric( $item->route)) {
                    array_push($listIdCouseDaoTao, $item->route);
                }
            }
            $listCourseDetail = CourseDetail::whereIn('id_course', $listIdCouseDaoTao)->get();
            $listCourse = Course::whereIn('id', $listIdCouseDaoTao)->get();
            $name_page = SubMenu::DAO_TAO_CHUYEN_DE;
        } else if($id == 'chung-chi') {
            $luyenThiChungChi = SubMenu::where('id_menu_client', SubMenu::LUYEN_THI_CHUNG_CHI)->get();
            $listIdChungChi = [];
            foreach ($luyenThiChungChi as $item2) {
                if(is_numeric( $item2->route)) {
                    array_push($listIdChungChi, $item2->route);
                }
            }
            $listCourseDetail = CourseDetail::whereIn('id_course', $listIdChungChi)->get();
            $listCourse = Course::whereIn('id', $listIdChungChi)->get();
            $name_page = SubMenu::LUYEN_THI_CHUNG_CHI;
        } else {
            $listCourseDetail = CourseDetail::where('id_course',$id)->get();
            $listCourse = Course::where('id',$id)->get();
        }
        $listOpen = [];
        foreach ($listCourseDetail as $item) {
            $open = OpeningSchedule::where('id_course_detail', $item->id)
                    ->where('start_date', '>=', now())->first();
            if($open){
                $listOpen[$item->id] = $open->discount;
            }else {
                $listOpen[$item->id] = 0;
            }
        }
        return view('client-new.pages.course.index')->with([
            'listCourseDetail' => $listCourseDetail,
            'courses' => $listCourse,
            'listDiscount' => $listOpen,
            'namePage' => $name_page,
            'title_page' => $name_page == 1 ? "Khóa học" : "Luyện thi chứng chỉ"
        ]);
    }

    public function introduce() {
        return view('client-new.pages.introduce.index');
        // return view('client/introduction')->with([
        //     'menuDaoTao' => $this->menuDaoTao,
        //     'menuCertification' => $this->menuCertification]);
    }

    public function detailCourse($id){

        $daoTaoChuyenDe = SubMenu::where('id_menu_client', SubMenu::DAO_TAO_CHUYEN_DE)->get();
        $listIdCouseDaoTao = [];
        foreach ($daoTaoChuyenDe as $item) {
            if(is_numeric( $item->route)) {
                array_push($listIdCouseDaoTao, $item->route);
            }
        }
        $listCourseDaoTao = CourseDetail::whereIn('id_course', $listIdCouseDaoTao)->where('id','<>',$id)->take(3)->get();

        $courseDetail = CourseDetail::find($id);
        $courseMainContent = $courseDetail->courseMainContent;
        $list_id_main_content = [];
        $listSchedule = OpeningSchedule::where('start_date', '>=', now())
                        ->where('id_course_detail', $id)
                        ->orderBy('start_date', 'desc')->get();
        foreach ($courseMainContent as $item) {
            array_push($list_id_main_content, $item->id);
        }
        $array_color = ['blue', 'green', 'orange', 'yellow', 'red', 'pink'];
        $courseDetailContent = CourseDetailContent::whereIn('id_course_main_content',$list_id_main_content)->get();
        return view('client-new.pages.course.course_detail')->with([
            'courseDetail'          => $courseDetail,
            'courseMainContent'     => $courseMainContent,
            'courseDetailContent'   => $courseDetailContent,
            'color'                 => $array_color,
            'listSchedule'          => $listSchedule,
            'dateEarliest'          => !$listSchedule->isEmpty() ?  $listSchedule[count($listSchedule) - 1]->start_date : date('Y-m-d'),
            'discount'              => !$listSchedule->isEmpty() ? $listSchedule[count($listSchedule) - 1]->discount : 0,
            'listCourseDaoTao'      => $listCourseDaoTao
        ]);
        /*return view('client/detail_course_client')->with([
            'courseDetail'          => $courseDetail,
            'courseMainContent'     => $courseMainContent,
            'courseDetailContent'   => $courseDetailContent,
            'color'                 => $array_color,
            'listSchedule'          => $listSchedule,
        ]);*/
    }

    public function lichKhaiGiang(){
        $courseDetail = CourseDetail::all();
        $listSchedule = OpeningSchedule::where('start_date', '>=', now())->orderBy('start_date', 'desc')->get();
        return view('client-new.pages.opening_schedule.index')->with([
            'courseDetail'          => $courseDetail,
            'listSchedule'          => $listSchedule,
            'menuDaoTao'            => $this->menuDaoTao,
            'menuCertification'     => $this->menuCertification
        ]);
    }

    public function contact() {
        return view('client-new.pages.contact.index');
    }

    public function courseRegister(CourseRegisterRequest $request) {
        $id = $request->input('id_open_schedule');
        $courseDetail = CourseDetail::find($id);
        Register::create([
            'id_open_schedule'  => $request->input('id_open_schedule'),
            'student_name'  => $request->input('student_name'),
            'email'         => $request->input('email'),
            'phone'         => $request->input('phone'),
            'message'       => $request->input('message'),
            'status'        => 0,
        ]);
        $request->session()->flash('status', 'Congratulations You has been registered successfully !');
        return redirect()->route('chi-tiet-khoa-hoc',['name' => Str::slug($courseDetail->name), 'id' => $id]);
    }

    public function sendMessage(SendMessageRequest $request) {
        Message::create([
            'name'  => $request->input('name'),
            'phone'  => $request->input('name'),
            'email'  => $request->input('email'),
            'title'  => $request->input('title'),
            'content'  => $request->input('content'),
            'status'  => 0,
        ]);
        $request->session()->flash('message', 'Send message success, Thank you for your contribution !');
        return redirect()->route('client.contact');
    }

    public function getListNewsLatest() {
        $listNews = News::paginate(10);
        $latestNews = News::orderBy('id', 'desc')->take(5)->get();
        return view('client-new.pages.news.index')->with([
            'latestNews'  => $latestNews,
            'listNews'     => $listNews
        ]);
    }

    public function getListNews($id) {
        $newsCategory = News::find($id);
        $nameCategory = NewsCategory::find($id);
        $listNews = News::where('id_news_category', $id)->paginate(10);
        $latestNews = News::where('id_news_category', $id)->orderBy('id', 'desc')->take(5)->get();
        return view('client-new.pages.news.news_category')->with([
            'latestNews'  => $latestNews,
            'listNews'     => $listNews,
            'newsCategory'     => $newsCategory,
            'title_page' => $nameCategory->name
        ]);
    }

    public function getDetailNews($id) {
        $detailNews = News::find($id);
        $latestNews = News::where('id_news_category', $detailNews->id_news_category)->orderBy('id', 'desc')->take(5)->get();
        return view('client-new.pages.news.news_detail')->with([
            'news_detail' => $detailNews,
            'latestNews' => $latestNews,
        ]);
    }

    public function getListKnowledgeLatest() {
        $listShareKnowledge = ShareKnowledgeDetail::paginate(10);
        $latestShareKnowledge = ShareKnowledgeDetail::orderBy('id', 'desc')->take(5)->get();
        return view('client-new.pages.share_knowledge.index')->with([
            'listShareKnowledge'        => $listShareKnowledge,
            'latestShareKnowledge'      => $latestShareKnowledge
        ]);
    }

    public function getListKnowledgeCategory($id) {
        $shareKnowledgeCategory = ShareKnowledgeCategory::find($id);
        $listShareKnowledge = ShareKnowledgeDetail::where('id_share_knowledge_category', $id)->paginate(10);
        $latestShareKnowledge = ShareKnowledgeDetail::where('id_share_knowledge_category', $id)->orderBy('id', 'desc')->take(5)->get();
        return view('client-new.pages.share_knowledge.share_knowledge_category')->with([
            'latestShareKnowledge'      => $latestShareKnowledge,
            'listShareKnowledge'        => $listShareKnowledge,
            'shareKnowledgeCategory'    => $shareKnowledgeCategory,
        ]);
    }

    public function getShareKnowledgeDetail($id) {
        $detailShareKnowledge = ShareKnowledgeDetail::find($id);
        $latestShareKnowledge = ShareKnowledgeDetail::where('id_share_knowledge_category', $detailShareKnowledge->id_share_knowledge_category)->orderBy('id', 'desc')->take(5)->get();
        return view('client-new.pages.share_knowledge.share_knowledge_detail')->with([
            'detailShareKnowledge' => $detailShareKnowledge,
            'latestShareKnowledge' => $latestShareKnowledge,
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditChuyenDeRequest;
use App\Http\Requests\SubMenuRequest;
use App\SubMenu;

class SubMenuController extends Controller
{
    public function createTheme(SubMenuRequest $request){
        SubMenu::create([
            'id_menu_client'    => 1,
            'name'              => $request->input('txtName'),
            'route'             => $request->input('txtRoute'),
        ]);
        return redirect('admin/chuyen-de-dao-tao');
    }

    public function createCertification(SubMenuRequest $request){
        SubMenu::create([
            'id_menu_client'    => 2,
            'name'              => $request->input('txtName'),
            'route'             => $request->input('txtRoute'),
        ]);
        return redirect('admin/luyen-chung-chi');
    }


    public function getViewEdit($id) {
        $item = SubMenu::find($id);
        return view('admin/menu_client/edit_chuyende')->with(['item' => $item]);
    }

    public function editChuyenDe(EditChuyenDeRequest $request, $id) {
        $item = SubMenu::find($id);
        $item->name = $request->input('txtName');
        $item->route = $request->input('txtRoute');
        $item->save();
        return redirect()->route('chuyen-de-dao-tao');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function listMessage() {
        $message = Message::all();
        return view('admin.course.list_message')->with([
            'listMessage' => $message
        ]);
    }

    public function messageDetail($id) {
        $message = Message::find($id);
        $message->status = 1;
        $message->save();
        return view('admin.course.message_detail')->with([
            'message' => $message
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MenuClient;
use Illuminate\Http\Request;

class MenuClientController extends Controller
{
    public function getTraining() {
        $list = MenuClient::find(1)->subMenu;
        return view('admin/menu_client/daotaochuyende')->with('list', $list);
    }

    public function getCertification() {
        $list = MenuClient::find(2)->subMenu;
        return view('admin/menu_client/luyenthichungchi')->with('list', $list);
    }
}

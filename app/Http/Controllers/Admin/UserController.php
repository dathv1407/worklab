<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateCreateUser;
use App\Http\Requests\validateLogin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    public function getUser() {
        $listUser = User::all();
        return View::make('admin/user/list_user')->with('listUser', $listUser);
    }


    public function createUser(ValidateCreateUser $request) {
        User::create([
            'name' => $request->input('txtUserName'),
            'email' => $request->input('txtEmail'),
            'password' => Hash::make($request->input('txtPassword')),
            'level' => $request->input('role'),
            'status' => $request->input('status')
        ]);
        return redirect('admin/list-user');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\CourseDetail;
use App\CourseDetailContent;
use App\CourseMainContent;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddCourseDetailContent;
use App\Http\Requests\AddCourseDetailContentRequest;
use App\Http\Requests\AddOpeningScheduleRequest;
use App\Http\Requests\CourseContentRequest;
use App\Http\Requests\CourseDetailRequest;
use App\Http\Requests\CourseRequest;
use App\Http\Requests\UpdateCourseDetailRequest;
use App\Http\Requests\UpdateCourseMainContentRequest;
use App\OpeningSchedule;
use App\Register;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller
{
    public function getCourse() {
        $list = Course::all();
        return view('admin/course/list_course')->with([
            'list_course' => $list
        ]);
    }

    public function createCourse(CourseRequest $request) {
        Course::create([
            'name' => $request->input('txtCourseName')
        ]);
        return redirect('admin/list-course');
    }

    public function getViewCourseDetail($id) {
        $course = Course::find($id);
        return view('admin/course/create_course_detail')->with([
            'course' => $course
        ]);
    }

    public function getListCourseDetail($id) {
        $course = Course::find($id);
        $listCourse = $course->courseDetail;
        return view('admin/course/list_course_detail')->with([
            'course' => $course,
            'list_course_detail' => $listCourse
        ]);
    }

    public function createCourseDetail(CourseDetailRequest $request) {
        if (!$request->hasFile('homepage_avatar')) {
            return false;
        }
        $path_homepage_avatar       = $request->file('homepage_avatar')->store('images',['disk' => 'public']);
        $path_image_top             = $request->file('image_top')->store('images',['disk' => 'public']);
        $path_image_information     = $request->file('image_information')->store('images',['disk' => 'public']);
        CourseDetail::create([
            'id_course'             => $request->input('course'),
            'name'                  => $request->input('courseName'),
            'url_homepage_avatar'   => 'uploads/'.$path_homepage_avatar,
            'url_image_top'         => 'uploads/'.$path_image_top,
            'image_information'     => 'uploads/'.$path_image_information,
            'information'           => trim($request->input('information')),
            'teacher'               => trim($request->input('teacher')),
            'note'                  => trim($request->input('note')),
            'document'              => trim($request->input('document')),
            'number_student'        => trim($request->input('number_student')),
            'day_study'             => trim($request->input('day_study')),
            'hour_study'            => trim($request->input('hour_study')),
        ]);
        return redirect()->route('course', ['id' => $request->input('course')]);
    }

    public function getViewEditCourseDetail($id) {
        $courseDetail = CourseDetail::find($id);
        return view('admin/course/edit_course_detail')->with([
            'courseDetail' => $courseDetail
        ]);
    }

    public function updateCourseDetail(UpdateCourseDetailRequest $request, $id) {
        $courseDetail = CourseDetail::find($id);
        if ($request->hasFile('homepage_avatar')) {
            $url_homepage_avatar = $request->file('homepage_avatar')->store('images',['disk' => 'public']);
            $courseDetail->url_homepage_avatar = 'uploads/'.$url_homepage_avatar;
        }
        if ($request->hasFile('image_top')) {
            $url_image_top = $request->file('image_top')->store('images',['disk' => 'public']);
            $courseDetail->url_image_top = 'uploads/'.$url_image_top;
        }
        if ($request->hasFile('image_information')) {
            $url_image_information = $request->file('image_information')->store('images',['disk' => 'public']);
            $courseDetail->image_information = 'uploads/'.$url_image_information;
        }

        $courseDetail->name         = $request->input('courseName');
        $courseDetail->information  = trim($request->input('information'));
        $courseDetail->teacher      = trim($request->input('teacher'));
        $courseDetail->note         = trim($request->input('note'));
        $courseDetail->document              = trim($request->input('document'));
        $courseDetail->number_student        = trim($request->input('number_student'));
        $courseDetail->day_study             = trim($request->input('day_study'));
        $courseDetail->hour_study            = trim($request->input('hour_study'));
        $courseDetail->save();
        return redirect()->route('course', ['id' => $courseDetail->id_course]);
    }

    public function getListMainContentCourseDetail($id_course_detail){
        $courseDetail = CourseDetail::find($id_course_detail);
        $courseMainContent = CourseMainContent::where('id_course_detail', $id_course_detail)->get();
        return view('admin/course/main_content_course')->with([
            'courseDetail' => $courseDetail,
            'courseMainContent' => $courseMainContent
        ]);
    }

    public function getViewCreateCourseMainContent($id_course_detail){
        $courseDetail = CourseDetail::find($id_course_detail);
        return view('admin/course/create_main_content_course')->with([
            'courseDetail' => $courseDetail
        ]);
    }

    public function createCourseMainContent(CourseContentRequest $request){
        $url_image_content = '';
        $id_course_detail = $request->input('id_course_detail');
        if ($request->hasFile('image_content')) {
            $url_image_content = $request->file('image_content')->store('images',['disk' => 'public']);
        }
        

        CourseMainContent::create([
            'id_course_detail' => $id_course_detail,
            'url_image_content' => 'uploads/'.$url_image_content,
            'title_content'     => $request->input('title_content')
        ]);
        return redirect()->route('list-main-content-course', ['id_course_detail' => $id_course_detail]);
    }

    public function getViewEditCourseMainContent($id_course_main_content){
        $courseMainContent = CourseMainContent::find($id_course_main_content);
        $courseDetail = CourseDetail::find($courseMainContent->id_course_detail);
        return view('admin/course/edit_course_main_content')->with([
            'courseMainContent' => $courseMainContent,
            'courseDetail'      => $courseDetail
        ]);
    }

    public function editCourseMainContent(UpdateCourseMainContentRequest $request){
        $id = $request->input('id_course_main_content');
        $courseMainContent = CourseMainContent::find($id);
        if ($request->hasFile('image_content')) {
            $url_image_content = $request->file('image_content')->store('images',['disk' => 'public']);
            $courseMainContent->url_image_content = 'uploads/'.$url_image_content;
        }
        $courseMainContent->title_content = $request->input('title_content');
        $courseMainContent->save();
        return redirect()->route('list-main-content-course', ['id_course_detail' => $courseMainContent->id_course_detail]);
    }

    public function getViewCourseDetailContent($id_course_main_content){
        $courseMainContent = CourseMainContent::find($id_course_main_content);
        $courseDetailContent = CourseDetailContent::where('id_course_main_content', $id_course_main_content)->get();
        return view('admin/course/list_course_detail_content')->with([
            'courseMainContent'     => $courseMainContent,
            'courseDetailContent'   => $courseDetailContent,
        ]);
    }

    public function getViewAddCourseDetailContent($id_course_main_content){
        $courseMainContent = CourseMainContent::find($id_course_main_content);
        $countDetailContent = CourseDetailContent::where('id_course_main_content', $id_course_main_content)->count();
        return view('admin/course/add_course_detail_content')->with([
            'courseMainContent'     => $courseMainContent,
            'countDetailContent'   => $countDetailContent,
        ]);
    }

    public function addCourseDetailContent(AddCourseDetailContentRequest $request){
        $id_course_main_content = $request->input('id_course_main_content');
        CourseDetailContent::create([
            'id_course_main_content' => $id_course_main_content,
            'main_content' => $request->input('main_content'),
            'detail_content' => $request->input('detail_content'),
        ]);
        return redirect()->route('course-detail-content', ['id_course_main_content' => $id_course_main_content]);
    }

    public function getViewEditCourseDetailContent($id_course_detail_content) {
        $courseDetailContent = CourseDetailContent::find($id_course_detail_content);
        $courseMainContent = CourseMainContent::find( $courseDetailContent->id_course_main_content);
        return view('admin/course/edit_course_detail_content')->with([
            'courseDetailContent'   => $courseDetailContent,
            'courseMainContent'     => $courseMainContent,
        ]);
    }

    public function editCourseDetailContent(AddCourseDetailContentRequest $request){
        $id = $request->input('id');
        $courseDetailContent = CourseDetailContent::find($id);
        $courseDetailContent->main_content = $request->input('main_content');
        $courseDetailContent->detail_content = $request->input('detail_content');
        $courseDetailContent->save();
        return redirect()->route('course-detail-content', ['id_course_main_content' => $courseDetailContent->id_course_main_content]);

    }

    public function getListOpeningSchedule($id_course_detail) {
        $id = $id_course_detail;
        $courseDetail = CourseDetail::find($id);
        $listSchedule = OpeningSchedule::where('id_course_detail', $id_course_detail)->orderBy('start_date', 'desc')->get();
        return view('admin/course/list_opening_schedule')->with([
            'listOpeningSchedule' =>  $listSchedule,
            'courseDetail'       =>  $courseDetail
        ]);
    }

    public function getViewAddOpeningSchedule($id_course_detail) {
        $id = $id_course_detail;
        $courseDetail = CourseDetail::find($id);
        return view('admin/course/add_opening_schedule')->with([
            'courseDetail'       =>  $courseDetail
        ]);
    }

    public function addOpeningSchedule(AddOpeningScheduleRequest $request){
        $id = $request->input('id_course_detail');
        OpeningSchedule::create([
            'id_course_detail'  => $id,
            'start_date'        => $request->input('start_date'),
            'local'             => $request->input('local'),
            'time'              => $request->input('time'),
            'schedule'          => $request->input('schedule'),
            'original_tuition'  => $request->input('original_tuition'),
            'discount'          => $request->input('discount'),
            'expire_date'       => $request->input('expire_date'),
            'note'              => $request->input('note'),
        ]);
        return redirect()->route('list-opening-schedule', ['id_course_detail' => $id]);
    }

    public function getViewEditOpeningSchedule($id_opening_schedule) {
        $openSchedule = OpeningSchedule::where('id', $id_opening_schedule)->first();
        return view('admin/course/edit_opening_schedule')->with([
            'item'       =>  $openSchedule
        ]);
    }

    public function editOpeningSchedule(AddOpeningScheduleRequest $request) {
        $id = $request->input('id');
        $openingSchedule = OpeningSchedule::find($id);
        $openingSchedule->start_date = $request->input('start_date');
        $openingSchedule->local = $request->input('local');
        $openingSchedule->time = $request->input('time');
        $openingSchedule->schedule = $request->input('schedule');
        $openingSchedule->original_tuition = $request->input('original_tuition');
        $openingSchedule->discount = $request->input('discount');
        $openingSchedule->expire_date = $request->input('expire_date');
        $openingSchedule->note = $request->input('note');
        $openingSchedule->save();

        return redirect()->route('list-opening-schedule', ['id_course_detail' => $id]);
    }

    public function listRegister($id){
        $openSchedule = OpeningSchedule::find($id);
        $courseDetail = CourseDetail::find($openSchedule->id_course_detail);
        $listRegister = Register::where('id_open_schedule', $id)->get();
        return view('admin.course.list_register')->with([
            'openSchedule' => $openSchedule,
            'courseDetail' => $courseDetail,
            'listRegister' => $listRegister
        ]);
    }

    public function getRegister($id) {
        $register = Register::find($id);
        $register->status = 1;
        $register->save();
        $openSchedule = OpeningSchedule::find($register->id_open_schedule);
        $courseDetail = CourseDetail::find($openSchedule->id_course_detail);

        return view('admin.course.register_infor')->with([
            'openSchedule' => $openSchedule,
            'courseDetail' => $courseDetail,
            'register'     => $register
        ]);
    }
}

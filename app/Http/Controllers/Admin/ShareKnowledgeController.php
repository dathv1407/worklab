<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddShareKnowledgeDetailRequest;
use App\Http\Requests\AddShareKnowledgeRequest;
use App\Http\Requests\UpdateShareKnowledgeRequest;
use App\ShareKnowledgeCategory;
use App\ShareKnowledgeDetail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ShareKnowledgeController extends Controller
{
    public function getShareKnowledgeCategory() {
        $listShares = ShareKnowledgeCategory::all();
        return view('admin.share_knowledge.list_share_knowledge_category',compact('listShares'));
    }

    public function editShareKnowledgeCategory($id) {
        $shareKnowledge = ShareKnowledgeCategory::find($id);
        return view('admin.share_knowledge.edit_share_knowledge_category',compact('shareKnowledge'));
    }

    public function createShareKnowledgeCategory(AddShareKnowledgeRequest $request) {
        ShareKnowledgeCategory::create([
            'id_user' => Auth::id(),
            'name'      => $request->input('txtName'),
            'slug'      => Str::slug($request->input('txtName'))
        ]);
        return redirect()->route('list-share-knowledge-category');
    }

    public function deleteShareKnowledgeCategory($id) {

    }

    public function getShareKnowledgeDetail($id) {
        $knowledgeCategory = ShareKnowledgeCategory::find($id);
        $listDetail = ShareKnowledgeDetail::where('id_share_knowledge_category', $id)->with('user')->get();
        return view('admin.share_knowledge.list_share_knowledge_detail')->with([
            'knowledgeCategory' => $knowledgeCategory,
            'listDetail'        => $listDetail
        ]);
    }

    public function getViewCreateShareKnowledgeDetail($id) {
        $knowledgeCategory = ShareKnowledgeCategory::find($id);
        return view('admin.share_knowledge.create_share_knowledge_detail')->with([
            'knowledgeCategory' => $knowledgeCategory
        ]);
    }

    public function createShareKnowledgeDetail(AddShareKnowledgeDetailRequest $request) {
        $id_share_knowledge_category = $request->input('id_share_knowledge_category');
        $url_image      = $request->file('url_img')->store('images',['disk' => 'public']);
        ShareKnowledgeDetail::create([
            'id_user'                       => Auth::id(),
            'id_share_knowledge_category'   => $request->input('id_share_knowledge_category'),
            'headline'                      => $request->input('headline'),
            'slug'                          => Str::slug($request->input('headline')),
            'url_img'                       => 'uploads/'.$url_image,
            'summary'                       => $request->input('summary'),
            'content'                       => $request->input('content'),
        ]);
        return redirect()->route('list-share-knowledge-detail', ['id' => $id_share_knowledge_category]);
    }

    public function viewUpdateShareKnowledgeDetail($id) {
        $knowledgeDetail = ShareKnowledgeDetail::find($id);
        return view('admin.share_knowledge.edit_share_knowledge_detail')->with([
            'knowledgeDetail' => $knowledgeDetail
        ]);
    }

    public function updateShareKnowledgeDetail(UpdateShareKnowledgeRequest $request) {
        $id = $request->input('id');
        $knowledgeDetail = ShareKnowledgeDetail::find($id);
        if ($request->hasFile('url_img')) {
            $url_img = $request->file('url_img')->store('images',['disk' => 'public']);
            $knowledgeDetail->url_img = 'uploads/'.$url_img;
        }
        $knowledgeDetail->headline = $request->input('headline');
        $knowledgeDetail->summary = $request->input('summary');
        $knowledgeDetail->content = $request->input('content');
        $knowledgeDetail->save();
        return redirect()->route('list-share-knowledge-detail', ['id' => $knowledgeDetail->id_share_knowledge_category]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsCategoryRequest;
use App\Http\Requests\UpdateNewCategory;
use App\Http\Requests\UpdateNewCategoryRequest;
use App\Http\Requests\RegisterNewRequest;
use App\Http\Requests\UpdateNewsDetailRequest;
use App\News;
use App\NewsCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;

class NewsController extends Controller
{
    public function getNewsCategory(){
        $list = NewsCategory::all();
        return view('admin/news/list_news_category')->with([
            'list_news_categories' => $list
        ]);
    }

    public function createNewsCategory(NewsCategoryRequest $request){
        NewsCategory::create([
            'id_user' => Auth::id(),
            'name'      => $request->input('txtName'),
            'slug'      => Str::slug($request->input('txtName'))
        ]);
        return redirect()->route('list-news-category');
    }

    public function getListNews($id) {
        $newsCategory = NewsCategory::find($id);
        $user = Auth::user();
        $listNews = News::where('id_news_category', $id)->with('user')->get();
        return view('admin/news/list_news')->with([
            'newsCategory'      => $newsCategory,
            'user'              => $user,
            'listNews'          => $listNews
        ]);
    }

    public function getViewCreateNews($id) {
        $newsCategory = NewsCategory::find($id);
        return view('admin/news/create_news')->with([
            'newsCategory' => $newsCategory
        ]);
    }

    public function editNewCategory($id) {
        $newsCategory = NewsCategory::find($id);
        return view('admin.news.edit_new_category',compact('newsCategory'));
    }

    public function updateNewCategory(UpdateNewCategoryRequest $request,$id) {
        $newsCategory = NewsCategory::find($id);
        $data         = $request->except('_token');
        $data['name']     = $request->txtName;
        $data['updated_at'] = Carbon::now(); 
        $data['slug']     = Str::slug($request->txtName);
        $newsCategory->update($data);
        return redirect()->route('list-news-category');
    }

    public function deleteNewCategory($id) {
        $checkCate = News::where('id_news_category',$id)->count();
        if ($checkCate == 0) {
            $newsCategory = NewsCategory::find($id);
            $newsCategory->delete();
        }
        return redirect()->route('list-news-category');
    }

    public function createNewDetail(RegisterNewRequest $request) {
        $url_img = $request->file('url_img')->store('images',['disk' => 'public']);
        $data = $request->except('_token');
        $data['headline']       = $request->headline;
        $data['slug']           = Str::slug($request->headline);
        $data['id_user']        = Auth::id();
        $data['url_img']        = $url_img;
        $data['summary']        = $request->summary;
        $data['content']        = $request->input('content');
        $data['created_at']     = Carbon::now(); 
        $data['id_news_category'] = $request->id_news_category ? $request->id_news_category : 0;
        $id = News::insertGetId($data);
        if ($id) {
            return redirect()->route('news', ['id' => $request->id_news_category]);
        }
    }

    public function editNew($id) {
        $new = News::find($id);
        return view('admin.news.edit_news_detail',compact('new'));
    }

    public function updateNew(UpdateNewsDetailRequest $request,$id) {
        $newDetail = News::find($id);
        $data = $request->except('_token');
        $data['headline']       = $request->headline;
        $data['slug']           = Str::slug($request->headline);
        $data['id_user']        = Auth::id();
        $data['summary']        = $request->summary;
        $data['content']        = $request->input('content');
        $data['updated_at']     = Carbon::now(); 
        if($request->url_img) {
            if(file_exists(public_path('uploads/' . $newDetail->url_img))){ 
                unlink(public_path('uploads/' . $newDetail->url_img));
            }
            $url_img = $request->file('url_img')->store('images',['disk' => 'public']);
            $data['url_img']        = $url_img;
        }
        $newDetail->update($data);
        return redirect()->route('news', ['id' => $newDetail->id_news_category]);
    }
}

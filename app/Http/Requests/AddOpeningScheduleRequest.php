<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOpeningScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required',
            'local' => 'required',
            'time' => 'required',
            'schedule' => 'required',
            'original_tuition' => 'required',
            'discount' => 'required',
            'expire_date' => 'required',
            'note' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'start_date.required'       => 'Please enter Start date !',
            'local.required'            => 'Please enter Local !',
            'time.required'             => 'Please enter Time !',
            'schedule.required'         => 'Please enter Schedule !',
            'original_tuition.required' => 'Please enter Original tuition!',
            'discount.required'         => 'Please enter Discount !',
            'expire_date.required'      => 'Please enter Expire date !',
            'note.required'             => 'Please enter Note !',
        ];
    }
}

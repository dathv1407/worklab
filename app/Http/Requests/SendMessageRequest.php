<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'email'          => 'required',
            'phone'          => 'required',
            'title'          => 'required',
            'content'          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'      => 'Please enter your name !',
            'email.required'             => 'Please enter your email !',
            'phone.required'             => 'Please enter your phone !',
            'title.required'     => 'Please enter title !',
            'content.required'   => 'Please enter content !'
        ];
    }
}

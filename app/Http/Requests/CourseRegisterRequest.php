<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_name'   => 'required',
            'email'          => 'required',
            'phone'          => 'required',
            'id_open_schedule'          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'student_name.required'      => 'Please enter your name !',
            'email.required'             => 'Please enter your email !',
            'phone.required'             => 'Please enter your phone !',
            'id_open_schedule.required'  => 'Please enter date start !'
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCourseDetailContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main_content' => 'required',
            'detail_content' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'main_content.required' => 'Please enter Main Content !',
            'detail_content.required' => 'Please enter Detail Content !',
        ];
    }
}

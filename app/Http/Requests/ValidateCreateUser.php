<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateCreateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtUserName'   => 'required',
            'txtEmail'      => 'required',
            'txtPassword'   => 'required',
            'txtRepassword' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'txtUserName.required'      => 'Please enter your username !',
            'txtEmail.required'         => 'Please enter your email !',
            'txtPassword.required'      => 'Please enter your password !',
            'txtRepassword.required'    => 'Please enter your password !'
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCourseMainContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_course_main_content'         => 'required',
            'title_content'         => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id_course_main_content.required'       => 'Error not found ID !',
            'title_content.required'       => 'Please enter Title Content !'
        ];
    }
}

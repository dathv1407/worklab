<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'courseName'        => 'required',
            'homepage_avatar'   => 'required',
            'image_top'         => 'required',
            // 'image_information' => 'required',
            'information'       => 'required',
            'note'              => 'required',
            'document'          => 'required',
            'number_student'    => 'required',
            'day_study'         => 'required',
            'hour_study'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'courseName.required'       => 'Please enter Course Name !',
            'homepage_avatar.required'  => 'Please enter Course Name !',
            'image_top.required'        => 'Please enter Course Name !',
            // 'image_information.required' => 'Please enter Schedule !',
            'information.required'      => 'Please enter Schedule !',
            'note.required'             => 'Please enter Schedule !',
            'document.required'         => 'Please enter Document !',
            'number_student.required'   => 'Please enter Number Student !',
            'day_study.required'        => 'Please enter Total day study !',
            'hour_study.required'       => 'Please enter Total hour study !',
        ];
    }
}

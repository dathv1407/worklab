<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditChuyenDeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtName'        => 'required',
            'txtRoute'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'txtName.required'       => 'Please enter Course Name !',
            'txtRoute.required'  => 'Please enter Route !',

        ];
    }
}

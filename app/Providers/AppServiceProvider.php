<?php

namespace App\Providers;

use App\MenuClient;
use App\Message;
use App\News;
use App\NewsCategory;
use App\ShareKnowledgeCategory;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $menuDaoTao = MenuClient::find(1)->subMenu;
        $menuCertification = MenuClient::find(2)->subMenu;
        $unreadMessage = Message::where('status', 0)->count();
        $listNews = NewsCategory::all();
        $listShareKnowledge = ShareKnowledgeCategory::all();

        View::share('menuDaoTao', $menuDaoTao);
        View::share('menuCertification', $menuCertification);
        View::share('unreadMessage', $unreadMessage);
        View::share('listNewsCategory', $listNews);
        View::share('listShareKnowledgeMenu', $listShareKnowledge);
    }
}

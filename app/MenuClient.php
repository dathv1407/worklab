<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class MenuClient extends Model
{
    use Notifiable;
    public $timestamps = true;

    protected $table = 'menu_client';

    protected $fillable = [
        'name'
    ];

    public function subMenu() {
        return $this->hasMany(SubMenu::class, 'id_menu_client');
    }
}

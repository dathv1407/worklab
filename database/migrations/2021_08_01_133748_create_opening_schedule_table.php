<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpeningScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opening_schedules', function (Blueprint $table) {
            $table->id();
            $table->integer('id_course_detail');
            $table->date('start_date');
            $table->string('local');
            $table->string('time');
            $table->string('schedule');
            $table->integer('original_tuition');
            $table->integer('discount');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opening_schedules');
    }
}

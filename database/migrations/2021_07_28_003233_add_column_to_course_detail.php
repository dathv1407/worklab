<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCourseDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_details', function (Blueprint $table) {
            $table->string('url_homepage_avatar')->after('name')->default(null);
            $table->string('url_image_top')->after('url_homepage_avatar')->default(null);
            $table->integer('original_tuition')->after('schedule');
            $table->integer('discount')->after('original_tuition');
            $table->string('image_information')->after('discount');
            $table->string('information')->after('image_information');
            $table->string('note')->after('information');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_detail', function (Blueprint $table) {
            //
        });
    }
}

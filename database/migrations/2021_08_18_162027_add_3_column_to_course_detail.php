<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Add3ColumnToCourseDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_details', function (Blueprint $table) {
            $table->string('document')->after('teacher');
            $table->integer('number_student')->after('document');
            $table->integer('day_study')->after('number_student');
            $table->integer('hour_study')->after('day_study');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_detail', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShareKnowledgeDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('share_knowledge_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user');
            $table->integer('id_share_knowledge_category');
            $table->string('headline');
            $table->string('slug');
            $table->string('url_img');
            $table->text('summary');
            $table->longText('content');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('share_knowledge_detail');
    }
}

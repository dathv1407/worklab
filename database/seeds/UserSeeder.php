<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin Worklab',
            'email' => 'adminworklab@gmail.com',
            'password' => Hash::make('worklab123'),
            'level' => 1,
            'status' => 1,
        ]);
        DB::table('users')->insert([
            'name' => 'Admin Worklab1',
            'email' => 'adminworklab1@gmail.com',
            'password' => Hash::make('worklab123'),
            'level' => 1,
            'status' => 1,
        ]);
    }
}

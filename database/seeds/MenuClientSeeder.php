<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_client')->insert([
            'name' => 'CHUYÊN ĐỀ ĐÀO TẠO',
        ]);
        DB::table('menu_client')->insert([
            'name' => 'LUYỆN THI CHỨNG CHỈ',
        ]);
    }
}

<link
rel="icon"
href="{{ asset('new/images/new/logo.jpg') }}"
type="image/png"
sizes="16x16"
/>

<link
rel="stylesheet"
href="{{asset('new/css/bootstrap.min.css') }}"
type="text/css"
media="all"
/>
<link
rel="stylesheet"
href="{{asset('new/css/animate.min.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/swiper-bundle.min.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/meanmenu.min.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/magnific-popup.min.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/main.min.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/boxicons.min.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/flaticon.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/settings.css') }}"
type="text/css"
media="all"
/>
<link
rel="stylesheet"
href="{{asset('new/css/layers.css') }}"
type="text/css"
media="all"
/>
<link
rel="stylesheet"
href="{{asset('new/css/navigation.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/style.css') }}"
type="text/css"
media="all"
/>

<link
rel="stylesheet"
href="{{asset('new/css/responsive.css') }}"
type="text/css"
media="all"
/>
<link
rel="stylesheet"
href="{{asset('new/css/custom.css') }}"
type="text/css"
media="all"
/>


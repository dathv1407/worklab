<footer>
    <div class="footer-upper pt-100 pb-80 bg-blue">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-3">
            <div class="footer-content-item">
              <div class="footer-logo">
                <a href="index.html"
                  ><img src="{{ asset('new/images/new/logo.jpg') }}" alt="logo" class="logo-custom"
                /></a>
              </div>
              <div class="footer-details footer-address-info">
                <div class="footer-address-info-item">
                  <h4>Call Us</h4>
                  <p class="footer-contact-number">
                    <a href="tel:078 924 7333">Hotline: 078 924 7333</a>
                  </p>
                </div>
                <div class="footer-address-info-item">
                  <p class="footer-email">
                    <a
                      href="#"
                      ><span
                        class="__cf_email__"
                        data-cfemail="c5ada0a9a9aa85afa0bdaceba6aaa8"
                        >Email: tuyensinh@worklab.edu.vn</span
                      ></a
                    >
                  </p>
                </div>
                <div class="footer-address-info-item">
                  <p class="footer-physical-address">
                    Address: LK4-TT1, Số 96-96B Nguyễn Huy Tưởng, Quận Thanh Xuân, Hà Nội.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-9">
            <div class="footer-right pl-80">
              <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-3">
                  <div class="footer-content-list footer-content-item">
                    <div class="footer-content-title">
                      <h3>VỀ WORKLAB</h3>
                    </div>
                    <ul class="footer-details footer-list">
                      <li><a href="{{ route('client.introduce') }}">Giới thiệu chung</a></li>
                      <li>
                        <a href="{{ route('technology-news') }}">Tin tức</a>
                      </li>
                      <li><a href={{ route('list-knowledge') }}>Chia sẻ kiến thức</a></li>
                      <li><a href="{{ route('client.contact') }}">Liên hệ</a></li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3">
                  <div class="footer-content-list footer-content-item">
                    <div class="footer-content-title">
                      <h3>CÁC KHÓA HỌC</h3>
                    </div>
                    <ul class="footer-details footer-list">
                      <li><a href="#">Lập trình phần mềm</a></li>
                      <li><a href="#">Kiểm thử phần mềm</a></li>
                      <li>
                        <a href=#">Phân tích nghiệp vụ (BA)</a>
                      </li>
                      <li>
                        <a href="#">Luyện thi chứng chỉ ISTQB</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                  <div
                    class="
                      footer-content-list footer-content-item
                      desk-pad-left-30
                    "
                  >
                    <div class="footer-content-title">
                      <h3>CHAT VỚI CHÚNG TÔI</h3>
                    </div>
                    <div class="footer-details">
                      <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fworklabacademy&tabs=messages&width=400&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="400" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-lower bg-off-white">
      <div class="background-shapes">
        <div class="background-shape-item">
          <img src="{{ asset('new/images/shapes/curved-line.png') }}" alt="line" />
        </div>
        <div class="background-shape-item">
          <img src="{{ asset('new/images/shapes/half-circle-shape.png') }}" alt="line" />
        </div>
        <div class="background-shape-item">
          <img src="{{ asset('new/images/shapes/animate-shape-1.png') }}" alt="line" />
        </div>
      </div>
      <div class="container">
        <div class="footer-lower-grid">
          <div class="footer-lower-item footer-lower-info">
            <div class="footer-copyright-text">
              <p>
                Copyright ©2021 Design &amp; Developed By
                <a href="#" target="_blank"
                  >Worklab</a
                >
              </p>
            </div>
          </div>
          <div class="footer-lower-item">
            <ul class="social-list">
              <li>
                <a href="#"
                  ><img src="{{ asset('new/images/facebook.png') }}" alt="social"
                /></a>
              </li>
              <li>
                <a href="#"
                  ><img src="{{ asset('new/images/twitter.png') }}" alt="social"
                /></a>
              </li>
              <li>
                <a href="#"
                  ><img src="{{ asset('new/images/linkedin.png') }}" alt="social"
                /></a>
              </li>
              <li>
                <a href="#"
                  ><img src="{{ asset('new/images/instagram.png') }}" alt="social"
                /></a>
              </li>
              <li>
                <a href="#"
                  ><img src="{{ asset('new/images/youtube.png') }}" alt="social"
                /></a>
              </li>
              <li>
                <a href="#"
                  ><img src="{{ asset('new/images/skype.png') }}" alt="social"
                /></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
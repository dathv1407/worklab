<script src="{{ asset('new/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{asset('new/js/jquery.countdown.min.js') }}"></script>

<script src="{{ asset('new/js/jquery.magnific-popup.min.js') }}"></script>

<script src="{{ asset('new/js/swiper-bundle.min.js') }}"></script>

<script src="{{ asset('new/js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('new/js/jquery.counterup.min.js') }}"></script>

<script src="{{ asset('new/js/moment.js') }}"></script>
<script src="{{ asset('new/js/main.min.js') }}"></script>
<script src="{{ asset('new/js/jquery-ui.js') }}"></script>

<script src="{{asset('new/js/isotope.pkgd.min.js') }}"></script>

<script src="{{asset('new/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{asset('new/js/jquery.themepunch.tools.min.js') }}"></script>

<script src="{{asset('new/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{asset('new/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{asset('new/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{asset('new/js/extensions/revolution.extension.layeranimation.min.j') }}s"></script>
<script src="{{asset('new/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{asset('new/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{asset('new/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{asset('new/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{asset('new/js/extensions/revolution.extension.video.min.js') }}"></script>

<script src="{{asset('new/js/jquery.ajaxchimp.min.js') }}"></script>

<script src="{{asset('new/js/form-validator.min.js') }}"></script>

<script src="{{asset('new/js/contact-form-script.js') }}"></script>

<script src="{{asset('new/js/jquery.meanmenu.min.js') }}"></script>

<script src="{{asset('new/js/script.js') }}"></script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v11.0&appId=274883790755816&autoLogAppEvents=1" nonce="Fj2LFYjv"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
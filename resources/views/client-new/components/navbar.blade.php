<div class="preloader bg-blue">
    <div class="preloader-wrapper">
      <div class="loader-content book">
        <figure class="page"></figure>
        <figure class="page"></figure>
        <figure class="page"></figure>
      </div>
      <h3>Welcome To Worklab</h3>
    </div>
  </div>
<div class="navbar-area">
    <div class="mobile-nav">
      <a href="index.html" class="mobile-brand">
        <img src="{{ asset('new/images/new/logo.jpg') }}" alt="logo" class="logo logo-custom" />
      </a>
      <div class="navbar-option">
        <div class="navbar-option-item navbar-option-dots dropdown">
          <button
            type="button"
            id="dot"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <i class="flaticon-menu"></i>
          </button>
          <ul class="dropdown-menu navbar-dots-dropdown">
            <li class="dropdown-item">
              <div
                class="
                  navbar-option-item navbar-option-language
                  dropdown
                  language-option
                "
              >
                <button
                  class="dropdown-toggle"
                  type="button"
                  id="language3"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i class="flaticon-worldwide"></i>
                  <span class="lang-name"></span>
                </button>
                <div
                  class="dropdown-menu language-dropdown-menu"
                  aria-labelledby="language3"
                >
                  <a class="dropdown-item" href="#">
                    <img src="{{ asset('new/images/usa.png') }}" alt="flag" />
                    English
                  </a>
                  <a class="dropdown-item" href="#">
                    <img src={{ asset('new/images/germany.png') }}" alt="flag" />
                    Deutsch
                  </a>
                  <a class="dropdown-item" href="#">
                    <img src="{{ asset('new/images/china.png') }}" alt="flag" />
                    简体中文
                  </a>
                  <a class="dropdown-item" href="#">
                    <img src="{{ asset('new/images/uae.png') }}" alt="flag" />
                    العربيّة
                  </a>
                </div>
              </div>
            </li>
            <li class="dropdown-item">
              <div class="navbar-option-item navbar-option-cart">
                <div class="navbar-option-item navbar-option-search dropdown">
                  <button
                    type="button"
                    id="search1"
                    data-bs-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i class="flaticon-search"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="search1">
                    <form>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="input-group-text">
                            <i class="flaticon-search"></i>
                          </div>
                        </div>
                        <input
                          type="text"
                          class="form-control"
                          placeholder="Search result"
                        />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </li>
            <li class="dropdown-item">
              <div class="navbar-option-item navbar-option-cart">
                <a href="cart.html"
                  ><i class="flaticon-shopping-cart"></i>
                  <span class="option-badge option-badge-main">2</span>
                </a>
              </div>
            </li>
            <li class="dropdown-item">
              <div class="navbar-option-item navbar-option-wishlist">
                <a href="wishlist.html"
                  ><i class="flaticon-heart"></i>
                  <span class="option-badge option-badge-main">1</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
        <div
          class="
            navbar-option-item navbar-option-language
            dropdown
            mobile-hide
            language-option
          "
        >
          <button
            class="dropdown-toggle"
            type="button"
            id="language1"
            data-bs-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <i class="flaticon-worldwide"></i>
            <span class="lang-name"></span>
          </button>
          <div
            class="dropdown-menu language-dropdown-menu"
            aria-labelledby="language1"
          >
            <a class="dropdown-item" href="#">
              <img src="{{ asset('new/images/usa.png') }}" alt="flag" />
              English
            </a>
            <a class="dropdown-item" href="#">
              <img src="{{ asset('new/images/germany.png') }}" alt="flag" />
              Deutsch
            </a>
            <a class="dropdown-item" href="#">
              <img src="{{ asset('new/images/china.png') }}" alt="flag" />
              简体中文
            </a>
            <a class="dropdown-item" href="#">
              <img src="{{ asset('new/images/uae.png') }}" alt="flag" />
              العربيّة
            </a>
          </div>
        </div>
        <div
          class="navbar-option-item navbar-option-search dropdown mobile-hide"
        >
          <button
            type="button"
            id="search"
            data-bs-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <i class="flaticon-search"></i>
          </button>
          <div class="dropdown-menu" aria-labelledby="search">
            <form>
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    <i class="flaticon-search"></i>
                  </div>
                </div>
                <input
                  type="text"
                  class="form-control"
                  placeholder="Search result"
                />
              </div>
            </form>
          </div>
        </div>
        <div class="navbar-option-item navbar-option-cart mobile-hide">
          <a href="cart.html"
            ><i class="flaticon-shopping-cart"></i>
            <span class="option-badge option-badge-main">2</span>
          </a>
        </div>
        <div class="navbar-option-item navbar-option-wishlist mobile-hide">
          <a href="wishlist.html"
            ><i class="flaticon-heart"></i>
            <span class="option-badge option-badge-main">1</span>
          </a>
        </div>
        <div class="navbar-option-item navbar-option-account">
          <a href="authentication.html"><i class="flaticon-user"></i></a>
        </div>
      </div>
    </div>

    <div class="main-nav">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-md navbar-light">
          <a class="navbar-brand" href="{{route('homepage')}}">
            <img src="{{ asset('new/images/new/logo.jpg') }}" alt="logo" class="logo logo-custom" />
          </a>
          <div
            class="collapse navbar-collapse mean-menu"
            id="navbarSupportedContent"
          >
            <ul class="navbar-nav mx-auto">
              <li class="nav-item">
                <a href="{{route('homepage')}}" class="nav-link {{ (request()->is('/')) ? 'active' : '' }}">TRANG CHỦ</a>
              </li>
              <li class="nav-item">
                <a href="{{ route('client.introduce') }}" class="nav-link {{ (request()->is('gioi-thieu')) ? 'active' : '' }}">GIỚI THIỆU</a>
              </li>
              <li class="nav-item">
                <a href="{{route('dao-tao-chuyen-de', ['id' => 'dao-tao'])}}" class="nav-link dropdown-toggle {{ (request()->segment(2) == 'dao-tao') ? 'active' : '' }}">KHÓA HỌC</a>
                <ul class="dropdown-menu">
                  @foreach($menuDaoTao as $item)
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('dao-tao-chuyen-de', ['id' => $item->route])}}"> {{ $item->name }}</a>
                    </li>
                  @endforeach
                </ul>
              </li>
              <li class="nav-item">
                <a href="{{route('dao-tao-chuyen-de', ['id' => 'chung-chi'])}}" class="nav-link dropdown-toggle {{ (request()->segment(2) == 'chung-chi') ? 'active' : '' }}">LUYỆN THI CHỨNG CHỈ</a>
                <ul class="dropdown-menu">
                  @foreach($menuCertification as $item)
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('dao-tao-chuyen-de', ['id' => $item->route])}}"> {{ $item->name }}</a>
                    </li>
                  @endforeach
                </ul>
              </li>
              <li class="nav-item">
                <a href="{{route('lich-khai-giang-moi-nhat')}}" class="nav-link {{ (request()->is('lich-khai-giang-moi-nhat')) ? 'active' : '' }}">LỊCH KHAI GIẢNG</a>
                <span id="txt-new">NEW</span>
              </li>
              <li class="nav-item">
                <a href="{{ route('technology-news') }}" class="nav-link dropdown-toggle {{ (request()->is('list-news/*')) ? 'active' : '' }}">TIN TỨC</a>
                  @if($listNewsCategory)
                  <ul class="dropdown-menu">
                    @foreach($listNewsCategory as $item)
                        <li class="nav-item">
                            <a href="{{ route('list-news', ['id' => $item->id, 'slug' => $item->slug]) }}" class="nav-link">
                                {{$item->name}}
                            </a>
                        </li>
                    @endforeach
                  </ul>
                  @endif
              </li>
              <li class="nav-item">
                 <a href="{{ route('list-knowledge') }}" class="nav-link dropdown-toggle {{ (request()->is('list-knowledge/*')) ? 'active' : '' }}">CHIA SẺ KIẾN THỨC</a>
                  @if($listShareKnowledgeMenu)
                      <ul class="dropdown-menu">
                            @foreach($listShareKnowledgeMenu as $item2)
                            <li class="nav-item">
                                <a href="{{ route('list-knowledge-category', ['id' => $item2->id, 'slug' => $item2->slug]) }}" class="nav-link" >
                                    {{$item2->name}}
                                </a>
                            </li>
                            @endforeach
                      </ul>
                  @endif
              </li>
              <li class="nav-item">
                <a href="{{ route('client.contact') }}" class="nav-link {{ (request()->is('lien-he')) ? 'active' : '' }}">LIÊN HỆ</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>

@extends('client-new.layout.master')
@section('content')
<header class="header-page">
    <div class="header-page-shape header-page-shape-middle">
        <img src="{{ asset('new/images/shapes/shape-19.png') }}" alt="shape">
    </div>
    <div class="container">
        <div class="header-page-content">
            <h1>LIÊN HỆ</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-custom">
                    <li class="breadcrumb-item"><a href="{{ route('homepage') }}">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Liên hệ</li>
                </ol>
            </nav>
        </div>
    </div>
</header>
<section class="contact-information-section pt-100 pb-70">
    <div class="container">
      <div class="row align-items-center">
        <h2 style="color: red">{{session('message')}}</h2>
        <div class="col-12 col-lg-5 pb-30">
          <div class="contact-information-item">

            <div
              class="
                section-title section-title-shapeless section-title-left
                text-start
              "
            >
              <h2>LIÊN HỆ VỚI CHÚNG TÔI</h2>
              <p>Với đội ngũ tư vấn viên nhiệt tình, hỗ trợ thường xuyên 24/7 sẽ giúp bạn giải đáp mọi thắc mắc về các sản phẩm, chương trình đào tạo của WORKLAB.</p>
            </div>
            <div class="contact-options">
              <div class="contact-option-item">
                <div class="contact-option-icon">
                  <i class="flaticon-phone-call"></i>
                </div>
                <div class="contact-option-details">
                  <p>Hotline: <a href="tel:078 924 7333">078 924 7333</a></p>
                  <p>
                    Email:
                    <a
                      href="#"
                      ><span
                        class="__cf_email__"
                        data-cfemail="721a171e1e1d3218170a1b5c111d1f"
                        >tuyensinh@worklab.edu.vn</span
                      ></a
                    >
                  </p>
                </div>
              </div>
              <div class="contact-option-item">
                <div class="contact-option-icon">
                  <i class="flaticon-network"></i>
                </div>
                <div class="contact-option-details">
                  <p>
                    LK4-TT1, Số 96-96B Nguyễn Huy Tưởng, Quận Thanh Xuân, Hà Nội.
                  </p>
                </div>
              </div>
              <div class="contact-option-item">
                <div class="contact-option-icon">
                  <i class="flaticon-clock"></i>
                </div>
                <div class="contact-option-details">
                  <p>Thứ 2 - Thứ 6: <span>09:00 am - 22:00 pm</span></p>
                  <p>Thứ 7 & Chủ nhật: <span>10:30 am - 22:00 pm</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-7 pb-30">
          <div class="contact-information-item">
            <div class="contact-information-image">
              <img src="{{ asset('new/images/new/contact/contact.png') }}" alt="contact" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="contact-form-section pb-100">
    <div class="container">
      <div class="contact-form-box">
        <div class="sub-section-title text-center">
          <h3 class="sub-section-title-heading">LỜI NHẮN</h3>
        </div>
        <form class="contact-form" method="post" action="{{ route('send-message') }}">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-12 col-md-6 col-lg-6">
              <div class="form-group mb-20">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="flaticon-user"></i
                    ></span>
                  </div>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    class="form-control"
                    placeholder="Họ tên*"
                    required
                    data-error="Vui lòng nhập họ tên!"
                  />
                </div>
                <div class="help-block with-errors">
                  @error('name')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
              <div class="form-group mb-20">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="flaticon-envelope"></i
                    ></span>
                  </div>
                  <input
                    type="text"
                    name="email"
                    id="email"
                    class="form-control"
                    placeholder="Email*"
                    required
                    data-error="Vui lòng nhập email!"
                  />
                </div>
                <div class="help-block with-errors">
                  @error('email')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
              <div class="form-group mb-20">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="flaticon-phone-call"></i
                    ></span>
                  </div>
                  <input
                    type="text"
                    name="phone"
                    id="phone"
                    class="form-control"
                    placeholder="Số điện thoại*"
                    required
                    data-error="Vui lòng nhập số điên thoại!"
                  />
                </div>
                <div class="help-block with-errors">
                  @error('phone')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
              <div class="form-group mb-20">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="flaticon-book"></i
                    ></span>
                  </div>
                  <input
                    type="text"
                    name="title"
                    id="subject"
                    class="form-control"
                    placeholder="Tiêu đề*"
                    required
                    data-error="Vui lòng nhập tiêu đề!"
                  />
                </div>
                <div class="help-block with-errors">
                  @error('title')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-12">
              <div class="form-group mb-20">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"
                      ><i class="flaticon-envelope"></i
                    ></span>
                  </div>
                  <textarea
                    name="content"
                    class="form-control"
                    id="message"
                    rows="6"
                    placeholder="Nội dung*"
                  ></textarea>
                </div>
                <div class="help-block with-errors">
                  @error('content')
                  <div class="text-danger">{{ $message }}</div>
                  @enderror
                </div>
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-12 text-center">
              <button class="btn main-btn" type="submit">
               HOÀN THÀNH
              </button>
              <div id="msgSubmit"></div>
              <div class="clearfix"></div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
@endsection

@section('text', 'Liên hệ')

@extends('client-new.layout.master')

@section('content')
    <header class="header-page">
        <div class="header-page-shape header-page-shape-middle">
            <img alt="shape" src="assets/images/shapes/shape-18.png">
        </div>
        <div class="container">
            <div class="header-page-content">
                <h1>TIN TỨC </h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('homepage')}}">Trang chủ</a></li>
                        <li aria-current="page" class="breadcrumb-item active">Tin tức</li>
                    </ol>
                </nav>
            </div>
        </div>
    </header>
    <section class="contact-information-section pb-70">
        <div class="blog-section pt-50 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8 pb-30">
                        <div class="blog-list-content" style="padding-left: 100px; padding-right: 100px">
                            @foreach($listNews as $news)
                            <div class="blog-list-card mb-30">
                                <div class="blog-list-card-thumb">
                                    <a href="{{route('detail-news', ['id' => $news->id, 'slug' => $news->slug])}}"><img alt="blog" src="{{asset('uploads/'.$news->url_img)}}"></a>
                                </div>
                                <div class="blog-list-card-content bg-off-white">
                                    {{--<ul class="course-entry-list">
                                        <li><i class="flaticon-open-book"></i> Study Pass</li>
                                        <li><i class="flaticon-man"></i> Admin</li>
                                    </ul>--}}
                                    <h3><a href="{{route('detail-news', ['id' => $news->id, 'slug' => $news->slug])}}">{{$news->headline}}</a></h3>
                                    <p>{{$news->summary}}</p>
                                </div>
                            </div>
                            @endforeach
                            <nav aria-label="Page navigation example" class="page-pagination">
                                {!! $listNews->links() !!}
                            </nav>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 pb-30">
                        <div class="sidebar-item mb-40">
                            <div class="sidebar-title">
                                <h3>Recent Posts</h3>
                            </div>
                            <div class="blog-recent-content">
                                @foreach($latestNews as $item)
                                <div class="blog-recent-content-item">
                                    <div class="blog-recent-content-image">
                                        <a href="blog-details.html">
                                            <img alt="recent" src="{{asset('uploads/'.$item->url_img)}}">
                                        </a>
                                    </div>
                                    <div class="blog-recent-content-details">
                                        <ul class="course-entry-list">
                                            <li><i class="flaticon-online-learning-1"></i>{{$item->created_at}}</li>
                                        </ul>
                                        <h3><a href="blog-details.html">{{$item->headline}}</a></h3>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('text', 'Tin tức')


@extends('client-new.layout.master')

@section('content')
    <header class="header-page">
        <div class="header-page-shape header-page-shape-middle">
            <img src="assets/images/shapes/shape-18.png" alt="shape">
        </div>
        <div class="container">
            <div class="header-page-content">
                <h3> LỊCH KHAI GIẢNG MỚI NHẤT TẠI WORKLAB</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('homepage')}}">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Lịch khai giảng</li>
                    </ol>
                </nav>
            </div>
        </div>
    </header>
    <section class="contact-information-section pt-100 pb-70">
        <div class="container">
            <div class="row align-items-center">
                <table style="min-height: auto!important" class="table table-bordered table-hover product-item">
                    <thead>
                    <tr>
                        <th>Khóa học</th>
                        <th>Ngày Khai giảng</th>
                        <th>Cơ sở/Hình thức học</th>
                        <th>Thời gian học</th>
                        <th>Lịch học</th>
                        <th>Ưu đãi học phí</th>
                        <th>Học phí còn lại </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($courseDetail as $course)
                        @foreach($listSchedule as $item)
                            @if($course->id == $item->id_course_detail)
                                <tr>
                                    <td>
                                        <a target="_blank"
                                           href="{{route('chi-tiet-khoa-hoc',['name' => Str::slug($course->name),'id' => $course->id])}}">
                                            {{$course->name}} </a>
                                    </td>
                                    <td>{{$item->start_date}}</td>
                                    <td>{{$item->local}}</td>
                                    <td>{{$item->time}}</td>
                                    <td>{{$item->schedule}}</td>
                                    <td class="text-danger font-weight-bold">Giảm <b>{{$item->discount}}%</b> khi hoàn thành học phí trước ngày <b>{{$item->expire_date}}</b></td>
                                    <td class="text-danger font-weight-bold"><b>{{number_format((100-$item->discount)*$item->original_tuition/100)}} vnđ</b></td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection

@section('text', 'Lịch khai giảng mới nhất')


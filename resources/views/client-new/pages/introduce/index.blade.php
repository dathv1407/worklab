@extends('client-new.layout.master')
@section('content')
<header class="header-page">
    <div class="header-page-shape header-page-shape-middle">
        <img src="{{ asset('new/images/shapes/shape-18.png') }}" alt="shape">
    </div>
    <div class="container">
        <div class="header-page-content">
            <h1>GIỚI THIỆU</h1>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-custom">
                    <li class="breadcrumb-item"><a href="{{ route('homepage') }}">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Giới thiệu</li>
                </ol>
            </nav>
        </div>
    </div>
</header>
<section class="contact-information-section pt-100 pb-70">
    <div class="container">
      <div class="row align-items-center box-custom">
        <div class="panel panel-default dobong wow zoomInDown" style="visibility: visible; animation-name: zoomInDown;">
                  <div class="panel-body">
                      <div id="detailpost">
                          <p style="text-align:center"><span style="font-size:36px"><span style="font-family:Arial,Helvetica,sans-serif">&nbsp;<strong>VỀ WORKLAB</strong></span></span></p>
                          <p><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Trung tâm đào tạo công nghệ WORKLAB được thành lập vào năm 2021 với sứ mệnh nghiên cứu, xây dựng và đào tạo đội ngũ nhân lực công nghệ thông tin chất lượng cao. Các chương trình đào tạo tại WORKLAB luôn luôn được cập nhật, cải tiến nhằm đem lại hiệu quả cao nhất cho học viên.</strong></span></span></p>
                           <!--<p><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Học viên tốt nghiệp tại Trung tâm sẽ có đủ năng lực về kiến thức nghiệp vụ cũng như kỹ năng mềm để đáp ứng có thể làm việc trong môi trường hội nhập quốc tế.&nbsp;</span></span></p>-->
                          <img style="margin-left: 55px;" src="{{ asset('new/images/new/introduce/vison2.png') }}" alt="vision" class="img-intro">

                          <p style="text-align:center"><span style="font-size:36px"><span style="font-family:Arial,Helvetica,sans-serif">&nbsp;<strong>GIÁ TRỊ CỐT LÕI</strong></span></span></p>
                          <ul>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Con người:</strong> WORKLAB lấy học viên là trung tâm, đội ngũ giảng viên có nghiệp vụ sư phạm, kinh nghiệm thực tế, luôn mang tới cho học viên những giá trị tốt nhất.</span></span></li>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Chất lượng:</strong> Chất lượng giảng dạy và hỗ trợ học viên trong quá trình làm việc là ưu tiên hàng đầu của WORKLAB. Chúng tôi cam kết chất lượng tới từng học viên sau mỗi khóa học.</span></span></li>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Môi trường đào tạo:</strong> Chuyên nghiệp, tập trung vào thực hành, với mô hình dự án thu nhỏ trong mỗi khóa học, chuẩn bị cho học viên sẵn sàng tâm lý và trang bị thêm nhiều kinh nghiệm trong quá trình làm việc.</span></span></li>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Giáo trình:</strong> Giáo trình được biên soạn, review kỹ lưỡng, dựa trên những dự án thực tế và được cập nhật thường xuyên để phù hợp với học viên cũng như đáp ứng theo xu thế phát triển của công nghệ.</span></span></li>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Cơ hội mở rộng mạng lưới:</strong>  Với mỗi học viên của WORKLAB, chúng tôi tạo sự gắn kết trước, trong và sau khóa học. Đặc biệt, với môi trường học tập đa dạng, số lượng học viên phong phú, học viên có cơ hội kết nối mở rộng mối quan hệ trong công việc, có nhiều cơ hội hợp tác trong tương lai.</span></span></li>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Uy tín:</strong> Động lực phát triển của WORKLAB là sự tín nhiệm của học viên. Ngoài ra, trong mỗi khóa học, WORKLAB luôn thực hiện các cuộc khảo sát, lấy ý kiến của học viên nhằm mục đích nâng cao chất lượng và giảng dạy.</span></span></li>
                          </ul>
                          <p style="text-align:center"><span style="font-size:36px"><span style="font-family:Arial,Helvetica,sans-serif">&nbsp;<strong>CƠ SỞ VẬT CHẤT</strong></span></span></p>
                          <ul>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Cơ sở: LK4-TT1, Số 96-96B Nguyễn Huy Tưởng, Thanh Xuân, Hà Nội.</span></span></li>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Trang thiết bị: Phòng học được trang bị đầy đủ thiết bị học tập hiện đại.</span></span></li>
                          </ul>
                          <p style="text-align:center"><span style="font-size:36px"><span style="font-family:Arial,Helvetica,sans-serif">&nbsp;<strong>CAM KẾT SAU ĐÀO TẠO</strong></span></span></p>
                          <ul>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Học viên tham gia các khóa học tại WORKLAB sẽ có đủ khả năng làm việc thực tế tại các doanh nghiệp chỉ sau 1 khóa học bất kì. </span></span></li>
                              <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Với mạng lưới liên kết tuyển dụng với các công ty công nghệ trên cả nước, WORKLAB cam kết hỗ trợ tìm kiếm việc làm cho mỗi học viên sau khóa học.</span></span></li>
                          </ul>
                      </div>
                  </div>
              </div>
      </div>
    </div>
  </section>
@endsection


@section('text', 'Giới thiệu về WorkLab')

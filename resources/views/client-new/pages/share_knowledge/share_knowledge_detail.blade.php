@extends('client-new.layout.master')

@section('content')
    <header class="header-page">
        <div class="header-page-shape header-page-shape-middle">
            <img alt="shape" src="{{asset('assets/images/shapes/shape-18.png')}}">
        </div>
        <div class="container">
            <div class="header-page-content">
                <h1>TIN TỨC </h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('homepage')}}">Trang chủ</a></li>
                        <li aria-current="page" class="breadcrumb-item active">Chia sẻ kiến thức</li>
                    </ol>
                </nav>
            </div>
        </div>
    </header>
    <section class="contact-information-section pb-70">
        <div class="blog-section pt-50 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4 pb-30">
                        <div class="sidebar-item mb-40">
                            <div class="sidebar-title">
                                <h3>Recent Posts</h3>
                            </div>
                            <div class="blog-recent-content">
                                @foreach($latestShareKnowledge as $item2)
                                    <div class="blog-recent-content-item">
                                        <div class="blog-recent-content-image">
                                            <a href="{{route('detail-share-knowledge', ['id' => $item2->id, 'slug' => $item2->slug])}}">
                                                <img alt="recent" src="{{asset($item2->url_img)}}">
                                            </a>
                                        </div>
                                        <div class="blog-recent-content-details">
                                            <ul class="course-entry-list">
                                                <li><i class="flaticon-online-learning-1"></i>{{$item2->created_at}}</li>
                                            </ul>
                                            <h3><a href="{{route('detail-share-knowledge', ['id' => $item2->id, 'slug' => $item2->slug])}}">{{$item2->headline}}</a></h3>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 pb-30">
                        <div class="blog-details-box mb-30">
                            <div class="blog-post-details">
                                {{--<div class="blog-list-card-thumb">
                                    <a href="single-new.html"><img alt="blog" src="assets/images/blog-list-1.jpg"></a>
                                </div>--}}
                                {!! $detailShareKnowledge->content !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

@section('text', $detailShareKnowledge->headline)


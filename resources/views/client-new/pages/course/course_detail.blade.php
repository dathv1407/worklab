@extends('client-new.layout.master')

@section('css')
<style>
.summery-info-details-inner h6,.summery-info-details-inner h5,.summery-info-details-inner h4,.summery-info-details-inner h3 {
    font-weight: unset;
}
.summery-details-item > .detail-item-custom {
    padding-left: 0px ;
}
</style>
@endsection

@section('content')

    <header class="header-page">
        <div class="container">
            <div class="header-page-content desk-pad-right-30">
                <div class="course-badge">-{{$discount}}%</div>
                <h1>{{ $courseDetail->name }}</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('homepage') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                           {{ $courseDetail->name }}
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </header>

    <section class="course-details-section pt-100 pb-70">
        <div class="container">
            <div class="row">
                <h2 style="color: red; font-size:25px;">{{session('status')}}</h2>
                <div class="col-12 col-lg-4 pb-30 order-lg-2">
                    <div class="summery-details-item sidebar-to-header">
                        <div class="summery-box">
                            <div class="summery-iframe">
                                <img src="{{asset($courseDetail->url_image_top)}}" alt="" class="img-fluid w-100">
                            </div>
                            <div class="summery-inner">
                                <div class="summery-list">
                                    <div class="summery-list-item">
                                        <div class="summery-label">
                                            <i class="flaticon-equalizer"></i>Số lượng học viên
                                        </div>
                                        <div class="summery-option">{{$courseDetail->number_student}} học viên/lớp</div>
                                    </div>
                                    <div class="summery-list-item">
                                        <div class="summery-label">
                                            <i class="flaticon-instructor"></i>Tổng số buổi học
                                        </div>
                                        <div class="summery-option">{{$courseDetail->day_study}} buổi</div>
                                    </div>
                                    <div class="summery-list-item">
                                        <div class="summery-label">
                                            <i class="flaticon-online-learning-1"></i>Thời lượng học
                                        </div>
                                        <div class="summery-option">{{$courseDetail->hour_study}}h</div>
                                    </div>
                                </div>
                                <div class="summery-material-list">
                                    <h4>Tài liệu tham khảo</h4>
                                    <ul>
                                        {!! $courseDetail->document !!}
                                    </ul>
                                </div>

                                <div class="summery-material-list">
                                    <h4 class="text-discount"></h4>
                                </div>
                                <div class="countdown countdown--style-1 countdown--style-1-v1" data-countdown-date="" data-countdown-label="show" data-date={{$dateEarliest}}>
                                </div>
                                <div class="contact-form-box">
                                    <div class="summery-material-list">
                                        <h4>ĐĂNG KÝ NGAY</h4>
                                    </div>
                                    <form class="contact-form" method="post" action="{{route('course-register')}}" >
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-12 col-md-12 col-lg-12">
                                                <div class="form-group mb-20">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="flaticon-user"></i></span>
                                                        </div>
                                                        <input type="text" value="{{ old('student_name') }}" name="student_name" id="name" class="form-control" placeholder="Name*" required="" data-error="Please enter your name">
                                                    </div>
                                                    <div class="help-block with-errors">
                                                        @error('student_name')
                                                        <div class="text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-12">
                                                <div class="form-group mb-20">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="flaticon-envelope"></i></span>
                                                        </div>
                                                        <input type="text" value="{{ old('email') }}" name="email" id="email" class="form-control" placeholder="Email*" required="" data-error="Please enter your email">

                                                    </div>
                                                    <div class="help-block with-errors">
                                                        @error('email')
                                                        <div class="text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-12">
                                                <div class="form-group mb-20">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="flaticon-calendar"></i></span>
                                                        </div>
                                                        <select name="id_open_schedule" class="form-control">
                                                            <option value="">Chọn ngày khai giảng</option>
                                                            @foreach($listSchedule as $item)
                                                                <option value="{{$item->id}}"> {{$item->start_date}}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                    <div class="help-block with-errors">
                                                        @error('id_open_schedule')
                                                        <div class="text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-12">
                                                <div class="form-group mb-20">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="flaticon-phone-call"></i></span>
                                                        </div>
                                                        <input type="text" value="{{ old('phone') }}" name="phone" id="phone" class="form-control" placeholder="Phone*" required="" data-error="Please enter your phone number">
                                                    </div>
                                                    <div class="help-block with-errors">
                                                        @error('phone')
                                                        <div class="text-danger">{{ $message }}</div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-12">
                                                <div class="form-group mb-20">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="flaticon-envelope"></i></span>
                                                        </div>
                                                        <textarea name="message" class="form-control" id="message" rows="6" placeholder="Your Comment*">{{ old('message') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-12 text-center">
                                                <button class="btn main-btn disabled" type="submit" style="pointer-events: all; cursor: pointer;">
                                                    SEND A MESSAGE
                                                </button>
                                                <div id="msgSubmit"></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <br>
                                {{-- <div class="summery-social-share">
                                    <ul class="social-list">
                                        <li>
                                            <a href="#"
                                            ><img src="{{ asset('new/images/facebook.png')}}" alt="social"
                                                /></a>
                                        </li>
                                        <li>
                                            <a href="#"
                                            ><img src="{{ asset('new/images/twitter.png')}}" alt="social"
                                                /></a>
                                        </li>
                                        <li>
                                            <a href="#"
                                            ><img src="{{ asset('new/images/linkedin.png')}}" alt="social"
                                                /></a>
                                        </li>
                                        <li>
                                            <a href="#"
                                            ><img src="{{ asset('new/images/instagram.png')}}" alt="social"
                                                /></a>
                                        </li>
                                        <li>
                                            <a href="#"
                                            ><img src="{{ asset('new/images/youtube.png')}}" alt="social"
                                                /></a>
                                        </li>
                                        <li>
                                            <a href="#"
                                            ><img src="{{ asset('new/images/skype.png')}}" alt="social"
                                                /></a>
                                        </li>
                                    </ul>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-8 pb-30 order-lg-1">
                    <div class="summery-details-item desk-pad-right-30">
                        <ul class="product-tab-list detail-item-custom">
                            <li class="active" data-product-tab="1">Thông tin khóa học</li>
                            <li data-product-tab="2">Lịch học - Học phí</li>
                            <li data-product-tab="3">Giảng viên</li>
                            <li data-product-tab="4">Nội dung học</li>
                        </ul>
                        <div class="summery-info-details">
                            <div
                                    class="
                    summery-info-details-item summery-info-details-item-active
                  "
                                    data-summery-info-details="1"
                            >
                                <div class="summery-info-details-inner">
                                    {!! $courseDetail->information !!}
                                </div>
                            </div>
                            <div
                                    class="summery-info-details-item"
                                    data-summery-info-details="2"
                            >
                                <table style="background-color:white; min-height: auto!important" class="table table-bordered table-hover product-item">
                                    <thead>
                                    <tr>
                                        <th style="width: 12%">Khai giảng</th>
                                        <th style="width: 15%">Cơ sở/<br>Hình thức</th>
                                        <th style="width: 10%">Thời gian</th>
                                        <th style="width: 10%">Lịch học</th>
                                        <th style="width: 20%">Ưu đãi học phí</th>
                                        <th style="width: 20%">Học phí còn lại </th>
                                        <th style="width: 20%">Note </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listSchedule as $item)
                                        <tr>
                                            <td>{{$item->start_date}}</td>
                                            <td>{{$item->local}}</td>
                                            <td>{{$item->time}}</td>
                                            <td>{{$item->schedule}}</td>
                                            <td class="text-danger font-weight-bold">Hỗ trợ <b>{{$item->discount}}%</b> học phí
                                                khi đăng ký đến hết ngày <b>{{$item->expire_date}}</b></td>
                                            <td class="text-danger font-weight-bold"><b>{{number_format((100-$item->discount)*$item->original_tuition/100)}} vnđ</b></td>
                                            <td class="text-danger font-weight-bold"><b>{{$item->note}}</b></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div
                                    class="summery-info-details-item"
                                    data-summery-info-details="3"
                            >
                                <div class="summery-info-details-inner">
                                    <div class="summery-info-instructor">
                                        <div class="summery-info-instructor-details">
                                            {!! $courseDetail->teacher !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="summery-info-details-item" data-summery-info-details="4">
                                @foreach ($courseMainContent as $course)
                                    <section id="htmlCss">
                                        <div class="container">
                                            <img src="{{asset($course->url_image_content)}}" alt="" class="img-fluid w-100">
                                            <div class="row content py-5">
                                                @php $check = 0 @endphp
                                                @foreach($courseDetailContent as $key => $detail)
                                                    @if($course->id == $detail->id_course_main_content)
                                                        @php $check++ @endphp
                                                        <div class="col-md-6">
                                                            <div class="pp-item pp-{{$color[$check%6]}}">
                                                                <div class="pp-item-number pp-{{$color[$check%6]}}"><span>{{$check}}</span></div>
                                                                <div class="pp-item-content">
                                                                    <h6>{{$detail->main_content}}</h6>
                                                                    <button onclick="detail({{$detail->id}})" class="btn btn-link" data-original-title="" title="">Chi tiết</button>
                                                                    <div id="detail-content-{{$detail->id}}" style="display: none; white-space: pre-line">
                                                                        {{$detail->detail_content}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </section>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="course-details-section pb-70">
        <div class="container">
            <div class="row">
                <div class="fb-comments" data-href="https://worklab.edu.vn/" data-width="" data-numposts="5"></div>
            </div>
        </div>
    </section>
    <div id="fb-root"></div>
    <section class="course-section pt-100 pb-70">
      <div class="container">
        <div class="section-title-group">
          <div class="section-title section-title-shapeless">
            <h2>KHÓA HỌC KHÁC</h2>
          </div>
        </div>
          <div class="row">
            @foreach($listCourseDaoTao as $key => $course)
              <div class="col-12 col-md-6 col-lg-4 pb-30">
                  <div class="course-card">
                      <div class="course-card-thumb">
                          <a href="{{route('chi-tiet-khoa-hoc',['id' => $course->id, 'name' => Str::slug($course->name) ])}}"><img src="{{asset($course->url_homepage_avatar)}}" class="custom-course" alt="course"></a>
                      </div>
                      <div class="course-card-content bg-off-white">
                          <ul class="course-info-list">
                              <li><i class="flaticon-reading"></i> {{$course->number_student}} Students</li>
                              <li><i class="flaticon-online-learning-1"></i> {{$course->day_study}} Lessons</li>
                          </ul>
                          <h3><a href="{{route('chi-tiet-khoa-hoc',['id' => $course->id, 'name' => Str::slug($course->name) ])}}">{{$course->name}}</a></h3>
                      </div>
                      {{-- <ul class="course-filter-list">
                          <li class="course-filter-danger">Featured</li>
                          <li class="course-filter-success">Free</li>
                      </ul> --}}
                  </div>
              </div>
              @endforeach
          </div>
      </div>
  </section>
    <script>
        function detail(id) {
            const element = document.getElementById('detail-content-'+id);
            const display = element.style.display;
            if(display === 'none') {
                element.style.display = 'block'
            } else {
                element.style.display = 'none'
            }
        }
    </script>
@endsection

@section('text', $courseDetail->name)

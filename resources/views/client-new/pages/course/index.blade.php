@extends('client-new.layout.master')

@section('content')
<header class="header-page">
    <div class="header-page-shape header-page-shape-middle">
        <img src="{{ asset('new/images/shapes/shape-15.png')}}" alt="shape">
    </div>
    <div class="container">
        <div class="header-page-content">
            @if($namePage == 1)
                <h1>KHÓA HỌC</h1>
            @else
                <h1>LUYỆN THI CHỨNG CHỈ</h1>
            @endif
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('homepage') }}">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        @if($namePage == 1)
                            Khóa học
                        @else
                            Luyện thi chứng chỉ
                        @endif
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</header>
<section class="course-section pt-100 pb-70">
    <div class="container">
        @foreach($courses as $course)
        <div class="container">
            <div class="section-title-group">
                <div class="section-title section-title-shapeless">
                    <h2>{{$course->name}}</h2>
                </div>
            </div>
            <div class="row">
            @foreach($listCourseDetail as $item)
                @if($course->id == $item->id_course)
                <div class="col-12 col-md-6 col-lg-4 pb-30">
                    <div class="course-card">
                        <div class="course-card-thumb">
                            <img src="{{asset($item->url_homepage_avatar)}}" class="custom-course" alt="course">
                        </div>
                        <div class="course-card-content bg-off-white">
                            <ul class="course-info-list">
                                <li><i class="flaticon-reading"></i> {{$item->number_student}} Students</li>
                                <li><i class="flaticon-online-learning-1"></i> {{$item->day_study}} Lessons</li>
                            </ul>
                            <h3><a href="{{route('chi-tiet-khoa-hoc',['name' => Str::slug($item->name), 'id' => $item->id])}}">{{$item->name}}</a></h3>
                        </div>
                        <ul class="course-filter-list">
                            @if($listDiscount[$item->id] > 0)
                            <li class="course-filter-danger">-{{$listDiscount[$item->id]}}%</li>
                            @endif
                        </ul>
                    </div>
                </div>
                @endif
            @endforeach
            </div>
        </div>
        @endforeach
    </div>
</section>

@endsection

{{-- @section('text', 'Khóa học công nghệ tại WorkLab') --}}

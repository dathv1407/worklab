@extends('client-new.layout.master')

@section('css')
    <style>
      .course-info-list li, .course-location li, .course-entry-list li, .event-entry-list li, .meeting-info-list li {
        margin-right: 9px;
        font-size: 14px;
      }
      .cards-custom > .card {
        border-bottom: 1px solid lightgray;
      }
    </style>
@endsection

@section('content')
<header>
    <div
      id="rev_slider_26_1_wrapper"
      class="rev_slider_wrapper fullscreen-container header-revolution"
      data-alias="mask-showcase"
      data-source="gallery"
      style="background: #aaaaaa; padding: 0px"
    >
      <div
        id="rev_slider_26_1"
        class="rev_slider fullscreenbanner tiny_bullet_slider"
        style="display: none"
        data-version="5.4.1"
      >
        <ul>
          <li
            data-index="rs-1"
            data-transition="zoomout"
            data-slotamount="default"
            data-hideafterloop="0"
            data-hideslideonmobile="off"
            data-easein="default"
            data-easeout="default"
            data-masterspeed="2000"
            data-thumb="http://works.themepunch.com/revolution_5_3/wp-content/"
            data-rotate="0"
            data-saveperformance="off"
            data-title="Slide"
          >
            <img
              src="{{ asset('assets/images/new/slider/01.png') }}"
              style="
                background: linear-gradient(
                  90deg,
                  rgba(134, 143, 150, 1) 0%,
                  rgba(89, 97, 100, 1) 100%
                );
              "
              alt=""
              data-bgposition="center center"
              data-bgfit="cover"
              data-bgrepeat="no-repeat"
              class="rev-slidebg"
              data-no-retina
              data-bgparallax="10"
            />

            <div
              class="tp-caption header-overlay tp-shape tp-shapewrapper"
              id="slide-1-layer-1"
              data-x="['right','right','right','right']"
              data-hoffset="['0','0','0','0']"
              data-y="['top','top','top','top']"
              data-voffset="['0','0','0','0']"
              data-width="['1200']"
              data-height="full"
              data-whitespace="nowrap"
              data-type="shape"
              data-basealign="slide"
              data-responsive_offset="on"
              data-responsive="off"
              data-frames='[{"from":"opacity:0;","speed":1500,"to":"o:1;","delay":750,"ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"ease":"nothing"}]'
              data-textAlign="['left','left','left','left']"
              style="
                z-index: 5;
                background-image: linear-gradient(
                  267deg,
                  rgba(255, 255, 255, 0.9) 65%,
                  rgba(255, 255, 255, 0.8) 72%,
                  rgba(255, 255, 255, 0.8) 59%,
                  transparent 87%
                );
              "
            ></div>

            <div
              class="tp-caption header-small-text tp-resizeme"
              id="slide-1-layer-2"
              data-x="['left','center','center','center']"
              data-hoffset="['800','0','0','0']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['-187','-177','-177','-200']"
              data-width="none"
              data-fontsize="['17','17','15','14']"
              data-height="none"
              data-whitespace="nowrap"
              data-type="text"
              data-responsive_offset="on"
              data-textAlign="['left','center','center','center']"
              data-frames='[{"delay":750,"speed":750,"sfxcolor":"#0067da","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-paddingtop="[0,0,0,0]"
              data-paddingright="[20,20,20,20]"
              data-paddingbottom="[0,0,0,0]"
              data-paddingleft="[20,20,20,20]"
              style="z-index: 7; white-space: nowrap"
            >
              <i class="flaticon-open-book"></i> WORKLAB
            </div>

            <div
              class="tp-caption header-title tp-resizeme"
              id="slide-1-layer-3"
              data-x="['left','center','center','center']"
              data-hoffset="['800','0','0','0']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['-50','-50','-50','-90']"
              data-fontsize="['80','70','70','45']"
              data-lineheight="['90','70','70','60']"
              data-width="['700','650','620','420']"
              data-height="none"
              data-whitespace="normal"
              data-type="text"
              data-responsive_offset="on"
              data-frames='[{"delay":1000,"speed":750,"sfxcolor":"#0067da","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['left','center','center','center']"
              data-paddingtop="[0,0,0,0]"
              data-paddingright="[20,20,20,20]"
              data-paddingbottom="[30,30,30,30]"
              data-paddingleft="[20,20,20,20]"
              style="
                z-index: 8;
                min-width: 650px;
                max-width: 650px;
                white-space: normal;
              "
            >
              <h1>Ưu đãi 10-50% học phí các khóa học tại WORKLAB</h2>
            </div>

            <div
              class="tp-caption header-paragraph tp-resizeme"
              id="slide-1-layer-4"
              data-x="['left','center','center','center']"
              data-hoffset="['800','0','0','0']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['80','60','60','30']"
              data-width="['700','650','620','380']"
              data-lineheight="['30','30','30','30']"
              data-fontsize="['20','20','16','15']"
              data-height="none"
              data-whitespace="normal"
              data-type="text"
              data-responsive_offset="on"
              data-frames='[{"delay":1500,"speed":750,"sfxcolor":"#0067da","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['left','center','center','center']"
              data-paddingtop="[0,0,0,0]"
              data-paddingright="[20,20,20,20]"
              data-paddingbottom="[0,0,0,0]"
              data-paddingleft="[20,20,20,20]"
              style="z-index: 7; white-space: normal"
            >
            Chúng tôi luôn có các chương trình ưu đãi học phí hợp lý, giúp học viên có đủ cơ hội được học tập những chương trình đào tạo chất lượng nhất.
            </div>

            <div
              class="tp-caption main-btn btn tp-resizeme"
              id="slide-1-layer-5"
              data-x="['left','center','left','left']"
              data-hoffset="['820','-110','160','25']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['170','150','150','130']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-type="button"
              data-responsive_offset="on"
              data-frames='[{"delay":1800,"speed":1000,"sfxcolor":"#0067da","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']"
              data-paddingtop="[10,0,0,0]"
              data-paddingright="[25,30,30,30]"
              data-paddingbottom="[10,0,0,0]"
              data-paddingleft="[25,30,30,30]"
              style="
                z-index: 5;
                white-space: nowrap;
                line-height: 50px;
                letter-spacing: 0;
              "
            >
              <a href="{{ route('lich-khai-giang-moi-nhat') }}">LỊCH HỌC MỚI NHẤT <i class="flaticon-edit"></i></a>
            </div>

            <div
              class="tp-caption main-btn btn btn-white tp-resizeme"
              id="slide-1-layer-6"
              data-x="['left','center','left','left']"
              data-hoffset="['1060','120','410','260']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['170','150','150','130']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-type="button"
              data-responsive_offset="on"
              data-frames='[{"delay":2100,"speed":1000,"sfxcolor":"#0067da","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']"
              data-paddingtop="[10,0,0,0]"
              data-paddingright="[25,30,30,30]"
              data-paddingbottom="[10,0,0,0]"
              data-paddingleft="[25,30,30,30]"
              style="
                z-index: 5;
                white-space: nowrap;
                line-height: 50px;
                letter-spacing: 0;
              "
            >
              <a href="{{route('dao-tao-chuyen-de', ['id' => 'dao-tao'])}}"
                >KHÓA HỌC MỚI NHẤT <i class="flaticon-online-learning"></i
              ></a>
            </div>
          </li>

          <li
            data-index="rs-2"
            data-transition="zoomout"
            data-slotamount="default"
            data-hideafterloop="0"
            data-hideslideonmobile="off"
            data-easein="default"
            data-easeout="default"
            data-masterspeed="2000"
            data-thumb="http://works.themepunch.com/revolution_5_3/wp-content/"
            data-rotate="0"
            data-saveperformance="off"
            data-title="Slide"
          >
            <img
              src="{{ asset('assets/images/new/slider/02.png') }}"
              style="
                background: linear-gradient(
                  90deg,
                  rgba(134, 143, 150, 1) 0%,
                  rgba(89, 97, 100, 1) 100%
                );
              "
              alt=""
              data-bgposition="center center"
              data-bgfit="cover"
              data-bgrepeat="no-repeat"
              class="rev-slidebg"
              data-no-retina
              data-bgparallax="10"
            />

            <div
              class="tp-caption header-overlay tp-shape tp-shapewrapper"
              id="slide-2-layer-1"
              data-x="['left','left','left','left']"
              data-hoffset="['0','0','0','0']"
              data-y="['top','top','top','top']"
              data-voffset="['0','0','0','0']"
              data-width="['1200']"
              data-height="full"
              data-whitespace="nowrap"
              data-type="shape"
              data-basealign="slide"
              data-responsive_offset="on"
              data-responsive="off"
              data-frames='[{"from":"opacity:0;","speed":1500,"to":"o:1;","delay":750,"ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"ease":"nothing"}]'
              data-textAlign="['left','left','left','left']"
              style="
                z-index: 5;
                background-image: linear-gradient(
                  -267deg,
                  rgba(255, 255, 255, 0.9) 65%,
                  rgba(255, 255, 255, 0.8) 72%,
                  rgba(255, 255, 255, 0.8) 59%,
                  transparent 87%
                );
              "
            ></div>

            <div
              class="tp-caption header-small-text tp-resizeme"
              id="slide-2-layer-2"
              data-x="['right','center','center','center']"
              data-hoffset="['800','0','0','0']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['-187','-177','-177','-200']"
              data-width="none"
              data-fontsize="['17','17','15','14']"
              data-height="none"
              data-whitespace="nowrap"
              data-type="text"
              data-responsive_offset="on"
              data-textAlign="['right','center','center','center']"
              data-frames='[{"delay":750,"speed":750,"sfxcolor":"#0067da","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-paddingtop="[0,0,0,0]"
              data-paddingright="[20,20,20,20]"
              data-paddingbottom="[0,0,0,0]"
              data-paddingleft="[20,20,20,20]"
              style="z-index: 7; white-space: nowrap"
            >
              <i class="flaticon-open-book"></i> WORKLAB
            </div>

            <div
              class="tp-caption header-title tp-resizeme"
              id="slide-2-layer-3"
              data-x="['right','center','center','center']"
              data-hoffset="['800','0','0','0']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['-50','-50','-50','-90']"
              data-fontsize="['80','70','70','45']"
              data-lineheight="['80','70','70','60']"
              data-width="['700','650','620','420']"
              data-height="none"
              data-whitespace="normal"
              data-type="text"
              data-responsive_offset="on"
              data-frames='[{"delay":1000,"speed":750,"sfxcolor":"#0067da","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['right','center','center','center']"
              data-paddingtop="[0,0,0,0]"
              data-paddingright="[20,20,20,20]"
              data-paddingbottom="[30,30,30,30]"
              data-paddingleft="[20,20,20,20]"
              style="
                z-index: 8;
                min-width: 650px;
                max-width: 650px;
                white-space: normal;
              "
            >
              <h2><h1>Các khóa học chất lượng và  hiệu quả cho học viên</h2></h2>
            </div>

            <div
              class="tp-caption header-paragraph tp-resizeme"
              id="slide-2-layer-4"
              data-x="['right','center','center','center']"
              data-hoffset="['800','0','0','0']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['80','60','60','30']"
              data-width="['700','650','620','380']"
              data-lineheight="['30','30','30','30']"
              data-fontsize="['20','20','16','15']"
              data-height="none"
              data-whitespace="normal"
              data-type="text"
              data-responsive_offset="on"
              data-frames='[{"delay":1500,"speed":750,"sfxcolor":"#0067da","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['right','center','center','center']"
              data-paddingtop="[0,0,0,0]"
              data-paddingright="[20,20,20,20]"
              data-paddingbottom="[0,0,0,0]"
              data-paddingleft="[20,20,20,20]"
              style="z-index: 7; white-space: normal"
            >
            Các khóa học tại WorkLab luôn luôn được cải tiến giáo trình, phương pháp giảng dạy hướng tới đào tạo kỹ năng thực hành theo các dự án thực tế.
            </div>

            <div
              class="tp-caption main-btn btn tp-resizeme"
              id="slide-2-layer-5"
              data-x="['left','center','left','left']"
              data-hoffset="['465','-110','160','25']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['170','150','150','130']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-type="button"
              data-responsive_offset="on"
              data-frames='[{"delay":1800,"speed":1000,"sfxcolor":"#0067da","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']"
              data-paddingtop="[10,0,0,0]"
              data-paddingright="[25,30,30,30]"
              data-paddingbottom="[10,0,0,0]"
              data-paddingleft="[25,30,30,30]"
              style="
                z-index: 5;
                white-space: nowrap;
                line-height: 50px;
                letter-spacing: 0;
                margin-left: 45px;
              "
            >
              <a href="{{ route('lich-khai-giang-moi-nhat') }}">LỊCH HỌC MỚI NHẤT <i class="flaticon-edit"></i></a>
            </div>

            <div
              class="tp-caption main-btn btn btn-white tp-resizeme"
              id="slide-2-layer-6"
              data-x="['left','center','left','left']"
              data-hoffset="['260','120','410','260']"
              data-y="['middle','middle','middle','middle']"
              data-voffset="['170','150','150','130']"
              data-width="none"
              data-height="none"
              data-whitespace="nowrap"
              data-type="button"
              data-responsive_offset="on"
              data-frames='[{"delay":2100,"speed":1000,"sfxcolor":"#0067da","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']"
              data-paddingtop="[10,0,0,0]"
              data-paddingright="[25,30,30,30]"
              data-paddingbottom="[10,0,0,0]"
              data-paddingleft="[25,30,30,30]"
              style="
                z-index: 5;
                white-space: nowrap;
                line-height: 50px;
                letter-spacing: 0;
              "
            >
              <a href="{{route('dao-tao-chuyen-de', ['id' => 'dao-tao'])}}"
                >KHÓA HỌC MỚI NHẤT <i class="flaticon-online-learning"></i
              ></a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </header>

  <div
    class="
      about-section
      bg-off-white
      pt-100
      pb-70
      position-relative
      overflow-hidden
    "
  >
    <div class="background-shapes">
      <div class="background-shape-item">
        <img src="assets/images/shapes/curved-line.png" alt="line" />
      </div>
      <div class="background-shape-item">
        <img src="assets/images/shapes/half-circle-shape.png" alt="line" />
      </div>
    </div>
    <div class="animate-shapes">
      <div class="animate-shape animation-tab-none">
        <img src="assets/images/shapes/animate-shape-1.png" alt="shape" />
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-6 col-lg-3 pb-30">
          <div class="about-item-card">
            <img
              src="assets/images/shapes/about-grid-icon.png"
              class="about-card-image"
              alt="icon"
            />
            <div class="about-card-icon">
              <i class="flaticon-book-of-black-cover-closed"></i>
              <i class="flaticon-mortarboard"></i>
            </div>
            <div class="about-card-text">
              <h4>Chương trình học kiến thức giống khi đi làm</h4>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3 pb-30">
          <div class="about-item-card">
            <img
              src="assets/images/shapes/about-grid-icon.png"
              class="about-card-image"
              alt="icon"
            />
            <div class="about-card-icon">
              <i class="flaticon-laptop"></i>
              <i class="flaticon-web"></i>
            </div>
            <div class="about-card-text">
              <h4>Hình thức học đa dạng (online/offline)</h4>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3 pb-30">
          <div class="about-item-card">
            <img
              src="assets/images/shapes/about-grid-icon.png"
              class="about-card-image"
              alt="icon"
            />
            <div class="about-card-icon">
              <i class="flaticon-pencil"></i>
              <i class="flaticon-agenda"></i>
            </div>
            <div class="about-card-text">
              <h4>Giảng viên hướng dẫn giàu kinh nghiệm</h4>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3 pb-30">
          <div class="about-item-card">
            <img
              src="assets/images/shapes/about-grid-icon.png"
              class="about-card-image"
              alt="icon"
            />
            <div class="about-card-icon">
              <i class="flaticon-monitor"></i>
              <i class="flaticon-address-book"></i>
            </div>
            <div class="about-card-text">
              <h4>Support hỗ trợ trong suốt khóa học
              </h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section class="about-section pt-100 pb-70">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-lg-5 pb-30">
          <div class="section-title section-title-left text-start">
            <h2>PHƯƠNG PHÁP HỌC TẬP TẠI WORKLAB</h2>
            <p>
              Chúng tôi luôn luôn nỗ lực không ngừng trong việc cải tiến chất lượng đào tạo, cải tiến phương pháp giảng dạy để đem lại hiệu quả tốt nhất cho học viên
            </p>
          </div>
          <div class="faq-accordion">
            <div
              class="
                faq-accordion-item
                faq-accordion-item-bgless
              "
            >
              <div class="faq-accordion-header">
                <h3 class="faq-accordion-title">
                  Hình thức học online
                </h3>
              </div>
              <div class="faq-accordion-body">
                <div class="faq-accordion-body-inner">
                  <p>
                    Các khóa học online tại WorkLab luôn được đầu tư chú trọng đảm bảo chất lượng, đảm bảo học online như học offline. Giảng viên/trợ giảng support tối đa trong suốt quá trình học
                  </p>
                </div>
              </div>
            </div>
            <div class="faq-accordion-item faq-accordion-item-bgless">
              <div class="faq-accordion-header">
                <h3 class="faq-accordion-title">
                  Hình thức học offline
                </h3>
              </div>
              <div class="faq-accordion-body">
                <div class="faq-accordion-body-inner">
                  <p>
                    Bên cạnh việc học online, các khóa học offline tại Worklab được tổ chức khoa học, hiệu quả. Trang thiết bị phòng Lab hiện đại đáp ứng nhu cầu học tập của học viên. Giảng viên/trợ giảng support tối đa trong suốt quá trình học
                  </p>
                </div>
              </div>
            </div>
            <div class="faq-accordion-item faq-accordion-item-bgless faq-accordion-item-active">
              <div class="faq-accordion-header">
                <h3 class="faq-accordion-title">
                  Tăng cường thực hành trong các khóa học
                </h3>
              </div>
              <div class="faq-accordion-body">
                <div class="faq-accordion-body-inner">
                  <p>
                    Các khóa học tại Worklab được thiết kế theo hướng tăng thời lượng các buổi thực hành trên dự án thực tế. Giúp học viên có đủ kỹ năng làm việc chỉ sau một khóa học
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 offset-lg-1 pb-30">
          <div class="about-image-content">
            <div class="about-bg-shapes">
              <div class="about-bg-shape-item">
                <img
                  src="assets/images/shapes/vertical-zigzag-line.png"
                  alt="shape"
                />
              </div>
            </div>
            <div class="about-shape-items">
              <div class="about-shape-item animation-tab-none">
                <img
                  src="assets/images/shapes/square-dotted-shape.png"
                  alt="shape"
                />
              </div>
            </div>
            <div class="about-image-grid">
              <div class="about-image-item">
                <img src="assets/images/new/homepage/homepage_pp1.jpg" alt="about" />
              </div>
              <div class="about-image-item">
                <img src="assets/images/new/homepage/homepage_pp2.jpg" alt="about" />
              </div>
              <div class="about-image-item">
                <img src="assets/images/new/homepage/homepage_pp3.jpg" alt="about" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="course-section p-tb-100 position-relative bg-off-white">
    <div class="course-animation-items">
      <div class="course-animation-item animation-tab-none">
        <img src="assets/images/shapes/curve-lines-shape.png" alt="shape" />
      </div>
      <div class="course-animation-item animation-tab-none">
        <img src="assets/images/shapes/cloud-shape.png" alt="shape" />
      </div>
    </div>
    <div class="container">
      <div class="section-title">
        <h2>KHÓA HỌC</h2>
        <p>
          Khóa học ngắn hạn, dài hạn dành cho sinh viên, người đi làm
        </p>
      </div>
      <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          @for($count1 = 1; $count1 <= (int)ceil(count($listCourseDaoTao)/4); $count1++)
                <div class="carousel-item {{$count1 == 1 ? 'active' : ''}}">
                    <div class="cards-wrapper cards-custom">
                        @foreach($listCourseDaoTao as $key => $course)
                            @if($key+1 > ($count1-1)*4 and $key+1 <= $count1*4)
                            <div class="card {{$key%4 != 0 ? 'd-none d-md-block' : ''}}">
                                <div class="course-card-thumb">
                                    <a href="{{route('chi-tiet-khoa-hoc',['id' => $course->id, 'name' => Str::slug($course->name) ])}}"><img src="{{$course->url_homepage_avatar}}" class="custom-course" alt="course"></a>
                                </div>
                                <div class="course-card-content">
                                    <ul class="course-info-list">
                                        <li><i class="flaticon-reading"></i> {{$course->number_student}} Students</li>
                                        <li>
                                            <i class="flaticon-online-learning-1"></i> {{$course->day_study}} Lessons
                                        </li>
                                    </ul>
                                    <h3>
                                        <a href="{{route('chi-tiet-khoa-hoc',['id' => $course->id, 'name' => Str::slug($course->name) ])}}">{{$course->name}}</a>
                                    </h3>

                                </div>
                                <ul class="course-filter-list">
                                  @if($listDiscount[$course->id] > 0)
                                    <li class="course-filter-danger">-{{$listDiscount[$course->id]}}%</li>
                                  @endif
                              </ul>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
          @endfor
        </div>
        <div class="owl-nav">
          <button type="button" role="button" href="#carouselExampleControls" class="carousel-control-prev" data-slide="prev">
          <span aria-label="Previous">‹</span>
          </button>
          <button type="button" role="button" href="#carouselExampleControls" class="carousel-control-next" data-slide="next">
              <span aria-label="Next">›</span>
          </button>
        </div>
      </div>
    </div>
  </section>
<section class="course-section p-tb-100 position-relative bg-off-white">
    <div class="course-animation-items">
        <div class="course-animation-item animation-tab-none">
            <img src="assets/images/shapes/curve-lines-shape.png" alt="shape" />
        </div>
        <div class="course-animation-item animation-tab-none">
            <img src="assets/images/shapes/cloud-shape.png" alt="shape" />
        </div>
    </div>
    <div class="container">
        <div class="section-title">
            <h2>LUYỆN THI CHỨNG CHỈ</h2>
            <p>
                Khóa học luyện thi chứng chỉ quốc tế
            </p>
        </div>
        <div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @for($count2 = 1; $count2 <= (int)ceil(count($listCourseChungChi)/4); $count2++)
                    <div class="carousel-item {{$count2 == 1 ? 'active' : ''}}">
                        <div class="cards-wrapper cards-custom">
                            @foreach($listCourseChungChi as $key2 => $item)
                                @if($key2+1 > ($count2-1)*4 and $key2+1 <= $count2*4)
                                    <div class="card {{$key2%4 != 0 ? 'd-none d-md-block' : ''}}">
                                        <div class="course-card-thumb">
                                            <a href="{{route('chi-tiet-khoa-hoc',['id' => $item->id, 'name' => Str::slug($course->name) ])}}"><img src="{{$item->url_homepage_avatar}}" class="custom-course" alt="course"></a>
                                        </div>
                                        <div class="course-card-content">
                                            <ul class="course-info-list">
                                                <li><i class="flaticon-reading"></i> {{$item->number_student}} Students</li>
                                                <li>
                                                    <i class="flaticon-online-learning-1"></i> {{$item->day_study}} Lessons
                                                </li>
                                            </ul>
                                            <h3>
                                                <a href="{{route('chi-tiet-khoa-hoc',['id' => $item->id, 'name' => Str::slug($course->name) ])}}">{{$item->name}}</a>
                                            </h3>

                                        </div>
                                        {{--<ul class="course-filter-list">
                                            <li class="course-filter-danger">Featured</li>
                                        </ul>--}}
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endfor
            </div>
            <div class="owl-nav">
                <button type="button" role="button" href="#carouselExampleControls2" class="carousel-control-prev" data-slide="prev">
                    <span aria-label="Previous">‹</span>
                </button>
                <button type="button" role="button" href="#carouselExampleControls2" class="carousel-control-next" data-slide="next">
                    <span aria-label="Next">›</span>
                </button>
            </div>
        </div>
    </div>
</section>
  {{--<section class="course-section p-tb-100 position-relative bg-off-white pd-0">
    <div class="course-animation-items">
      <div class="course-animation-item animation-tab-none">
        <img src="assets/images/shapes/curve-lines-shape.png" alt="shape" />
      </div>
      <div class="course-animation-item animation-tab-none">
        <img src="assets/images/shapes/cloud-shape.png" alt="shape" />
      </div>
    </div>
    <div class="container">
      <div class="section-title">
        <h2>LUYỆN THI CHỨNG CHỈ</h2>
        <p>
          Khóa học luyện thi chứng chỉ quốc tế
        </p>
      </div>
      <div class="course-carousel swiper-container swiper-container-initialized swiper-container-horizontal">
        <div class="swiper-wrapper" id="swiper-wrapper-13b42fc193bcadf6" aria-live="polite" style="transform: translate3d(-1326px, 0px, 0px); transition: all 0ms ease 0s;"><div class="course-carousel-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" role="group" aria-label="1 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-1.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 23 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 30 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Listening Skills the Ultimate Soft Skills for
                    Beginners</a>
                </h3>

              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">Free</li>
              </ul>
            </div>
          </div><div class="course-carousel-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="1" role="group" aria-label="2 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-2.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 16 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 45 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Yoga Medicine’s Guide to Therapeutic Man &amp; Woman</a>
                </h3>
              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">Free</li>
              </ul>
            </div>
          </div><div class="course-carousel-item swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="2" role="group" aria-label="3 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-3.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 16 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 45 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Excel Crash Course: Master Excel for Financial
                    Analysis</a>
                </h3>
              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">-50%</li>
              </ul>
            </div>
          </div>
          <div class="course-carousel-item swiper-slide swiper-slide-active" data-swiper-slide-index="0" role="group" aria-label="4 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-1.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 23 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 30 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Listening Skills the Ultimate Soft Skills for
                    Beginners</a>
                </h3>

              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">Free</li>
              </ul>
            </div>
          </div>
          <div class="course-carousel-item swiper-slide swiper-slide-next" data-swiper-slide-index="1" role="group" aria-label="5 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-2.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 16 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 45 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Yoga Medicine’s Guide to Therapeutic Man &amp; Woman</a>
                </h3>
              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">Free</li>
              </ul>
            </div>
          </div>
          <div class="course-carousel-item swiper-slide swiper-slide-duplicate-prev" data-swiper-slide-index="2" role="group" aria-label="6 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-3.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 16 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 45 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Excel Crash Course: Master Excel for Financial
                    Analysis</a>
                </h3>
              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">-50%</li>
              </ul>
            </div>
          </div>
        <div class="course-carousel-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" role="group" aria-label="7 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-1.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 23 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 30 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Listening Skills the Ultimate Soft Skills for
                    Beginners</a>
                </h3>

              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">Free</li>
              </ul>
            </div>
          </div><div class="course-carousel-item swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="1" role="group" aria-label="8 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-2.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 16 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 45 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Yoga Medicine’s Guide to Therapeutic Man &amp; Woman</a>
                </h3>
              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">Free</li>
              </ul>
            </div>
          </div><div class="course-carousel-item swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" role="group" aria-label="9 / 9" style="width: 412px; margin-right: 30px;">
            <div class="course-card">
              <div class="course-card-thumb">
                <a href="single-certificate.html"><img src="assets/images/new/courses/course-image-3.jpg" class="custom-course" alt="course"></a>
              </div>
              <div class="course-card-content bg-gray">
                <ul class="course-info-list">
                  <li><i class="flaticon-reading"></i> 16 Students</li>
                  <li>
                    <i class="flaticon-online-learning-1"></i> 45 Lessons
                  </li>
                </ul>
                <h3>
                  <a href="single-certificate.html">Excel Crash Course: Master Excel for Financial
                    Analysis</a>
                </h3>
              </div>
              <ul class="course-filter-list">
                <li class="course-filter-danger">Featured</li>
                <li class="course-filter-success">-50%</li>
              </ul>
            </div>
          </div></div>
        <div class="swiper-carousel-control">
          <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-13b42fc193bcadf6"></div>
          <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-13b42fc193bcadf6"></div>
        </div>
      <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
    </div>
  </section>--}}

  <section class="help-section pt-100 pb-70">
    <div class="container">
      <div class="section-title">
        <h2>CHÚNG TÔI ĐEM ĐẾN CHO BẠN</h2>
      </div>
      <div class="row">
        <div class="col-12 col-sm-6 col-lg-3 pb-30">
          <div class="help-card help-card-center">
            <div class="help-card-thumb">
              <i class="flaticon-resume"></i>
            </div>
            <div class="help-card-content">
              <h3>Khóa học chất lượng </h3>
              <p>
                Nội dung giảng dạy chất lượng, luôn được cập nhật, cải tiến
              </p>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3 pb-30">
          <div class="help-card help-card-center">
            <div class="help-card-thumb">
              <i class="flaticon-satisfaction"></i>
            </div>
            <div class="help-card-content">
              <h3>Phương pháp giảng học tập</h3>
              <p>
                Phương pháp giảng dạy khoa học, tập trung vào kỹ năng thực hành.
              </p>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3 pb-30">
          <div class="help-card help-card-center">
            <div class="help-card-thumb">
              <i class="flaticon-instructions"></i>
            </div>
            <div class="help-card-content">
              <h3>Học là làm được việc​</h3>
              <p>
                Đảm bảo học viên có đủ kỹ năng để làm việc thực tế
              </p>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-6 col-lg-3 pb-30">
          <div class="help-card help-card-center">
            <div class="help-card-thumb">
              <i class="flaticon-learn"></i>
            </div>
            <div class="help-card-content">
              <h3>Cơ hội việc làm ​​</h3>
              <p>
                Hỗ trợ tối đa học viên trong quá trình tìm kiếm việc làm sau khóa học
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="course-section pt-100 pb-70">
    <div class="container">
      <div class="section-title-group">
        <div class="section-title section-title-shapeless">
          <h2>TIN TỨC NỔI BẬT</h2>
        </div>
        <a href="events.html" class="btn main-btn"
          >VIEW ALL EVENTS <i class="flaticon-edit"></i
        ></a>
      </div>
      <div class="row">
          @foreach($latestNews as $news)
              <div class="col-12 col-md-6 col-lg-4 pb-30">
                  <div class="course-flat-card">
                      <div class="course-card-thumb">
                          <a href="{{route('detail-news', ['id' => $news->id, 'slug' => $news->slug])}}">
                              <img src="{{asset('uploads/'.$news->url_img)}}" class="custom-course" alt="course"/>
                          </a>
                      </div>
                      <div class="course-card-content">
                          <ul class="course-location">
                              <li><i class="flaticon-placeholder"></i> VietNam</li>
                          </ul>
                          <h3>{{$news->headline}}</h3>
                          <p>
                              <a href="{{route('detail-news', ['id' => $news->id, 'slug' => $news->slug])}}" class="redirect-link"
                              >Learn More <i class="flaticon-right-arrow"></i
                                  ></a>
                          </p>
                      </div>
                      <ul class="course-filter-list">
                          <li class="course-filter-danger">{{date_format($news->created_at,"Y/m/d")}}</li>
                      </ul>
                  </div>
              </div>
          @endforeach
      </div>
    </div>
  </section>
  <section class="course-section pt-100 pb-70">
    <div class="container">
      <div class="section-title-group">
        <div class="section-title section-title-shapeless">
          <h2>KIẾN THỨC CÔNG NGHỆ</h2>
        </div>
        <a href="events.html" class="btn main-btn"
          >VIEW ALL EVENTS <i class="flaticon-edit"></i
        ></a>
      </div>
      <div class="row">
          @foreach($latestShareKnowledge as $shareKnowledge)
              <div class="col-12 col-md-6 col-lg-4 pb-30">
                  <div class="course-flat-card">
                      <div class="course-card-thumb">
                          <a href="{{route('detail-share-knowledge', ['id' => $shareKnowledge->id, 'slug' => $shareKnowledge->slug])}}">
                              <img src="{{asset($shareKnowledge->url_img)}}" class="custom-course" alt="course"/>
                          </a>
                      </div>
                      <div class="course-card-content">
                          <ul class="course-location">
                              <li><i class="flaticon-placeholder"></i> VietNam</li>
                          </ul>
                          <h3>{{$shareKnowledge->headline}}</h3>
                          <p>
                              <a href="{{route('detail-share-knowledge', ['id' => $shareKnowledge->id, 'slug' => $shareKnowledge->slug])}}" class="redirect-link"
                              >Learn More <i class="flaticon-right-arrow"></i
                                  ></a>
                          </p>
                      </div>
                      <ul class="course-filter-list">
                          <li class="course-filter-danger">{{date_format($shareKnowledge->created_at,"Y/m/d")}}</li>
                      </ul>
                  </div>
              </div>
          @endforeach
      </div>
    </div>
  </section>

  {{-- <section class="partner-section pb-100">
    <div class="container">
      <div class="tab-border-top tab-pt-50">
        <div class="section-title">
          <h2>MẠNG LƯỚI ĐỐI TÁC VỚI WORKLAB</h2>
        </div>
        <div class="partner-carousel swiper-container">
          <div class="swiper-wrapper">
            <div class="partner-item swiper-slide">
              <a href="#"
                ><img
                  src="assets/images/partners/partner-1.png"
                  alt="partner"
              /></a>
            </div>
            <div class="partner-item swiper-slide">
              <a href="#"
                ><img
                  src="assets/images/partners/partner-2.png"
                  alt="partner"
              /></a>
            </div>
            <div class="partner-item swiper-slide">
              <a href="#"
                ><img
                  src="assets/images/partners/partner-3.png"
                  alt="partner"
              /></a>
            </div>
            <div class="partner-item swiper-slide">
              <a href="#"
                ><img
                  src="assets/images/partners/partner-4.png"
                  alt="partner"
              /></a>
            </div>
            <div class="partner-item swiper-slide">
              <a href="#"
                ><img
                  src="assets/images/partners/partner-5.png"
                  alt="partner"
              /></a>
            </div>
            <div class="partner-item swiper-slide">
              <a href="#"
                ><img
                  src="assets/images/partners/partner-6.png"
                  alt="partner"
              /></a>
            </div>
            <div class="partner-item swiper-slide">
              <a href="#"
                ><img
                  src="assets/images/partners/partner-7.png"
                  alt="partner"
              /></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> --}}
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('new/css/bootstrap.css') }}" type="text/css" media="all" />
@endsection

@section('text', 'Đào tạo công nghệ thông tin chất lượng cao')

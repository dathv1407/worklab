<!DOCTYPE html>
<html lang="zxx">
  <head>
    <meta charset="utf-8" />
    <meta name="description" content="Worklab" />
    <meta name="keywords" content="HTML,CSS,JavaScript" />
    <meta name="author" content="Worklab" />
    <meta property="fb:app_id" content="274883790755816" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>WorkLab | {{ $title_page ?? "" }} @yield('text')</title>
    @include('client-new.components.style')
    @yield('css')
  </head>
  <body>
   @include('client-new.components.navbar')

    @yield('content')

    @include('client-new.components.footer')

    @include('client-new.components.script')
    @yield('script')

  </body>

</html>

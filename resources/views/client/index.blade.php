<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Học thiết kế đồ họa, học lập trình tại WorkLab</title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" type="image/png" href="/logo-favicon.jpg"/>
    <link rel="icon" type="image/png" href="/logo-favicon.jpg"/>
    <link rel="stylesheet" href="/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="/css/form-tu-van.css">
    <link rel="stylesheet" href="/owl/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/owl/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/slick/slick.css">
    <link rel="stylesheet" href="/slick/slick-theme.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <!-- <link rel="stylesheet" href="http://itplus-academy.edu.vn/public/css/monokai-sublime.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src="//code.jquery.com/jquery.js"></script>
    <style type="text/css">

    </style>
    <style>
        .gradient-bg {
            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#25a7db+34,ffffff+100&1+0,0+77 */
            background: -moz-linear-gradient(top, rgba(37, 167, 219, 1) 0%, rgba(37, 167, 219, 0.56) 34%, rgba(179, 224, 242, 0) 77%, rgba(255, 255, 255, 0) 100%);
            /* FF3.6-15 */
            background: -webkit-linear-gradient(top, rgba(37, 167, 219, 1) 0%, rgba(37, 167, 219, 0.56) 34%, rgba(179, 224, 242, 0) 77%, rgba(255, 255, 255, 0) 100%);
            /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, rgba(37, 167, 219, 1) 0%, rgba(37, 167, 219, 0.56) 34%, rgba(179, 224, 242, 0) 77%, rgba(255, 255, 255, 0) 100%);
            /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#25a7db', endColorstr='#00ffffff', GradientType=0);
        }

        #forkid .slick-slide {
            padding: 34px;
        }

        .forkid {
            padding: 50px 0
        }

        @media screen and (max-width: 768px) {

            #chuyende .slick-slide,
            #forkid .slick-slide {
                padding: 9px;
            }
        }
    </style>
    <script>
        $(function () {
            $('.lazy').lazy();
        });

    </script>
</head>

<body id="wrapper">
    <div class="overlay"></div>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=1213750372079302";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <script>
        // Menu mobile
        $(document).ready(function () {

            $('.dropdown a').on("click", function (e) {
                $(this).next('ul').toggle();
                e.stopPropagation();
                // e.preventDefault();
            });


            var trigger = $('.hamburger'),
                overlay = $('.overlay'),
                isClosed = false;

            trigger.click(function () {
                hamburger_cross();
            });

            function hamburger_cross() {

                if (isClosed == true) {
                    overlay.hide();
                    trigger.removeClass('is-open');
                    trigger.addClass('is-closed');
                    isClosed = false;
                } else {
                    overlay.show();
                    trigger.removeClass('is-closed');
                    trigger.addClass('is-open');
                    isClosed = true;
                }
            }

            $('[data-toggle="offcanvas"]').click(function () {
                $('#wrapper').toggleClass('toggled');
            });
        });

        //End menu mobile

        $(document).ready(function () {
            var lastScrollTop = 0;
            $(window).scroll(function () {
                var st = $(this).scrollTop();
                if (st > lastScrollTop) {
                    $('#menufix').removeClass('fix', 1000);
                } else {
                    // upscroll code
                    if ($(this).scrollTop() > 100) {
                        $('#menufix').addClass('fix', function () {

                            //$("#menufix").css('display', 'none');
                            //$("#menufix").fadeIn();

                        });
                    } else
                        $('#menufix').removeClass('fix', 1000);
                }
                lastScrollTop = st;

            });
            $(".dropdown-toggle").click(function (event) {
                window.location.assign($(this).attr('href'));
            });
            $('#chuyende').slick({
                slidesToShow: 4,
                lazyLoad: 'ondemand',
                slidesToScroll: 4,
                arrows: true,
                nextArrow: '<i class="arrow-right fa fa-angle-right fa-3x" aria-hidden="true"></i>',
                prevArrow: '<i class="arrow-left fa fa-angle-left fa-3x" aria-hidden="true"></i>',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
            $('#forkid').slick({
                slidesToShow: 4,
                lazyLoad: 'ondemand',
                slidesToScroll: 4,
                arrows: true,
                nextArrow: '<i class="arrow-right fa fa-angle-right fa-3x" aria-hidden="true"></i>',
                prevArrow: '<i class="arrow-left fa fa-angle-left fa-3x" aria-hidden="true"></i>',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
            $('#chuyensau').slick({
                slidesToShow: 4,
                slidesToScroll: 4,
                arrows: true,
                lazyLoad: 'ondemand',
                nextArrow: '<i class="arrow-right fa fa-angle-right fa-3x" aria-hidden="true"></i>',
                prevArrow: '<i class="arrow-left fa fa-angle-left fa-3x" aria-hidden="true"></i>',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
            $('#truyenthongslide').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                lazyLoad: 'ondemand',
                autoplay: true,
                autoplaySpeed: 3000,
                dots: true,
            });


            $('#doitac').slick({
                lazyLoad: 'ondemand',
                slidesToShow: 6,
                slidesToScroll: 1,
                arrows: true,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 4,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }]
            });
            $("#search").click(function (event) {
                $(this).hide();
                event.preventDefault();
                $("#formsearch").addClass('showform');
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            if ($(window).width() <= 1024) {
                $(".logo-header").css({
                    float: 'right',
                    width: '30%'
                });
                /*$("#mobilenav").addClass('gradient-bg');*/
            }
        });
    </script>
    @include('client.header')
    <style>
        .video-bg {
            background: #fff;
        }

        .video-bg p {
            padding: 20px 10px;
        }

        .nav-tabs>li>a {
            padding: 0;
            /*margin-right:8px;*/
        }

        .nav-tabs {
            border: none;
        }

        .nav-tabs>li.active>a {
            background: none;
            border: none;
            /*transform: scale(1.1,1.1);*/
            /*transition: all 0.2s;*/
        }

        img.img-button-video {
            width: 100%;
            height: 50px;
            border-radius: 5px;
            padding: 5px;
            box-sizing: border-box;
            background: #fff;
        }

        button.btn-video {
            width: 100%;
            height: 50px;
        }

        .panel-default {
            border: none;
        }

        .panel {
            background-color: transparent;
            border: none;
            box-shadow: none;
        }

        .flex-content {
            display: flex;
            justify-content: center;
        }

        .flex-content .col-xs-2 {
            padding-right: 3px;
            padding-left: 3px;
        }

        .btn-video {
            background-color: #d9534f;
            border-color: #d43f3a;
            color: #fff;
            width: 100px;
            padding: 5px 0;
            border-radius: 5px;
            height: 50px;
        }

        .btn-video i.fa.fa-2x.fa-youtube {
            float: left;
            padding-left: 5px;
            padding-top: 5px;
        }

        #doitac2 img {
            width: 100%;
            max-height: 125px;
            margin-bottom: 20px;
        }

        @media screen and (max-width: 768px) {
            .a-quyet {
                border: none;
            }

            .header-text {
                font-size: 1.2em;
                letter-spacing: 2px;
            }

            #doitac2 img {
                max-height: 200px;
            }
        }

    </style>


    @section('content')

    @show
    <footer id="footer" class="gray">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="contact-wrap">
                        <div class="contact-wrap__title">
                            <h3 style="font-weight: bold;">HÀ NỘI</h3>
                        </div>
                        <div class="contact-wrap__content">
                            <div class="contact-wrap__content--bold">
                                <h4 style="font-weight: bold;">Nguyễn Huy Tưởng</h4>
                            </div>
                            <p>LK4-TT1, Số 96-96B Nguyễn Huy Tưởng, Quận Thanh Xuân, Hà Nội.</p>
                            <p><span style="font-weight: bold;">Hotline:</span> 078 924 7333</p>
                            <p><span style="font-weight: bold;">Email:</span> tuyensinh@worklab.edu.vn
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-5" style="overflow: hidden;">
                    <div class="contact-wrap">
                        <div class="contact-wrap__title" style="font-weight: bold; margin-left:22px;">
                            <h3 style="font-weight: bold;">CHAT VỚI CHÚNG TÔI</h3>
                        </div>
                        <div class="contact-wrap__content text-center">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fworklabacademy&tabs=messages&width=400&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="400" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer text-center">
            <h5>Copyright © 2021 All Rights Reserved. Phát triển bởi WORKLAB</h5>
        </div>
    </footer>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/facebook/facebook.css">

    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script type="text/javascript">
        var uniqueBrowserID;



        $(document).ready(function () {


            function detectmob() {
                if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
                    return true;
                } else {
                    return false;
                }
            }

            var t = { delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button") };
            setTimeout(function () {
                $("div.fb-livechat").fadeIn()
            }, 8 * t.delay);
            if (!detectmob()) {
                $(".ctrlq").on("click", function (e) {
                    e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({
                        bottom: 0,
                        opacity: 0
                    }, 2 * t.delay, function () {
                        $(this).hide("slow"), t.button.show()
                    })) : t.button.fadeOut("medium", function () {
                        t.widget.stop().show().animate({
                            bottom: "30px",
                            opacity: 1
                        }, 2 * t.delay), t.overlay.fadeIn(t.delay)
                    })
                })
            }


            // Detect ios 11_x_x affected
            // NEED TO BE UPDATED if new versions are affected
            var ua = navigator.userAgent,
                iOS = /iPad|iPhone|iPod/.test(ua),
                iOS11 = /OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2|OS 11_2_1/.test(ua);

            // ios 11 bug caret position
            if (iOS && iOS11) {

                // Add CSS class to body
                $("body").addClass("iosBugFixCaret");

            }

            var auth = JSON.parse(localStorage.getItem("auth"));
            if (auth && auth.token) {
                handlePermission();
            }


        });
        // (function (d, s, id) {
        //     var js, fjs = d.getElementsByTagName(s)[0];
        //     if (d.getElementById(id)) return;
        //     js = d.createElement(s);
        //     js.id = id;
        //     js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1213750372079302";
        //     fjs.parentNode.insertBefore(js, fjs);
        // }(document, 'script', 'facebook-jssdk'));

        // facebook pixel
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1213750372079302');
        fbq('track', 'PageView');





        function removeStorage(name) {
            try {
                localStorage.removeItem(name);
                localStorage.removeItem(name + '_expiresIn');
            } catch (e) {
                console.log('removeStorage: Error removing key [' + key + '] from localStorage: ' + JSON.stringify(e));
                return false;
            }
            return true;
        }

        /*  getStorage: retrieves a key from localStorage previously set with setStorage().
            params:
                key <string> : localStorage key
            returns:
                <string> : value of localStorage key
                null : in case of expired key or failure
         */
        function getStorage(key) {

            var now = Date.now();  //epoch time, lets deal only with integer
            // set expiration for storage
            var expiresIn = localStorage.getItem(key + '_expiresIn');
            if (expiresIn === undefined || expiresIn === null) {
                expiresIn = 0;
            }

            if (expiresIn < now) {// Expired
                removeStorage(key);
                return null;
            } else {
                try {
                    var value = localStorage.getItem(key);
                    return value;
                } catch (e) {
                    console.log('getStorage: Error reading key [' + key + '] from localStorage: ' + JSON.stringify(e));
                    return null;
                }
            }
        }

        /*  setStorage: writes a key into localStorage setting a expire time
            params:
                key <string>     : localStorage key
                value <string>   : localStorage value
                expires <number> : number of seconds from now to expire the key
            returns:
                <boolean> : telling if operation succeeded
         */
        function setStorage(key, value, expires) {

            if (expires === undefined || expires === null) {
                expires = (24 * 60 * 60);  // default: seconds for 1 day
            } else {
                expires = Math.abs(expires); //make sure it's positive
            }

            var now = Date.now();  //millisecs since epoch time, lets deal only with integer
            var schedule = now + expires * 1000;
            try {
                localStorage.setItem(key, value);
                localStorage.setItem(key + '_expiresIn', schedule);
            } catch (e) {
                console.log('setStorage: Error setting key [' + key + '] in localStorage: ' + JSON.stringify(e));
                return false;
            }
            return true;
        }


        //react truyá»n call Ä‘á»ƒ má»Ÿ modal cv cá»§a react
        function openModalCV(callback) {
            $('#button-open-cv').click(function () {
                callback();
            });
        }

        function handlePermission() {
            navigator.permissions.query({ name: 'geolocation' }).then(function (result) {
                if (result.state == 'granted') {
                    report(result.state);
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else if (result.state == 'prompt') {
                    report(result.state);
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else if (result.state == 'denied') {
                    report(result.state);
                }
            });
        }

        function report(state) {
            console.log('Permission ' + state);
        }


        function showPosition(position) {
            var auth = JSON.parse(localStorage.getItem("auth"));
            if (auth && auth.token) {

                var url = "/api/v3/address-user-info?token=" + auth.token;
                axios
                    .post(url, {
                        long: position.coords.longitude,
                        lat: position.coords.latitude
                    })
                    .then(
                        function () {
                        }
                    )
            }

        }





    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <script src="/sweetalert.js"></script>
    <script src="/owl/owl.carousel.min.js"></script>
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="/slick/slick.min.js"></script>
    <script src="/js/wow.js"></script>
    <script src="/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="/lazy-master/jquery.lazy.min.js"></script>
    <script src="/lazy-master/jquery.lazy.plugins.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
    <script src="/js/main.js"></script>
    <script>

        $(document).ready(function () {


            new WOW().init();
        });
        jQuery(document).ready(function () {

            $('.owl-carousel-top').owlCarousel({
                loop: true,
                autoplay: true,
                items: 1,
                dots: false,
                autoplayTimeout: 4000,
                autoplayHoverPause: true,
                animateOut: 'slideOutDown',
                animateIn: 'flipInX',
            });
            $('.owl-carousel2').owlCarousel({
                loop: true,
                autoplay: true,
                items: 1,
                autoplayTimeout: 4000,
                autoplayHoverPause: true,
                animateOut: 'rollOut',
                animateIn: 'fadeInLeft',
            });

            $('.select2').select2();

            $('#bannercamp').slick();
            jQuery(".chat_fb").click(function () {
                jQuery('.fchat').toggle('slow');
            });

            setInterval(function(){
                $('#txt-new').css('background-color', 'red')
            }, 400);
            setInterval(function(){
                $('#txt-new').css('background-color', 'green')
            }, 800);
        });
    </script>





    <script type="text/javascript">

        /* <![CDATA[ */
        var google_conversion_id = 1006975853;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1006975853/?guid=ON&amp;script=0" />
        </div>
    </noscript>

    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '613420715479114'); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=613420715479114&ev=PageView&noscript=1" /></noscript>
</body>

</html>

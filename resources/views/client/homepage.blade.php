@extends('client.index')
@section('content')
    <div class="owl-carousel2 owl-carousel owl-theme">
        <div class="item ">
            <a href="#">
                <img class="img-responsive" style="width: 100%"
                     src="/images/Cover01.png">
            </a>
        </div>
        <div class="item ">
            <a href="#">
                <img class="img-responsive" style="width: 100%"
                     src="/images/cover02.png">
            </a>
        </div>
        <div class="item ">
            <a href="#">
                <img class="img-responsive" style="width: 100%"
                     src="/images/Cover03.png">
            </a>
        </div>
        <div class="item active">
            <a href="#">
                <img class="img-responsive" style="width: 100%"
                     src="/images/Cover04.png">
            </a>
        </div>
    </div>
    <div id="timkiemnhanh">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center section" style="padding-top: 0;">
                    <h2 class="blue header-text text-uppercase" style="font-size: 16px;letter-spacing: 0.3em;"><i
                                class="fa fa-search"></i> Tìm kiếm khóa học</h2>
                    <div class="bar"></div>
                </div>
            </div>
            <div class="pp-live-search">
                <form action="">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 bg-wrap pp-box-shadow">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <select name="hedaotao" id="hedaotao" data-placeholder="Chọn loại khóa học"
                                                class="select2 form-control">
                                            <option value="">Chọn loại khóa học</option>
                                            <option value="1">Đạo tạo chuyên đề</option>
                                            <option value="4">Luyện thi chứng chỉ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <select name="chuyennganh" data-placeholder="Chọn chuyên ngành" id="chuyennganh"
                                                class="select2 form-control">
                                            <option value="">Chọn chuyên ngành</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select name="khoahoc" id="khoahoc_seach" data-placeholder="Chọn khóa học"
                                                    class="form-control select2">
                                                <option value="">Chọn khóa học</option>
                                            </select>
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-primary btn-block btn-custom"
                                                        id="btnSearchKhoaHoc"><i class="fa fa-search"
                                                                                 style="font-size: 16px;"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="khoahocvue">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center section">
                    <h2 class="blue header-text text-uppercase"><a
                                href="#">Khóa học </a></h2>
                    <p class="subtext text-uppercase">Sinh viên và người đi làm</p>
                    <div class="bar"></div>
                </div>
            </div>
        </div>
        <div class="gray chuyende" style="overflow: hidden;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="chuyende">
                            @foreach($listCourse as $item)
                                <a href="{{route('chi-tiet-khoa-hoc',['name' => Str::slug($item->name), 'id' => $item->id])}}" class="slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="0" style="width: 285px;">
                                    <img class="product dobong" alt="image" src="{{asset($item->url_homepage_avatar)}}" style="opacity: 1;">
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row">
                    {{--<div class="col-md-12 text-center" style="padding-top:20px">
                        <a href='#' class="btn btn-success btn-sm">Xem
                            lịch khai giảng</a>
                    </div>--}}
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center section">
                    <h2 class="blue header-text text-uppercase" style="letter-spacing: .2em;"><a
                                href="#l">LUYỆN THI CHỨNG CHỈ</a></h2>
                    <div class="bar"></div>
                </div>
            </div>
        </div>
        <div class="gray forkid" style="overflow: hidden;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="forkid">
                            <a href="#"><img class="product dobong"
                                             data-lazy="http://itplus-academy.edu.vn/admin/public/uploads/45.jpg" alt=""></a>
                            <a href="#"><img class="product dobong"
                                             data-lazy="http://itplus-academy.edu.vn/admin/public/uploads/34.jpg" alt=""></a>
                            <a href="#"><img class="product dobong"
                                             data-lazy="http://itplus-academy.edu.vn/admin/public/uploads/110.jpg" alt=""></a>
                            <a href="#"><img class="product dobong"
                                             data-lazy="http://itplus-academy.edu.vn/admin/public/uploads/210.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center" style="padding-top:20px">
                        {{--                            <a href='#' class="btn btn-success btn-sm">Xem--}}
                        {{--                                lịch thi</a>--}}
                        {{--                            <a href='#' class="btn btn-success btn-sm">Đăng--}}
                        {{--                                ký học</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div id="tintuc" style="padding-bottom:25px;">
        <div class="container ">
            <div class="row">
                <div class="col-md-12 title-box wow bounce">
                    <p class="title bg-blue"><a href="#"> Tin
                            WORKLAB</a></p>
                </div>
                <div class="col-md-6 bounceimg">
                    <div class="newsone1 dobong" style='padding: 5px 10px 10px 15px; overflow: hidden;'>
                        <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/Untitled-1.jpg"
                             class="img-responsive lazy">
                        <div class="new-detail1">
                            <h4>
                                <a class="yellow"
                                   href="#">
                                    ONLINE TRAINING LẬP TRÌNH: "SQL VÀ NOSQL - TẤT TẦN TẬT VỀ DATABASE TRONG LẬP TRÌNH”
                                </a>
                            </h4>
                            <p>Chuỗi chương trình cung cấp các kiến thức về "Đồ họa & Lập trình" miễn phí dành cho cộng
                                đồng sinh viên vủa ITPlus Academy ngày 24/07/2021 tới đây sẽ đón chào sự quay trở lại
                                với một buổi Online Training về lĩnh vực Lập Trình với chủ đề vô cùng bổ ích: “SQL và
                                NoSQL - tất tần tật về Database trong lập trình”</p>
                            <p><a href="#"
                                  class="btn btn-outlined btn-success btn-sm">Đọc thêm</a></p>
                        </div>
                    </div>
                    <div class="hidden-lg hidden-sm hidden-md" style="margin-top: 2em">
                        <div class="newsone dobong">
                            <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/900x600(11).jpg"
                                 class="img-responsive lazy">
                            <div class="new-detail">
                                <h4>
                                    <a class="yellow"
                                       href="#">
                                        JULY SCHOLARSHIP – ƯU ĐÃI 50% HỌC PHÍ – CHỈ TỪ 5.000.000Đ CHO 01 KHÓA HỌC 06
                                        THÁNG </a>
                                </h4>
                                <p>Khuyến học tháng 07/ 2021: "July Scholarship - Ưu đãi lên đến 50% học phí" dành cho
                                    các khóa học chuyên đề, giảm thêm từ 500.000đ/ học viên cho nhóm 02 người &
                                    1.000.000đ/ học viên cho nhóm 05 người. Các khóa học phù hợp với sinh viên và người
                                    đi làm có mong muốn nâng cao kỹ năng, kiến thức "Lập Trình" & "Thiết Kế Đồ Họa -
                                    Truyền Thông Đa Phương Tiện". Đăng ký ngay để hưởng ưu đãi đầy hấp dẫn của ITPlus
                                    Academy trong tháng 07 này nhé!</p>
                                <p><a href="#"
                                      class="btn btn-outlined btn-success btn-sm">Đọc thêm</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 hidden-xs">
                    <div class="media">
                        <div class="media-left media-middle">
                            <a class="yellow"
                               href="#">
                                <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/900x600(11).jpg"
                                     class="media-object img-thumbnail dobong lazy" style="width:190px;height: 184px">
                            </a>
                        </div>
                        <div class="media-body">
                            <a class="yellow"
                               href="#">
                                <h4 class="media-heading">JULY SCHOLARSHIP – ƯU ĐÃI 50% HỌC PHÍ – CHỈ TỪ 5.000.000Đ CHO
                                    01 KHÓA HỌC 06 THÁNG</h4>
                            </a>
                            Khuyến học tháng 07/ 2021: "July Scholarship - Ưu đãi lên đến 50% học phí" dành cho các khóa
                            học chuyên đề, giảm thêm từ 500.000đ/ học viên cho nhóm 02 người & 1.000.000đ/ học viên cho
                            nhóm 05 người. Các khóa học phù hợp với sinh viên và người đi làm có mong muốn nâng cao kỹ
                            năng, kiến thức "Lập Trình" & "Thiết Kế Đồ Họa - Truyền Thông Đa Phương Tiện". Đăng ký ngay
                            để hưởng ưu đãi đầy hấp dẫn của ITPlus Academy trong tháng 07 này nhé! <br>
                            <a class="yellow"
                               href="#">Đọc
                                thêm</a>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a class="yellow"
                               href="#">
                                <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/1a-.jpg"
                                     class="media-object img-thumbnail dobong lazy" style="width:190px;height: 184px">
                            </a>
                        </div>
                        <div class="media-body">
                            <a class="yellow"
                               href="#">
                                <h4 class="media-heading">BÁO NHỊP SỐNG SỐ - ITPLUS ACADEMY NHẬN GIẢI SAO KHUÊ LẦN THỨ
                                    04 CHO "CÁC CHƯƠNG TRÌNH ĐÀO TẠO KỸ NĂNG CNTT VÀ THIẾT KẾ ĐỒ HỌA - TRUYỀN THÔNG ĐA
                                    PHƯƠNG</h4>
                            </a>
                            Ngày 24/04, Lễ công bố và trao giải Sao Khuê 2021" - Danh hiệu uy tín và danh giá nhất của
                            ngành phần mềm và dịch vụ CNTT Việt Nam do Hiệp hội Phần mềm và Dịch vụ CNTT (VINASA) tổ
                            chức thường niên đã diễn ra. Với sự nỗ lực và đổi mới không ngừng về chương trình đào tạo,
                            nâng cao chất lượng giảng dạy ITPlus Academy đã vinh dự vinh danh tại chương trình. Đây là
                            lần thứ tư ITPlus Academy nhận Danh hiệu Sao Khuê. <br>
                            <a class="yellow"
                               href="#">Đọc
                                thêm</a>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left media-middle">
                            <a class="yellow"
                               href="#">
                                <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/1a(3).jpg"
                                     class="media-object img-thumbnail dobong lazy" style="width:190px;height: 184px">
                            </a>
                        </div>
                        <div class="media-body">
                            <a class="yellow"
                               href="#">
                                <h4 class="media-heading">ITPLUS ACADEMY KHAI TRƯƠNG CHI NHÁNH MIỀN NAM TẠI QUẬN 5, TP
                                    HCM</h4>
                            </a>
                            Hôm nay, ngày 07/04/2021 ITPlus Academy với một sự biết ơn, một lòng hồ hởi, mong ngóng sau
                            bao nhiêu ngày đã chính thức khai trương Chi nhánh miền Nam tại địa chỉ cao ốc MH, 728, Võ
                            Văn Kiệt, Phường 1, Quận 5, TP Hồ Chí Minh. Sau nhiều sự chuẩn bị cùng với sự hỗ trợ của các
                            đối tác, sự quyết tâm nỗ lực của tập thể CBNV ITPlus, ITPlus Academy đã chính thức có mặt
                            tại TP Hồ Chí Minh với cơ sở đào tạo thứ 04 của mình. <br>
                            <a class="yellow"
                               href="http://itplus-academy.edu.vn/itplus-academy-khai-truong-chi-nhanh-mien-nam-tai-quan-5-tp-hcm-3303.html">Đọc
                                thêm</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <a href="#">
                        <button type="button" class="btn btn-success btn-sm">Xem thêm</button>
                    </a>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="registerHome" style="padding-bottom: 25px; background-color: #eee;">
        <div class="container">
            <div class="row">
                <div class="itemM col-xs-12 col-sm-6 col-lg-4">
                    <div class="email">
                        <img src="https://niithanoi.edu.vn/img/email.png" alt="">
                    </div>
                    <span class="title">Đăng ký tư vấn</span>
                    <div class="text">
                        Nhân viên gọi điện tư vấn miễn phí sau khi đăng ký <div>Được cập nhật các ưu đãi sớm nhất</div>
                    </div>
                    <a href="tel:078 924 7333" title="" class="hotLine btn">Hotline: 078 924 7333</a>
                </div>
                <div class="itemM col-xs-12 col-sm-6 col-lg-4">
                    <div class="khungAnh">
                        <a href="javascript:void(0)" target="_blank" title="" class="khungAnhCrop">
                            <img alt="" src="https://niithanoi.edu.vn/pic/banner/tu-van-vi_636907522460973297.png">
                        </a>
                    </div>
                </div>
                <div class="itemM col-xs-12 col-sm-6 col-lg-4">
                    <div id="form_contactF" class="form">
                        <div class="item">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <input id="tbHoTenF" type="text" placeholder="Họ tên *">
                        </div>

                        {{--                            <div id="cf_name" class="item alert">Tên không được để trống</div>--}}

                        <div class="item">
                            <i class="fa fa-phone" aria-hidden="true" style="font-size: 16px;"></i>
                            <input id="tbPhoneF" type="text" name="number" placeholder="Điện thoại *">
                        </div>

                        {{--                            <div id="cf_phone" class="item alert">Số điện thoại không được để trống</div>--}}

                        <div class="item">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <input id="tbEmailF" type="text" placeholder="Email *">
                        </div>

                        {{--                            <div id="cf_email" class="item alert">Email không được để trống</div>--}}

                        <div class="item">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            <textarea id="tbNoiDungF" placeholder="Ghi chú"></textarea>
                        </div>
                        <span id="alertTV" class="item" style="color: #0661ac;display: none">Gửi yêu cầu tư vấn thành
                            công!</span>
                        <div class="item">
                            <a onclick="SendContact3()" href="javascript:void(0)" title="Tư vấn cho tôi ngay !"
                               class="btn">Tư vấn cho tôi ngay !</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="overflow: hidden">
        <div class="row">
            <div class="col-md-12 text-center section">
                <h2 class="blue header-text text-uppercase"><a href="#">Hợp tác với worklab</a></h2>
                <div class="bar2"></div>
            </div>
            {{--<div class="col-md-12">
                <div id="doitac2">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="/Dao-tao-chuyen-de.html"><img
                                        src="http://itplus-academy.edu.vn/public/images/home-dao-tao-cho-doanh-nghiep.jpg"
                                        alt=""></a>
                        </div>
                        <div class="col-sm-4">
                            <a href="/Doanh-nghiep--Doi-tac.html"><img
                                        src="http://itplus-academy.edu.vn/public/images/home-hop-tac-tuyen-dung.jpg"
                                        alt=""></a>
                        </div>
                        <div class="col-sm-4">
                            <a href="/Tin-tuyen-dung.html"><img
                                        src="http://itplus-academy.edu.vn/public/images/home-tin-tuyen-dung.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
    </div>
    <style>
        .contact-wrap__title {
            border-bottom: 1px solid #333;
            margin-bottom: 15px;
        }
    </style>
@endsection

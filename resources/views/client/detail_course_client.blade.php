<!DOCTYPE html>
<html lang="en">

<head>
    <style type="text/css">
        .swal-icon--error {
            border-color: #f27474;
            -webkit-animation: animateErrorIcon .5s;
            animation: animateErrorIcon .5s
        }

        .swal-icon--error__x-mark {
            position: relative;
            display: block;
            -webkit-animation: animateXMark .5s;
            animation: animateXMark .5s
        }

        .swal-icon--error__line {
            position: absolute;
            height: 5px;
            width: 47px;
            background-color: #f27474;
            display: block;
            top: 37px;
            border-radius: 2px
        }

        .swal-icon--error__line--left {
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
            left: 17px
        }

        .swal-icon--error__line--right {
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            right: 16px
        }

        @-webkit-keyframes animateErrorIcon {
            0% {
                -webkit-transform: rotateX(100deg);
                transform: rotateX(100deg);
                opacity: 0
            }

            to {
                -webkit-transform: rotateX(0deg);
                transform: rotateX(0deg);
                opacity: 1
            }
        }

        @keyframes animateErrorIcon {
            0% {
                -webkit-transform: rotateX(100deg);
                transform: rotateX(100deg);
                opacity: 0
            }

            to {
                -webkit-transform: rotateX(0deg);
                transform: rotateX(0deg);
                opacity: 1
            }
        }

        @-webkit-keyframes animateXMark {
            0% {
                -webkit-transform: scale(.4);
                transform: scale(.4);
                margin-top: 26px;
                opacity: 0
            }

            50% {
                -webkit-transform: scale(.4);
                transform: scale(.4);
                margin-top: 26px;
                opacity: 0
            }

            80% {
                -webkit-transform: scale(1.15);
                transform: scale(1.15);
                margin-top: -6px
            }

            to {
                -webkit-transform: scale(1);
                transform: scale(1);
                margin-top: 0;
                opacity: 1
            }
        }

        @keyframes animateXMark {
            0% {
                -webkit-transform: scale(.4);
                transform: scale(.4);
                margin-top: 26px;
                opacity: 0
            }

            50% {
                -webkit-transform: scale(.4);
                transform: scale(.4);
                margin-top: 26px;
                opacity: 0
            }

            80% {
                -webkit-transform: scale(1.15);
                transform: scale(1.15);
                margin-top: -6px
            }

            to {
                -webkit-transform: scale(1);
                transform: scale(1);
                margin-top: 0;
                opacity: 1
            }
        }

        .swal-icon--warning {
            border-color: #f8bb86;
            -webkit-animation: pulseWarning .75s infinite alternate;
            animation: pulseWarning .75s infinite alternate
        }

        .swal-icon--warning__body {
            width: 5px;
            height: 47px;
            top: 10px;
            border-radius: 2px;
            margin-left: -2px
        }

        .swal-icon--warning__body,
        .swal-icon--warning__dot {
            position: absolute;
            left: 50%;
            background-color: #f8bb86
        }

        .swal-icon--warning__dot {
            width: 7px;
            height: 7px;
            border-radius: 50%;
            margin-left: -4px;
            bottom: -11px
        }

        @-webkit-keyframes pulseWarning {
            0% {
                border-color: #f8d486
            }

            to {
                border-color: #f8bb86
            }
        }

        @keyframes pulseWarning {
            0% {
                border-color: #f8d486
            }

            to {
                border-color: #f8bb86
            }
        }

        .swal-icon--success {
            border-color: #a5dc86
        }

        .swal-icon--success:after,
        .swal-icon--success:before {
            content: "";
            border-radius: 50%;
            position: absolute;
            width: 60px;
            height: 120px;
            background: #fff;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg)
        }

        .swal-icon--success:before {
            border-radius: 120px 0 0 120px;
            top: -7px;
            left: -33px;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            -webkit-transform-origin: 60px 60px;
            transform-origin: 60px 60px
        }

        .swal-icon--success:after {
            border-radius: 0 120px 120px 0;
            top: -11px;
            left: 30px;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            -webkit-transform-origin: 0 60px;
            transform-origin: 0 60px;
            -webkit-animation: rotatePlaceholder 4.25s ease-in;
            animation: rotatePlaceholder 4.25s ease-in
        }

        .swal-icon--success__ring {
            width: 80px;
            height: 80px;
            border: 4px solid hsla(98, 55%, 69%, .2);
            border-radius: 50%;
            box-sizing: content-box;
            position: absolute;
            left: -4px;
            top: -4px;
            z-index: 2
        }

        .swal-icon--success__hide-corners {
            width: 5px;
            height: 90px;
            background-color: #fff;
            padding: 1px;
            position: absolute;
            left: 28px;
            top: 8px;
            z-index: 1;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg)
        }

        .swal-icon--success__line {
            height: 5px;
            background-color: #a5dc86;
            display: block;
            border-radius: 2px;
            position: absolute;
            z-index: 2
        }

        .swal-icon--success__line--tip {
            width: 25px;
            left: 14px;
            top: 46px;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
            -webkit-animation: animateSuccessTip .75s;
            animation: animateSuccessTip .75s
        }

        .swal-icon--success__line--long {
            width: 47px;
            right: 8px;
            top: 38px;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            -webkit-animation: animateSuccessLong .75s;
            animation: animateSuccessLong .75s
        }

        @-webkit-keyframes rotatePlaceholder {
            0% {
                -webkit-transform: rotate(-45deg);
                transform: rotate(-45deg)
            }

            5% {
                -webkit-transform: rotate(-45deg);
                transform: rotate(-45deg)
            }

            12% {
                -webkit-transform: rotate(-405deg);
                transform: rotate(-405deg)
            }

            to {
                -webkit-transform: rotate(-405deg);
                transform: rotate(-405deg)
            }
        }

        @keyframes rotatePlaceholder {
            0% {
                -webkit-transform: rotate(-45deg);
                transform: rotate(-45deg)
            }

            5% {
                -webkit-transform: rotate(-45deg);
                transform: rotate(-45deg)
            }

            12% {
                -webkit-transform: rotate(-405deg);
                transform: rotate(-405deg)
            }

            to {
                -webkit-transform: rotate(-405deg);
                transform: rotate(-405deg)
            }
        }

        @-webkit-keyframes animateSuccessTip {
            0% {
                width: 0;
                left: 1px;
                top: 19px
            }

            54% {
                width: 0;
                left: 1px;
                top: 19px
            }

            70% {
                width: 50px;
                left: -8px;
                top: 37px
            }

            84% {
                width: 17px;
                left: 21px;
                top: 48px
            }

            to {
                width: 25px;
                left: 14px;
                top: 45px
            }
        }

        @keyframes animateSuccessTip {
            0% {
                width: 0;
                left: 1px;
                top: 19px
            }

            54% {
                width: 0;
                left: 1px;
                top: 19px
            }

            70% {
                width: 50px;
                left: -8px;
                top: 37px
            }

            84% {
                width: 17px;
                left: 21px;
                top: 48px
            }

            to {
                width: 25px;
                left: 14px;
                top: 45px
            }
        }

        @-webkit-keyframes animateSuccessLong {
            0% {
                width: 0;
                right: 46px;
                top: 54px
            }

            65% {
                width: 0;
                right: 46px;
                top: 54px
            }

            84% {
                width: 55px;
                right: 0;
                top: 35px
            }

            to {
                width: 47px;
                right: 8px;
                top: 38px
            }
        }

        @keyframes animateSuccessLong {
            0% {
                width: 0;
                right: 46px;
                top: 54px
            }

            65% {
                width: 0;
                right: 46px;
                top: 54px
            }

            84% {
                width: 55px;
                right: 0;
                top: 35px
            }

            to {
                width: 47px;
                right: 8px;
                top: 38px
            }
        }

        .swal-icon--info {
            border-color: #c9dae1
        }

        .swal-icon--info:before {
            width: 5px;
            height: 29px;
            bottom: 17px;
            border-radius: 2px;
            margin-left: -2px
        }

        .swal-icon--info:after,
        .swal-icon--info:before {
            content: "";
            position: absolute;
            left: 50%;
            background-color: #c9dae1
        }

        .swal-icon--info:after {
            width: 7px;
            height: 7px;
            border-radius: 50%;
            margin-left: -3px;
            top: 19px
        }

        .swal-icon {
            width: 80px;
            height: 80px;
            border-width: 4px;
            border-style: solid;
            border-radius: 50%;
            padding: 0;
            position: relative;
            box-sizing: content-box;
            margin: 20px auto
        }

        .swal-icon:first-child {
            margin-top: 32px
        }

        .swal-icon--custom {
            width: auto;
            height: auto;
            max-width: 100%;
            border: none;
            border-radius: 0
        }

        .swal-icon img {
            max-width: 100%;
            max-height: 100%
        }

        .swal-title {
            color: rgba(0, 0, 0, .65);
            font-weight: 600;
            text-transform: none;
            position: relative;
            display: block;
            padding: 13px 16px;
            font-size: 27px;
            line-height: normal;
            text-align: center;
            margin-bottom: 0
        }

        .swal-title:first-child {
            margin-top: 26px
        }

        .swal-title:not(:first-child) {
            padding-bottom: 0
        }

        .swal-title:not(:last-child) {
            margin-bottom: 13px
        }

        .swal-text {
            font-size: 16px;
            position: relative;
            float: none;
            line-height: normal;
            vertical-align: top;
            text-align: left;
            display: inline-block;
            margin: 0;
            padding: 0 10px;
            font-weight: 400;
            color: rgba(0, 0, 0, .64);
            max-width: calc(100% - 20px);
            overflow-wrap: break-word;
            box-sizing: border-box
        }

        .swal-text:first-child {
            margin-top: 45px
        }

        .swal-text:last-child {
            margin-bottom: 45px
        }

        .swal-footer {
            text-align: right;
            padding-top: 13px;
            margin-top: 13px;
            padding: 13px 16px;
            border-radius: inherit;
            border-top-left-radius: 0;
            border-top-right-radius: 0
        }

        .swal-button-container {
            margin: 5px;
            display: inline-block;
            position: relative
        }

        .swal-button {
            background-color: #7cd1f9;
            color: #fff;
            border: none;
            box-shadow: none;
            border-radius: 5px;
            font-weight: 600;
            font-size: 14px;
            padding: 10px 24px;
            margin: 0;
            cursor: pointer
        }

        .swal-button:not([disabled]):hover {
            background-color: #78cbf2
        }

        .swal-button:active {
            background-color: #70bce0
        }

        .swal-button:focus {
            outline: none;
            box-shadow: 0 0 0 1px #fff, 0 0 0 3px rgba(43, 114, 165, .29)
        }

        .swal-button[disabled] {
            opacity: .5;
            cursor: default
        }

        .swal-button::-moz-focus-inner {
            border: 0
        }

        .swal-button--cancel {
            color: #555;
            background-color: #efefef
        }

        .swal-button--cancel:not([disabled]):hover {
            background-color: #e8e8e8
        }

        .swal-button--cancel:active {
            background-color: #d7d7d7
        }

        .swal-button--cancel:focus {
            box-shadow: 0 0 0 1px #fff, 0 0 0 3px rgba(116, 136, 150, .29)
        }

        .swal-button--danger {
            background-color: #e64942
        }

        .swal-button--danger:not([disabled]):hover {
            background-color: #df4740
        }

        .swal-button--danger:active {
            background-color: #cf423b
        }

        .swal-button--danger:focus {
            box-shadow: 0 0 0 1px #fff, 0 0 0 3px rgba(165, 43, 43, .29)
        }

        .swal-content {
            padding: 0 20px;
            margin-top: 20px;
            font-size: medium
        }

        .swal-content:last-child {
            margin-bottom: 20px
        }

        .swal-content__input,
        .swal-content__textarea {
            -webkit-appearance: none;
            background-color: #fff;
            border: none;
            font-size: 14px;
            display: block;
            box-sizing: border-box;
            width: 100%;
            border: 1px solid rgba(0, 0, 0, .14);
            padding: 10px 13px;
            border-radius: 2px;
            transition: border-color .2s
        }

        .swal-content__input:focus,
        .swal-content__textarea:focus {
            outline: none;
            border-color: #6db8ff
        }

        .swal-content__textarea {
            resize: vertical
        }

        .swal-button--loading {
            color: transparent
        }

        .swal-button--loading~.swal-button__loader {
            opacity: 1
        }

        .swal-button__loader {
            position: absolute;
            height: auto;
            width: 43px;
            z-index: 2;
            left: 50%;
            top: 50%;
            -webkit-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
            text-align: center;
            pointer-events: none;
            opacity: 0
        }

        .swal-button__loader div {
            display: inline-block;
            float: none;
            vertical-align: baseline;
            width: 9px;
            height: 9px;
            padding: 0;
            border: none;
            margin: 2px;
            opacity: .4;
            border-radius: 7px;
            background-color: hsla(0, 0%, 100%, .9);
            transition: background .2s;
            -webkit-animation: swal-loading-anim 1s infinite;
            animation: swal-loading-anim 1s infinite
        }

        .swal-button__loader div:nth-child(3n+2) {
            -webkit-animation-delay: .15s;
            animation-delay: .15s
        }

        .swal-button__loader div:nth-child(3n+3) {
            -webkit-animation-delay: .3s;
            animation-delay: .3s
        }

        @-webkit-keyframes swal-loading-anim {
            0% {
                opacity: .4
            }

            20% {
                opacity: .4
            }

            50% {
                opacity: 1
            }

            to {
                opacity: .4
            }
        }

        @keyframes swal-loading-anim {
            0% {
                opacity: .4
            }

            20% {
                opacity: .4
            }

            50% {
                opacity: 1
            }

            to {
                opacity: .4
            }
        }

        .swal-overlay {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            font-size: 0;
            overflow-y: auto;
            background-color: rgba(0, 0, 0, .4);
            z-index: 10000;
            pointer-events: none;
            opacity: 0;
            transition: opacity .3s
        }

        .swal-overlay:before {
            content: " ";
            display: inline-block;
            vertical-align: middle;
            height: 100%
        }

        .swal-overlay--show-modal {
            opacity: 1;
            pointer-events: auto
        }

        .swal-overlay--show-modal .swal-modal {
            opacity: 1;
            pointer-events: auto;
            box-sizing: border-box;
            -webkit-animation: showSweetAlert .3s;
            animation: showSweetAlert .3s;
            will-change: transform
        }

        .swal-modal {
            width: 478px;
            opacity: 0;
            pointer-events: none;
            background-color: #fff;
            text-align: center;
            border-radius: 5px;
            position: static;
            margin: 20px auto;
            display: inline-block;
            vertical-align: middle;
            -webkit-transform: scale(1);
            transform: scale(1);
            -webkit-transform-origin: 50% 50%;
            transform-origin: 50% 50%;
            z-index: 10001;
            transition: opacity .2s, -webkit-transform .3s;
            transition: transform .3s, opacity .2s;
            transition: transform .3s, opacity .2s, -webkit-transform .3s
        }

        @media (max-width:500px) {
            .swal-modal {
                width: calc(100% - 20px)
            }
        }

        @-webkit-keyframes showSweetAlert {
            0% {
                -webkit-transform: scale(1);
                transform: scale(1)
            }

            1% {
                -webkit-transform: scale(.5);
                transform: scale(.5)
            }

            45% {
                -webkit-transform: scale(1.05);
                transform: scale(1.05)
            }

            80% {
                -webkit-transform: scale(.95);
                transform: scale(.95)
            }

            to {
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @keyframes showSweetAlert {
            0% {
                -webkit-transform: scale(1);
                transform: scale(1)
            }

            1% {
                -webkit-transform: scale(.5);
                transform: scale(.5)
            }

            45% {
                -webkit-transform: scale(1.05);
                transform: scale(1.05)
            }

            80% {
                -webkit-transform: scale(.95);
                transform: scale(.95)
            }

            to {
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }
    </style>
    <title>Chi tiết khóa học Worklab – Hỗ trợ việc làm</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">
    <link rel="icon" type="image/png" href="/logo-favicon.jpg"/>
    <link rel="icon" type="image/png" href="/logo-favicon.jpg"/>
    <link rel="stylesheet" href="{{asset('landing-page/PHP-V2/fonts/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('landing-page/PHP-V2/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing-page/PHP-V2/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('landing-page/PHP-V2/css/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing-page/PHP-V2/fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('landing-page/PHP-V2/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('landing-page/PHP-V2/css/site.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing-page/PHP-V2/css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    <link rel="stylesheet" href="?v=45">


    <script async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script>
    <script type="text/javascript" async="" src="//s10.histats.com/js15_as.js"></script>
</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300" data-aos-easing="slide"
      data-aos-duration="800" data-aos-delay="0">
<div id="fb-root"></div>


<div class="site-wrap" id="home-page">
    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body">
            <ul class="site-nav-wrap">
                <li>
                    <a href="{{route('homepage')}}" class="nav-link">Trang Chủ</a>
                </li>
                <li>
                    <a href="#noidungchitietkhoahoc" class="nav-link active">Nội Dung Khóa Học</a>
                </li>
                <li>
                    <a href="#chinhsachhocphi" class="nav-link">Học Phí Khóa Học</a>
                </li>
                <li>
                    <a href="#footer" class="nav-link">Liên Hệ</a>
                </li>
                <li>
                    <a href="tel:0963976565" class="nav-link btn-tel">Hotline: 078 924 7333</a>
                </li>
            </ul>
        </div>
    </div>
    <header class="d-block position-relative" role="banner">
        <div id="sticky-wrapper" class="sticky-wrapper is-sticky" style="height: 79px;">
            <div class="site-navbar py-4 js-sticky-header site-navbar-target p-2 mb-5 shrink"
                 style="top: -120px; width: 1284px; position: fixed; z-index: inherit;">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-6 col-md-3 col-xl-2  d-block">
                            <h1 class="mb-0 site-logo d-flex">
                                <a href="/" class="text-black h2 mb-0">
                                    <img style="width: 120px; margin-left: 70px;" src="{{asset('logo.jpg')}}" alt="">
                                </a>
                            </h1>
                        </div>
                        <div class="col-12 col-md-9 col-xl-10 main-menu">
                            <nav class="site-navigation position-relative text-right" role="navigation">
                                <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block ml-0 pl-0">
                                    <li>
                                        <a href="{{route('homepage')}}" class="nav-link">Trang Chủ</a>
                                    </li>
                                    <li>
                                        <a href="#noidungchitietkhoahoc" class="nav-link active">Nội Dung Khóa
                                            Học</a>
                                    </li>
                                    <li>
                                        <a href="#chinhsachhocphi" class="nav-link">Học Phí Khóa Học</a>
                                    </li>
                                    <li>
                                        <a href="#footer" class="nav-link">Liên Hệ</a>
                                    </li>
                                    <li>
                                        <a href="tel:0963976565" class="nav-link btn-tel">Hotline: 078 924 7333</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-6 col-md-9 d-inline-block d-lg-none ml-md-0">
                            <a href="#" class="site-menu-toggle js-menu-toggle text-black float-right"><span
                                        class="icon-menu h3"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="app">
        <section id="slider" style="overflow: hidden;"><img src="{{asset($courseDetail->url_image_top)}}" alt=""
                                                            class="img-fluid w-100"></section>
        <section id="chinhsachhocphi">
            <div class="container">
                <div class="pp-title-style-1 text-center"><img src="landing-page/PHP-V2/images/square_rotate.png"
                                                               alt="">
                    <h4 class="text-uppercase font-weight-bold">Học phí và lịch khai giảng mới nhất</h4> <span
                            class="pp-line"></span>
                </div>
                <div class="row">
                    <div class="col-md-12 pt-5">
                        <table style="background-color:white; min-height: auto!important" class="table table-bordered table-hover product-item">
                            <thead>
                            <tr>
                                <th style="width: 12%">Khai giảng</th>
                                <th style="width: 15%">Cơ sở/<br>Hình thức</th>
                                <th style="width: 10%">Thời gian</th>
                                <th style="width: 10%">Lịch học</th>
                                <th style="width: 20%">Ưu đãi học phí</th>
                                <th style="width: 20%">Học phí còn lại </th>
                                <th style="width: 20%">Note </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($listSchedule as $item)
                                <tr>
                                    <td>{{$item->start_date}}</td>
                                    <td>{{$item->local}}</td>
                                    <td>{{$item->time}}</td>
                                    <td>{{$item->schedule}}</td>
                                    <td class="text-danger font-weight-bold">Hỗ trợ <b>{{$item->discount}}%</b> học phí
                                        khi đăng ký đến hết ngày <b>{{$item->expire_date}}</b></td>
                                    <td class="text-danger font-weight-bold"><b>{{number_format((100-$item->discount)*$item->original_tuition/100)}} vnđ</b></td>
                                    <td class="text-danger font-weight-bold"><b>{{$item->note}}</b></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        {{-- <section id="doituong">
            <div class="container"><img src="{{asset($courseDetail->image_information)}}" alt="" class="img-fluid w-100">
            </div>
        </section> --}}
        <section id="thongtinkhoahoc" class="my-5">
            <div class="container">
                <div class="pp-title-style-1 text-center"><img src="landing-page/PHP-V2/images/square_rotate.png"
                                                               alt="">
                    <h4 class="text-uppercase font-weight-bold">Thông tin khóa học</h4> <span
                            class="pp-line"></span>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="box-shadow p-5 bg-line bg-white mt-5 py-xs-0">
                            {!! $courseDetail->information !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="thongtingiangvien" class="my-5">
            <div class="container">
                <div class="pp-title-style-1 text-center"><img src="landing-page/PHP-V2/images/square_rotate.png"
                                                               alt="">
                    <h4 class="text-uppercase font-weight-bold">Thông tin giảng viên</h4> <span
                            class="pp-line"></span>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="box-shadow p-5 bg-line bg-white mt-5 py-xs-0">
                            {!! $courseDetail->teacher !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="noidungchitietkhoahoc" class="text-center mb-5">
            <div class="container"><img src="{{asset('landing-page/PHP-V2/images/noidung_chitietkhoahoc.png')}}" alt=""
                                        class="img-fluid"></div>
        </section>
        @foreach ($courseMainContent as $course)
            <section id="htmlCss">
                <div class="container">
                    <img src="{{asset($course->url_image_content)}}" alt="" class="img-fluid w-100">
                    <div class="row content py-5">
                        @php $check = 0 @endphp
                        @foreach($courseDetailContent as $key => $detail)
                            @if($course->id == $detail->id_course_main_content)
                                @php $check++ @endphp
                                <div class="col-md-6">
                                    <div class="pp-item pp-{{$color[$check%6]}}">
                                        <div class="pp-item-number pp-{{$color[$check%6]}}"><span>{{$check}}</span></div>
                                        <div class="pp-item-content">
                                            <h6>{{$detail->main_content}}</h6>
                                            <button onclick="detail({{$detail->id}})" class="btn btn-link" data-original-title="" title="">Chi tiết</button>
                                            <div id="detail-content-{{$detail->id}}" style="display: none; white-space: pre-line">
                                                {{$detail->detail_content}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </section>
        @endforeach
         {{-- <div class="registerHome" style="padding-bottom: 25px; background-color: #eee;">
            <div class="container">
                <div class="row">
                    <div class="itemM col-xs-12 col-sm-6 col-lg-4">
                        <div class="email">
                            <img src="https://niithanoi.edu.vn/img/email.png" alt="">
                        </div>
                        <span class="title">Đăng ký tư vấn</span>
                        <div class="text">
                            Nhân viên gọi điện tư vấn miễn phí sau khi đăng ký <div>Được cập nhật các ưu đãi sớm nhất</div>
                        </div>
                        <a href="tel:078 924 7333" title="" class="hotLine btn">Hotline: 078 924 7333</a>
                    </div>
                    <div class="itemM col-xs-12 col-sm-6 col-lg-4">
                        <div class="khungAnh">
                            <a href="javascript:void(0)" target="_blank" title="" class="khungAnhCrop">
                                <img alt="" src="https://niithanoi.edu.vn/pic/banner/tu-van-vi_636907522460973297.png">
                            </a>
                        </div>
                    </div>
                    <div class="itemM col-xs-12 col-sm-6 col-lg-4">
                        <div id="form_contactF" class="form">
                            <div class="item">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <input id="tbHoTenF" type="text" placeholder="Họ tên *">
                            </div>

--}}{{--                            <div id="cf_name" class="item alert">Tên không được để trống</div>--}}{{--

                            <div class="item">
                                <i class="fa fa-phone" aria-hidden="true" style="font-size: 16px;"></i>
                                <input id="tbPhoneF" type="text" name="number" placeholder="Điện thoại *">
                            </div>

--}}{{--                            <div id="cf_phone" class="item alert">Số điện thoại không được để trống</div>--}}{{--

                            <div class="item">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <input id="tbEmailF" type="text" placeholder="Email *">
                            </div>

--}}{{--                            <div id="cf_email" class="item alert">Email không được để trống</div>--}}{{--

                            <div class="item">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                <textarea id="tbNoiDungF" placeholder="Ghi chú"></textarea>
                            </div>
                            <span id="alertTV" class="item" style="color: #0661ac;display: none">Gửi yêu cầu tư vấn thành
                            công!</span>
                            <div class="item">
                                <a onclick="SendContact3()" href="javascript:void(0)" title="Tư vấn cho tôi ngay !"
                                   class="btn">Tư vấn cho tôi ngay !</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>


    <div id="histats_counter" style="height:0px; position:absolute; left:-1000000px;"></div>

    <div id="overlayer" style="display: none;"></div>
    <div class="loader" style="display: none;">
        <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <footer id="footer" style="background-color: #F1F1F1!important;">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-7">
                    <h3 style="text-transform:uppercase; font-weight: bold;">Hệ thống cơ sở đào tạo</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <h4 style="border-bottom: 1px solid rgb(68 68 73); display: inline-block; padding-bottom: 5px;">
                                Cơ sở Hà Nội</h4>
                            <p><strong>Nguyễn Huy Tưởng</strong></p>
                            <p><strong>LK4-TT1, Số 96-96B Nguyễn Huy Tưởng, Quận Thanh Xuân, Hà Nội.</strong></p>
                            <p><strong>Hotline</strong>: 078 924 7333</p>
                            <p><strong>Email</strong>: tuyensinh@worklab.edu.vn</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5" style="overflow: hidden;">
                    <div class="contact-wrap">
                        <div class="contact-wrap__title" style="font-weight: bold; margin-left: 20px;">
                            <h3 style="font-weight: bold;">CHAT VỚI CHÚNG TÔI</h3>
                        </div>
                        <div class="contact-wrap__content text-center">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fworklabacademy&tabs=messages&width=400&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="400" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright text-center py-2" style="background-color: #E5E6EC;">
            Copyright ©
            <script data-cfasync="false"
                    src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
            <script>document.write((new Date()).getFullYear())</script>2021 All Rights Reserved. Phát triển bởi
            WorkLab
        </div>
    </footer>
</div>
<script src="{{asset('landing-page/PHP-V2/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('landing-page/PHP-V2/js/jquery-ui.js')}}"></script>
<script src="{{asset('landing-page/PHP-V2/js/popper.min.js')}}"></script>
<script src="{{asset('landing-page/PHP-V2/js/bootstrap.min.js')}}"></script>
<script src="{{asset('landing-page/PHP-V2/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('landing-page/PHP-V2/js/aos.js')}}"></script>
<script src="{{asset('landing-page/PHP-V2/js/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('landing-page/PHP-V2/js/jquery.sticky.js')}}"></script>
<script src="{{asset('landing-page/PHP-V2/js/site.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
<meta charset="utf-8">
<link rel="stylesheet" href="/facebook/facebook.css">
<script src="landing-page/PHP-V2/js/main.js"></script>
<script>
    function detail(id) {
        const element = document.getElementById('detail-content-'+id);
        const display = element.style.display;
        if(display === 'none') {
            element.style.display = 'block'
        } else {
            element.style.display = 'none'
        }
    }
</script>

</body>

</html>

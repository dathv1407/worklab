<header>
    <div class="container ">
        <div class="row" id="mobilenav">
            <div class="col-md-3">
                <button type="button" class="hamburger is-closed hidden-md hidden-lg" data-toggle="offcanvas">
                    <span class="hamb-top"></span>
                    <span class="hamb-middle"></span>
                    <span class="hamb-bottom"></span>
                </button>

                <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
                    <ul class='nav sidebar-nav'>
                        <li><a href="/">Trang chủ</a></li>
                        <li>
                            <a href="/gioi-thieu">Giới thiệu</a>
                        </li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="daotaochuyende">Khóa học<b class="caret"></b></a>
                            <ul class='dropdown-menu' role='menu'>
                                @foreach($menuDaoTao as $item)
                                    <li>
                                        <a href="{{$item->route}}"> {{ $item->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Luyện thi chứng chỉ <b class="caret"></b></a>
                            <ul class='dropdown-menu' role='menu'>
                                @foreach($menuCertification as $item)
                                    <li>
                                        <a href="#"> {{ $item->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Tin Tức <b
                                        class="caret"></b></a>
                            <ul class='dropdown-menu' role='menu'>
                                <li><a href="#">Bản tin
                                        WorkLab</a></li>
                                <li><a href="#">Tin công nghệ </a>
                                </li>
                                <li><a href="#">Tin tuyển dụng</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">chia sẻ kiến thức <b
                                        class="caret"></b></a>
                            <ul class='dropdown-menu' role='menu'>
                                <li><a href="#">Lập Trình Ứng Dụng</a></li>
                                <li><a href="#">Kiểm Thử Phần Mềm</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Tin THPT <b
                                        class="caret"></b></a>
                            <ul class='dropdown-menu' role='menu'>
                                <li><a href="#">Thông
                                        tin thi THPT quốc gia</a></li>
                                <li><a href="#">Tư vấn ngành
                                        nghề</a></li>
                                <li><a href="#">Bài học cuộc
                                        sống</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('lich-khai-giang-moi-nhat')}}">Lịch khai giảng</a></li>
                        <li><a href="#footer">Liên hệ</a></li>
                    </ul>
                </nav>
                <a href="#">
                    <img src="{{asset('logo.jpg')}}" style="width: 140px;"
                         class="img-responsive" alt="Image">
                </a>
            </div>
            <div class="col-md-9 hidden-xs hidden-sm" style="margin-top: 17px">
                <div class="owl-carousel-top owl-carousel owl-theme">
                    <div class="item text-center" style="padding-top:20px">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="school-vi" style="margin-top: 0;font-size: 22px;margin-bottom: 5px;">
                                    TRUNG TÂM ĐÀO TẠO CÔNG NGHỆ WORKLAB
                                </h3>
                                <p class="school-en">WORKLAB TECHNOLOGY TRAINING CENTER</p>
                            </div>
{{--                            <div class="col-md-3">--}}
{{--                                <img src="#"--}}
{{--                                     style="width:150px;margin-top:5px;margin-left: 20px" class="img-responsive log2"--}}
{{--                                     alt="PTIT">--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div id="menufix" class="hidden-xs hidden-sm">
    <div id="mainnav" class="navbar navbar-default" role="navigation">
        <div class="container">

            <div class="collapse navbar-collapse">
                <ul class='nav navbar-nav'>
                    <li><a href="/">Trang chủ</a></li>
                    <li>
                        <a href="/gioi-thieu">Giới thiệu</a>
                    </li>
                    <li><a class="dropdown-toggle" data-toggle="dropdown"
                           href="{{route('dao-tao-chuyen-de', ['id' => 'all'])}}">Khóa học <b
                                    class="caret"></b></a>
                        <ul class='dropdown-menu'>
                            @foreach($menuDaoTao as $item)
                                <li>
                                    <a href="{{route('dao-tao-chuyen-de', ['id' => $item->route])}}"> {{ $item->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li><a class="dropdown-toggle" data-toggle="dropdown"
                           href="#">Luyện thi chứng chỉ<b
                                    class="caret"></b></a>
                        <ul class='dropdown-menu'>
                            @foreach($menuCertification as $item)
                                <li>
                                    <a href="{{route('dao-tao-chuyen-de', ['id' => $item->route])}}"> {{ $item->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li><a class="dropdown-toggle" data-toggle="dropdown"
                           href="#">Tin Tức <b class="caret"></b></a>
                        <ul class='dropdown-menu'>
                            <li><a href="#">Bản tin WORKLAB</a></li>
                            <li><a href="#">Tin công nghệ </a></li>
                            <li><a href="#">Tin tuyển dụng</a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-toggle" data-toggle="dropdown"
                           href="#">chia sẻ kiến thức <b class="caret"></b></a>
                        <ul class='dropdown-menu'>
                            <li><a href="#">Lập trình ứng dụng</a></li>
                            <li><a href="#">Kiểm thử phần mềm</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('lich-khai-giang-moi-nhat')}}">Lịch khai giảng <span id="txt-new" style="position: relative; top: -11px; background-color: red">new</span></a>
                    </li>
                    <li><a href="#footer">Liên hệ</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

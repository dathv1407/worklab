@extends('client.index')
@section('content')
    <div id="banner" style="
	    background: url(/images/Cover04.png);
	    font-family: 'Josefin Sans', sans-serif;
    color: #fff;
    padding-top: 3em;
    padding-bottom: 3em;
    background-size: cover;
    background-repeat: no-repeat;
    background-position-y: -248px;
">
{{--        <a href="http://itplus-academy.edu.vn/Doanh-nghiep--Doi-tac.html" style="color: #fff;font-family: 'Josefin Sans', sans-serif;">--}}
{{--            <h2 style="margin: 0px; font-size: 5em; visibility: visible; animation-name: flipInX;" class="text-center wow flipInX">--}}
{{--                Doanh nghiệp &amp; Đối tác </h2>--}}
{{--        </a>--}}
    </div>
    <div class="container" id="">
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12">
                <div class="panel panel-default dobong wow zoomInDown" style="border-top: 3px solid rgb(0, 59, 168); visibility: visible; animation-name: zoomInDown;">
                    <div class="panel-body">
                        <h1 id="post-title">Giới thiệu về WORKLAB</h1>
                        <div id="detailpost">
                            <p style="text-align:center"><span style="font-size:36px"><span style="font-family:Arial,Helvetica,sans-serif">&nbsp;<strong>VỀ WORKLAB</strong></span></span></p>
                            <p><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Trung tâm đào tạo công nghệ WORKLAB được thành lập vào năm 2021 với sứ mệnh nghiên cứu, xây dựng và đào tạo đội ngũ nhân lực công nghệ thông tin chất lượng cao. Các chương trình đào tạo tại WORKLAB luôn luôn được cập nhật, cải tiến nhằm đem lại hiệu quả cao nhất cho học viên.</strong></span></span></p>
                             <!--<p><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Học viên tốt nghiệp tại Trung tâm sẽ có đủ năng lực về kiến thức nghiệp vụ cũng như kỹ năng mềm để đáp ứng có thể làm việc trong môi trường hội nhập quốc tế.&nbsp;</span></span></p>-->
                            <img  style="margin-left: 55px;" src="images/vison2.png" alt="vision">

                            <p style="text-align:center"><span style="font-size:36px"><span style="font-family:Arial,Helvetica,sans-serif">&nbsp;<strong>GIÁ TRỊ CỐT LÕI</strong></span></span></p>
                            <ul>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Con người:</strong> WORKLAB lấy học viên là trung tâm, đội ngũ giảng viên có nghiệp vụ sư phạm, kinh nghiệm thực tế, luôn mang tới cho học viên những giá trị tốt nhất.</span></span></li>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Chất lượng:</strong> Chất lượng giảng dạy và hỗ trợ học viên trong quá trình làm việc là ưu tiên hàng đầu của WORKLAB. Chúng tôi cam kết chất lượng tới từng học viên sau mỗi khóa học.</span></span></li>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Môi trường đào tạo:</strong> Chuyên nghiệp, tập trung vào thực hành, với mô hình dự án thu nhỏ trong mỗi khóa học, chuẩn bị cho học viên sẵn sàng tâm lý và trang bị thêm nhiều kinh nghiệm trong quá trình làm việc.</span></span></li>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Giáo trình:</strong> Giáo trình được biên soạn, review kỹ lưỡng, dựa trên những dự án thực tế và được cập nhật thường xuyên để phù hợp với học viên cũng như đáp ứng theo xu thế phát triển của công nghệ.</span></span></li>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Cơ hội mở rộng mạng lưới:</strong>  Với mỗi học viên của WORKLAB, chúng tôi tạo sự gắn kết trước, trong và sau khóa học. Đặc biệt, với môi trường học tập đa dạng, số lượng học viên phong phú, học viên có cơ hội kết nối mở rộng mối quan hệ trong công việc, có nhiều cơ hội hợp tác trong tương lai.</span></span></li>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif"><strong>Uy tín:</strong> Động lực phát triển của WORKLAB là sự tín nhiệm của học viên. Ngoài ra, trong mỗi khóa học, WORKLAB luôn thực hiện các cuộc khảo sát, lấy ý kiến của học viên nhằm mục đích nâng cao chất lượng và giảng dạy.</span></span></li>
                            </ul>
                            <p style="text-align:center"><span style="font-size:36px"><span style="font-family:Arial,Helvetica,sans-serif">&nbsp;<strong>CƠ SỞ VẬT CHẤT</strong></span></span></p>
                            <ul>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Cơ sở: LK4-TT1, Số 96-96B Nguyễn Huy Tưởng, Thanh Xuân, Hà Nội.</span></span></li>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Trang thiết bị: Phòng học được trang bị đầy đủ thiết bị học tập hiện đại.</span></span></li>
                            </ul>
                            <p style="text-align:center"><span style="font-size:36px"><span style="font-family:Arial,Helvetica,sans-serif">&nbsp;<strong>CAM KẾT SAU ĐÀO TẠO</strong></span></span></p>
                            <ul>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Học viên tham gia các khóa học tại WORKLAB sẽ có đủ khả năng làm việc thực tế tại các doanh nghiệp chỉ sau 1 khóa học bất kì. </span></span></li>
                                <li><span style="font-size:14px"><span style="font-family:Arial,Helvetica,sans-serif">Với mạng lưới liên kết tuyển dụng với các công ty công nghệ trên cả nước, WORKLAB cam kết hỗ trợ tìm kiếm việc làm cho mỗi học viên sau khóa học.</span></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                {{--<h4>Bài viết cùng chủ đề</h4>
                <div class="row" id="">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post wow flipInX" data-wow-delay="0s" style="visibility: hidden; animation-delay: 0s; animation-name: none;">
                            <div class="panel panel-default dobong">
                                <div class="panel-heading" style="padding: 0px;overflow: hidden;max-height: 150px">
                                    <a href="http://itplus-academy.edu.vn/le-ki-ket-hop-tac-giua-cong-ty-co-phan-ho-tro-truong-hoc-viet-nam-va-itplus-academy-3131.html">
                                        <img style="min-height: 300px" src="http://itplus-academy.edu.vn//upload/071663969d40ee0e4a5c57251c1a8993/files/vina5.jpg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="panel-body" style="color:#fff;background: #3498db;min-height:80px;font-weight: bold">
                                    <a style="color: #fff" href="http://itplus-academy.edu.vn/le-ki-ket-hop-tac-giua-cong-ty-co-phan-ho-tro-truong-hoc-viet-nam-va-itplus-academy-3131.html">
                                        <p>
                                            Lễ kí kết hợp tác giữa công ty cổ phần hỗ trợ trường học việ ...
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post wow flipInX" data-wow-delay="0.1s" style="visibility: hidden; animation-delay: 0.1s; animation-name: none;">
                            <div class="panel panel-default dobong">
                                <div class="panel-heading" style="padding: 0px;overflow: hidden;max-height: 150px">
                                    <a href="http://itplus-academy.edu.vn/le-ki-ket-hop-tac-giua-cong-ty-tnhh-nh-n-luc-reco-va-itplus-academy-3130.html">
                                        <img style="min-height: 300px" src="http://itplus-academy.edu.vn//upload/071663969d40ee0e4a5c57251c1a8993/files/vina8.jpg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="panel-body" style="color:#fff;background: #3498db;min-height:80px;font-weight: bold">
                                    <a style="color: #fff" href="http://itplus-academy.edu.vn/le-ki-ket-hop-tac-giua-cong-ty-tnhh-nh-n-luc-reco-va-itplus-academy-3130.html">
                                        <p>
                                            Lễ kí kết hợp tác giữa công ty tnhh nhân lực reco và itplus ...
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post wow flipInX" data-wow-delay="0.2s" style="visibility: hidden; animation-delay: 0.2s; animation-name: none;">
                            <div class="panel panel-default dobong">
                                <div class="panel-heading" style="padding: 0px;overflow: hidden;max-height: 150px">
                                    <a href="http://itplus-academy.edu.vn/itplus-academy-tham-du-le-trao-bang-tot-nghiep-nam-2020-khoa-cong-nghe-thong-tin-va-truyen-thong-dai-hoc-phuong-dong-ha-noi-3005.html">
                                        <img style="min-height: 300px" src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/1%20(1).jpg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="panel-body" style="color:#fff;background: #3498db;min-height:80px;font-weight: bold">
                                    <a style="color: #fff" href="http://itplus-academy.edu.vn/itplus-academy-tham-du-le-trao-bang-tot-nghiep-nam-2020-khoa-cong-nghe-thong-tin-va-truyen-thong-dai-hoc-phuong-dong-ha-noi-3005.html">
                                        <p>
                                            Itplus academy tham dự lễ trao bằng tốt nghiệp năm 2020 khoa ...
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post wow flipInX" data-wow-delay="0.3s" style="visibility: hidden; animation-delay: 0.3s; animation-name: none;">
                            <div class="panel panel-default dobong">
                                <div class="panel-heading" style="padding: 0px;overflow: hidden;max-height: 150px">
                                    <a href="http://itplus-academy.edu.vn/le-ky-ket-hop-tac-giua-hoc-vien-cong-nghe-thong-tin-itplus-va-cao-dang-nghe-cong-nghiep-ha-noi-2978.html">
                                        <img style="min-height: 300px" src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2020/1(5).jpg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="panel-body" style="color:#fff;background: #3498db;min-height:80px;font-weight: bold">
                                    <a style="color: #fff" href="http://itplus-academy.edu.vn/le-ky-ket-hop-tac-giua-hoc-vien-cong-nghe-thong-tin-itplus-va-cao-dang-nghe-cong-nghiep-ha-noi-2978.html">
                                        <p>
                                            Lễ ký kết hợp tác giữa itplus academy và cao đẳng nghề công ...
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post wow flipInX" data-wow-delay="0.4s" style="visibility: hidden; animation-delay: 0.4s; animation-name: none;">
                            <div class="panel panel-default dobong">
                                <div class="panel-heading" style="padding: 0px;overflow: hidden;max-height: 150px">
                                    <a href="http://itplus-academy.edu.vn/le-ky-ket-hop-tac-giua-hoc-vien-cong-nghe-thong-tin-itplus-va-vien-sang-tao-va-chuyen-doi-so-2841.html">
                                        <img style="min-height: 300px" src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2020/2a.jpg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="panel-body" style="color:#fff;background: #3498db;min-height:80px;font-weight: bold">
                                    <a style="color: #fff" href="http://itplus-academy.edu.vn/le-ky-ket-hop-tac-giua-hoc-vien-cong-nghe-thong-tin-itplus-va-vien-sang-tao-va-chuyen-doi-so-2841.html">
                                        <p>
                                            Lễ ký kết hợp tác giữa itplus academy và viện sáng tạo và ch ...
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post wow flipInX" data-wow-delay="0.5s" style="visibility: hidden; animation-delay: 0.5s; animation-name: none;">
                            <div class="panel panel-default dobong">
                                <div class="panel-heading" style="padding: 0px;overflow: hidden;max-height: 150px">
                                    <a href="http://itplus-academy.edu.vn/le-ky-ket-hop-tac-giua-hoc-vien-cong-nghe-thong-tin-itplus-va-cao-dang-cong-nghe-thuong-mai-ha-noi-2829.html">
                                        <img style="min-height: 300px" src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2020/1(2).jpg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="panel-body" style="color:#fff;background: #3498db;min-height:80px;font-weight: bold">
                                    <a style="color: #fff" href="http://itplus-academy.edu.vn/le-ky-ket-hop-tac-giua-hoc-vien-cong-nghe-thong-tin-itplus-va-cao-dang-cong-nghe-thuong-mai-ha-noi-2829.html">
                                        <p>
                                            Lễ ký kết hợp tác giữa itplus academy và cao đẳng công nghệ ...
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}
            </div>
            {{--<div class="col-md-3">
                <div class="panel panel-default dobong">
                    <div class="panel-heading" style="background: #003ba8;color:#fff;font-weight: bold;text-transform: uppercase;">
                        Tin nổi bật
                    </div>
                    <div class="panel-body">

                        <div class="media" style="border-bottom: solid 1px #f1f1f1">
                            <div class="media-left media-middle">
                                <a href="http://itplus-academy.edu.vn/online-training-lap-trinh-sql-va-nosql---tat-tan-tat-ve-database-trong-lap-trinh-3446.html">
                                    <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/Untitled-1.jpg" class="media-object" style="width:60px;height: 60px;border-radius: 5px;">
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="http://itplus-academy.edu.vn/online-training-lap-trinh-sql-va-nosql---tat-tan-tat-ve-database-trong-lap-trinh-3446.html">
                                    <h4 class="media-heading" style="font-size: 1em">Online training lập trình: "sql và nosql - tất tần tật về database trong lập trình”</h4>
                                </a>
                            </div>
                        </div>

                        <div class="media" style="border-bottom: solid 1px #f1f1f1">
                            <div class="media-left media-middle">
                                <a href="http://itplus-academy.edu.vn/july-scholarship-uu-dai-50-hoc-phi-chi-tu-5000000d-cho-01-khoa-hoc-06-thang-3428.html">
                                    <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/900x600(11).jpg" class="media-object" style="width:60px;height: 60px;border-radius: 5px;">
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="http://itplus-academy.edu.vn/july-scholarship-uu-dai-50-hoc-phi-chi-tu-5000000d-cho-01-khoa-hoc-06-thang-3428.html">
                                    <h4 class="media-heading" style="font-size: 1em">July scholarship – ưu đãi 50% học phí – chỉ từ 5.000.000đ cho 01 khóa học 06 tháng</h4>
                                </a>
                            </div>
                        </div>

                        <div class="media" style="border-bottom: solid 1px #f1f1f1">
                            <div class="media-left media-middle">
                                <a href="http://itplus-academy.edu.vn/bao-nhip-song-so---itplus-academy-nhan-giai-sao-khue-lan-thu-04-cho-cac-chuong-trinh-dao-tao-ky-nang-cntt-va-thiet-ke-do-hoa---truyen-thong-da-phuong-tien-3362.html">
                                    <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/1a-.jpg" class="media-object" style="width:60px;height: 60px;border-radius: 5px;">
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="http://itplus-academy.edu.vn/bao-nhip-song-so---itplus-academy-nhan-giai-sao-khue-lan-thu-04-cho-cac-chuong-trinh-dao-tao-ky-nang-cntt-va-thiet-ke-do-hoa---truyen-thong-da-phuong-tien-3362.html">
                                    <h4 class="media-heading" style="font-size: 1em">Báo nhịp sống số - itplus academy nhận giải sao khuê lần thứ 04 cho "các chương trình đào tạo kỹ năng cntt và thiết kế đồ họa - truyền thông đa phương</h4>
                                </a>
                            </div>
                        </div>

                        <div class="media" style="border-bottom: solid 1px #f1f1f1">
                            <div class="media-left media-middle">
                                <a href="http://itplus-academy.edu.vn/itplus-academy-khai-truong-chi-nhanh-mien-nam-tai-quan-5-tp-hcm-3303.html">
                                    <img src="http://itplus-academy.edu.vn//upload/e0299984838d38ecac3805d4d6661829/files/2021/1a(3).jpg" class="media-object" style="width:60px;height: 60px;border-radius: 5px;">
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="http://itplus-academy.edu.vn/itplus-academy-khai-truong-chi-nhanh-mien-nam-tai-quan-5-tp-hcm-3303.html">
                                    <h4 class="media-heading" style="font-size: 1em">Itplus academy khai trương chi nhánh miền nam tại quận 5, tp hcm</h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/">
                    <img style="width: 100%" src="http://itplus-academy.edu.vn/public/images/khoahoc.gif" class="img-responsive hidden-xs hidden-sm dobong" alt="Image">
                </a>
            </div>--}}
        </div>
    </div>
@endsection

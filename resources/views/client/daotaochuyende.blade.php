@extends('client.index')
@section('content')
    <div id="banner" style="
	    background: url(/images/cover02.png);
	    font-family: 'Josefin Sans', sans-serif;
    color: #fff;
    padding-top: 3em;
    padding-bottom: 3em;
    background-size: cover;
    background-repeat: no-repeat;
    background-position-y: -248px;
">
{{--        <h2 style="margin: 0px;font-size: 5em" class="text-center text-uppercase">--}}
{{--            Đào tạo chuyên đề--}}
{{--        </h2>--}}
    </div>
    <div style="background: #f1f1f1;padding-top: 30px;padding-bottom: 30px">
        {{--<div class="container">
            <p class="title-text">
                Lịch khai giảng mới nhất
            </p>
            <div class="row">
                <div class="col-md-12">
                    <table style="min-height: auto!important" class="table table-bordered table-hover product-item">
                        <thead>
                        <tr>
                            <th>Khóa học</th>
                            <th>Ngày Khai giảng</th>
                            <th>Cơ sở/Hình thức học</th>
                            <th>Thời gian học</th>
                            <th>Lịch học</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listCourseDetail as $course)
                            <tr>
                                <td>
                                    <a target="_blank"
                                       href="{{route('detail-course-client',['id' => $course->id])}}">
                                        {{$course->name}} </a>
                                </td>
                                <td>{{$course->start_date}}</td>
                                <td>{{$course->local}}</td>
                                <td>{{$course->times}}</td>
                                <td>{{$course->schedule}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>--}}
        @foreach($courses as $course)
        <div class="container">
            <p class="title-text">
                {{$course->name}}
            </p>
            <div class="row">
                @foreach($listCourseDetail as $item)
                    @if($course->id == $item->id_course)
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="product-item" style="min-height: auto">
                            <div class="pi-img-wrapper">
                                <img src="{{asset($item->url_homepage_avatar)}}" class="img-responsive"
                                     alt="image">
                                <div>
                                    <a href="{{route('chi-tiet-khoa-hoc',['name' => Str::slug($item->name), 'id' => $item->id])}}"
                                       class="btn">Xem chi tiết</a>
                                </div>
                            </div>
                            <h3>
                                <a href="{{route('chi-tiet-khoa-hoc',['name' => Str::slug($item->name), 'id' => $item->id])}}">{{$item->name}}</a></h3>
                            <p class="text-sm short-text">Khóa học được xây dựng với mục tiêu trang bị cho Học viên những kiến thức đầy đủ nhất.</p>
                            <a href="{{route('chi-tiet-khoa-hoc',['name' => Str::slug($item->name), 'id' => $item->id])}}l"
                               class="btn add2cart pull-left">Xem chi tiết</a>
                            <a href="#" class="btn add2cart">Đăng ký học</a>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
        @endforeach
    </div>
@endsection

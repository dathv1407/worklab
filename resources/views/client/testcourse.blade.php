
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Khóa học tester tại ITPlus Academy - Hỗ trợ việc làm</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="fb:app_id" content="1213750372079302" />


    <meta name="keywords" content="học tester, kiểm thử phần mềm" />
    <meta name="description" content="Khóa học tester tại ITPlus Academy - Hỗ trợ việc làm" />
    <meta name="theme-color" content="#16479c">
    <meta property="og:image" content="http://itplus-academy.edu.vn/public/images/banner.png" />
    <meta property="og:url" content="http://itplus-academy.edu.vn/Khoa-hoc-Kiem-thu-phan-mem-Tester.html" />
    <meta property="og:title" content="Khóa học tester tại ITPlus Academy - Hỗ trợ việc làm" />
    <meta property="og:description" content="Khóa học tester tại ITPlus Academy - Hỗ trợ việc làm" />
    <meta property="og:type" content="article" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="landing-page/css/bootstrap.min.css">
    <link rel="stylesheet" href="landing-page/Tester/css/animate.min.css">
    <link rel="stylesheet" href="landing-page/Tester/stylesheets/site.css">
    <link rel="stylesheet" href="landing-page/css/my_style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="landing-page/template/hand.css">


    <style>
        .qheader{
            padding: 15px;
            text-transform: uppercase;
            background: url(http://itplus-academy.edu.vn/templates/home/v2/images/background.png?v=1) no-repeat;
        }
        .gray a{
            color:#000!important;
        }
        body{
            font-family: 'Roboto', sans-serif;
        }

        .outline-success{
            padding:0px;
            background: #27ae60!important;
            color: #fff;
        }
        .gray{
            color: black;
        }
        #cfacebook {
            position: fixed;
            bottom: 0;
            right: 10px;
            z-index: 999999999999999;
            height: auto;
            box-shadow: 6px 6px 6px 10px rgba(0, 0, 0, 0.2);
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            overflow: hidden;
        }

        #cfacebook .fchat {
            float: left;
            width: 100%;
            height: 270px;
            overflow: hidden;
            display: none;
            background-color: #fff;
        }

        #cfacebook .fchat .fb-page {
            margin-top: -130px;
            float: left;
        }

        #cfacebook a.chat_fb {

            display: block;
            text-align: center;
            font-size: 14px;
            padding: 15px 15px;
            margin: 0;
            line-height: 0;
            font-weight: bold;

            color: #fff;
            text-decoration: none;
            text-shadow: 0 1px 0 rgba(0, 0, 0, 0.1);

            background-color: #3a5795;
            border: 0;
            border-bottom: 1px solid #133783;
            z-index: 9999999;
        }

        #cfacebook a.chat_fb:hover {
            color: yellow;
            text-decoration: none;
        }
        a.btn.btn-success.menu-head{
            color: #fff!important;
        }
        a.btn.btn-success.menu-head:hover{
            color: #000!important;
        }
    </style>
    <script src="https://unpkg.com/vue"></script>
    <script>

        function addLink() {
            //Get the selected text and append the extra info
            var selection = window.getSelection(),
                pagelink = '<br /><br /> Xem thêm tại: ' + document.location.href,
                copytext = selection + pagelink,
                newdiv = document.createElement('div');

            //hide the newly created container
            newdiv.style.position = 'absolute';
            newdiv.style.left = '-99999px';

            //insert the container, fill it with the extended text, and define the new selection
            document.body.appendChild(newdiv);
            newdiv.innerHTML = copytext;
            selection.selectAllChildren(newdiv);

            window.setTimeout(function () {
                document.body.removeChild(newdiv);
            }, 100);
        }

        document.addEventListener('copy', addLink);
        $(document).ready(function(){

            $(".chat_fb").click(function() {
                $('.fchat').toggle('slow');
            });

            /*
                     $('.carousel').carousel({
                           interval: 3000
                       });*/
        });
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-56596608-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body id="myapp" data-spy="scroll" data-target=".navbar-nav" data-offset="50">
<script>
    $(document).on('click', 'a.menu-head[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });
</script>
<header class="qheader">
    <nav class="navbar navbar-default" style="background: none;border: none" role="navigation">
        <div class="container" style="margin-top:20px">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a style="padding: 0;" class="navbar-brand" href="/"><img style="position: relative;top: -34px;left: 84px;width: 150px;" src="logo.jpg" class="img-responsive" alt="Image"></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a class=" menu-head" href="\">Trang chủ</a></li>
                    <li><a class=" menu-head" href="#nd">Nội Dung Khóa Học</a></li>
                    <li><a class=" menu-head" href="#khuyenhoc">Chính Sách Khuyến Học</a></li>
                    <li><a class=" menu-head" href="#lienhe">Liên Hệ</a></li>
                    <li><a class="btn btn-success menu-head" href="javascript:void(0)">Hotline: 0963 97 65 65</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>


<script src="/landing-page/Tester/js/wow.min.js"></script>
<script src="/landing-page/Tester/js/vue.min.js"></script>
<script src="/landing-page/Tester/js/site.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=1213750372079302";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<style type="text/css">
    #cfacebook .fchat2 {
        float: left;
        width: 100%;
        height: 270px;
        overflow: hidden;
        display: none;
        background-color: #fff;
    }

    #cfacebook .fchat2 .fb-page {
        margin-top: -130px;
        float: left;
    }
</style>
<div id="app">



    <div class="section1">
        <img src="landing-page/Tester/img/s.jpg" class="img img-responsive">
    </div>

    <div class="section2 wow fadeIn">
        <div class="container">
            <div class="col-md-6 col-sm-6 text-center">
                <h3>đối tượng tham gia</h3>
                <p>
                    Tất cả các bạn muốn trở thành những tester chuyên nghiệp<br>
                    Sinh viên khoa CNTT các trường Trung cấp - Cao đẳng - Đại học<br>
                    Học viên tại ITPlus sau khi kết thúc phần Lập trình căn bản
                </p><br>
                <img src="landing-page/Tester/img/point.jpg" class="img img-responsive ball"><br><br>
                <img src="landing-page/Tester/img/line.jpg" class="img img-responsive line">
            </div>
            <div class="col-md-6 col-sm-6">
                <img src="landing-page/Tester/img/dttg-img.jpg" class="img img-responsive">
            </div>
        </div>
    </div>

    <div class="section3">
        <div class="container">
            <div class="col-md-6 col-sm-6">
                <img src="landing-page/Tester/img/banner2.png" class="img img-responsive">
            </div>
            <div class="col-md-6 col-sm-6">
                <h3>mục tiêu khóa học</h3>
                <ul>
                    <li><img src="landing-page/Tester/img/list.png">Hiểu được các khái niệm cơ bản liên quan đến kiểm thử, quá trình kiểm thử, phương pháp kiểm thử, và các nguyên tắc để hỗ trợ mục tiêu thử nghiệm.</li>
                    <li><img src="landing-page/Tester/img/list.png">Cung cấp cho HV các kiến thức về kiểm thử phần mềm một cách có hệ thống từ cơ bản đến nâng cao.</li>
                    <li><img src="landing-page/Tester/img/list.png">Rèn luyện và phát triển kỹ năng kiểm thử phần mềm của các HV thông qua các tình huống kiểm thử phần mềm thực tế.</li>
                    <li><img src="landing-page/Tester/img/list.png">Cung cấp, cập nhật những công nghệ kiểm thử phần mềm hiện đại nhất đang được ứng dụng trên thế giới.</li>
                    <li><img src="landing-page/Tester/img/list.png">Trang bị cho HV những kỹ năng mềm cần thiết để HV có thể làm công việc kiểm thử phần mềm một cách chuyên nghiệp trong thực tế cũng như thích nghi nhanh với công việc của chuyên viên kiểm thử phần mềm tại các tập đoàn phần mềm đa quốc gia.</li>
                    <li><img src="landing-page/Tester/img/list.png">Phân tích kiểm thử cho nhóm chức năng (funtional) và nhóm phi chức năng (non-funtional: như hiệu suất và khả năng mở rộng) ở tất cả các cấp độ kiểm thử từ thấp đến mức cao.</li>
                    <li><img src="landing-page/Tester/img/list.png">Thực hiện kiểm thử theo kế hoạch và kịch bản kiểm thử, phân tích và báo cáo kết quả kiểm thử.</li>
                    <li><img src="landing-page/Tester/img/list.png">Làm quen và sử dụng các công cụ hỗ trợ trong quá trình kiểm thử.</li>
                </ul>
                <img src="landing-page/Tester/img/line2.png" class="img img-responsive line">
            </div>
        </div>
    </div>

    <div class="section4">
        <div class="container">
            <h3>Thu hoạch sau khóa học</h3>
            <div class="col-md-4 col-sm-4 col-xs-8 col-md-offset-0 col-sm-offset-0 col-xs-offset-2">
                <div class="w10">1</div><br>
                <div class="w80">Được trang bị kiến thức chuyên sâu vững chắc, minh họa thực tế về kiểm thử phần mềm.</div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-8 col-md-offset-0 col-sm-offset-0 col-xs-offset-2">
                <div class="w10">2</div><br>
                <div class="w80">Cung cấp các kiến thức căn bản để dự thi chứng chỉ ISTQB® Certified Foundation</div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-8 col-md-offset-0 col-sm-offset-0 col-xs-offset-2">
                <div class="w10">3</div><br>
                <div class="w80">Được rèn luyện kỹ năng kiểm thử trên những bài tập thực hành có tính thực tế cao, được các chuyên gia đúc kết từ kinh nghiệm thực hiện dự án.</div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-8 col-md-offset-0 col-sm-offset-0 col-xs-offset-2">
                <div class="w10">4</div><br>
                <div class="w80">Được chia sẻ những kinh nghiệm thực tế từ các chuyên gia nên sau khi học xong, học viên sẽ có kiến thức thực tiến rất tốt.</div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-8 col-md-offset-0 col-sm-offset-0 col-xs-offset-2">
                <div class="w10">5</div><br>
                <div class="w80">Có thêm kinh nghiệm và kiến thức kiểm thử cần thiết để có thể xây dựng phần mềm chuyên nghiệp hơn, chấy lượng hơn.</div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-8 col-md-offset-0 col-sm-offset-0 col-xs-offset-2">
                <div class="w10">6</div><br>
                <div class="w80">Được trang bị đầy đủ kiến thức, kỹ năng thực tiễn cần thiết để có thể ứng tuyển vào vị trí Tester, QA tại các công ty trong và ngoài nước.</div>
            </div>
        </div>
        <img src="landing-page/Tester/img/line.jpg" class="img img-responsive line">
    </div>

    <div id="ndd" class="section5">
        <div class="container">
            <h3>nội dung khóa học</h3>
            <div class="row">
                <div class="panel-group" id="accordion4">
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item41" href="#item41">
                        <div slot="title">
                            <span class="tit">Phần 1.</span> Cơ bản về kiểm thử (Testing Software)
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Mô hình phát triển phần mềm</li>
                                <li>Tại sao cần phải kiểm thử phần mềm</li>
                                <li>Kiểm thử phần mềm là gì?</li>
                                <li>7 nguyên tắc của kiểm thử</li>
                                <li>Quy trình kiểm thử phần mềm</li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item42" href="#item42">
                        <div slot="title">
                            <span class="tit">Phần 2.</span> Lập Kế hoạch kiểm thử(Test Plan) và viết kịch bản kiểm thử Test Case
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Phân tích yêu cầu khách hàng</li>
                                <li>Lập Kế hoạch kiểm thử</li>
                                <li>Cách thiết kế kịch bản kiểm thử</li>
                                <li>Thực hành trên dự án</li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item43" href="#item43">
                        <div slot="title">
                            <span class="tit">Phần 3.</span> Các kỹ thuật kiểm thử và viết kịch bản kiểm thử Test Case
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Các kỹ thuật kiểm thử</li>
                                <li>Thực hành viết kịch bản kiểm thử</li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item44" href="#item44">
                        <div slot="title">
                            <span class="tit">Phần 4.</span> Thực thi kiểm thử và Báo cáo kết quả kiểm thử
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Thực hiện kiểm thử phần mềm</li>
                                <li>Báo cáo kết quả kiểm thử</li>
                                <li>Ghi nhận Bug</li>
                                <li>Phân tích đánh giá kết quả kiểm thử</li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item45" href="#item45">
                        <div slot="title">
                            <span class="tit">Phần 5.</span> Thực hành trên ứng dụng Web
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Phân tích yêu cầu</li>
                                <li>Lập kế hoạch kiểm thử</li>
                                <li>Viết kịch bản kiểm thử</li>
                                <li>Thực hiện kiểm thử</li>
                                <li>Báo cáo và đánh giá kết quả</li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item46" href="#item46">
                        <div slot="title">
                            <span class="tit">Phần 6.</span> Thực hành trên ứng dụng Window
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Phân tích yêu cầu</li>
                                <li>Lập kế hoạch kiểm thử</li>
                                <li>Viết kịch bản kiểm thử</li>
                                <li>Thực hiện kiểm thử</li>
                                <li>Báo cáo và đánh giá kết quả</li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item47" href="#item47">
                        <div slot="title">
                            <span class="tit">Phần 7.</span> Thực hành trên ứng dụng SmartPhone
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Phân tích yêu cầu</li>
                                <li>Lập kế hoạch kiểm thử</li>
                                <li>Viết kịch bản kiểm thử</li>
                                <li>Thực hiện kiểm thử</li>
                                <li>Báo cáo và đánh giá kết quả</li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item48" href="#item48">
                        <div slot="title">
                            <span class="tit">Phần 8.</span> Kiểm thử tự động (Automation Testing)
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Giới thiệu về Automation Testing</li>
                                <li>Thực hành Automation Testing</li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default wow bounceInRight" parent="#accordion4" id="item49" href="#item49">
                        <div slot="title">
                            <span class="tit">Phần 9.</span> Kiểm tra cuối khóa và định hướng nghề nghiệp
                        </div>
                        <div slot="body">
                            <ul>
                                <li>Kiểm tra cuối khóa</li>
                                <li>Tư vấn ban đầu khi bước vào nghề testing</li>
                                <li>Hướng dẫn viết CV, trả lời phỏng vấn vị trí tester ở các công ty</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <h4 class="title">Tài nguyên khóa học</h4>
            <div class="more">
                <div>
                    <a target="_blank" href="http://logbug.itplus-academy.edu.vn">Ứng dụng Logbugs</a>
                </div>
                <div>
                    <a target="_blank" href="http://kshop.itplus-academy.edu.vn">Ứng dụng kiểm thử</a>
                </div>
            </div>
        </div>
    </div>

    <div class="section6">
        <div class="container">
            <div class="col-md-6 col-sm-6 col-xs-8 col-md-offset-0 col-sm-offset-0 col-xs-offset-2">
                <p class="title">Đánh giá cuối khóa dựa vào bài thực hành và kiểm tra<br>Cách tính điểm và xếp loại Chứng chỉ:</p>
                <p>Thang điểm được sử dụng trong toàn bộ thời gian đào tạo là 100. Dựa vào bảng dưới để xếp loại cho Chứng chỉ của Khóa.</p>
                <br>
                <p class="title">Tài liệu tham khảo:</p>
                <p>Testing Computer Software, 2nd. <i><a href="">Tải xuống</a></i><br>
                    Edition Paperback - April 12, 1999. <i><a href="">Tải xuống</a></i></p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-8 col-md-offset-0 col-sm-offset-0 col-xs-offset-2">
                <div class="table-responsive">
                    <table>
                        <tr class="th-row">
                            <th class="brtl">Xếp loại</th>
                            <th>Điểm</th>
                            <th class="brtr">Chú giải</th>
                        </tr>
                        <tr>
                            <td>A</td>
                            <td>91 - 100</td>
                            <td>Xuất sắc</td>
                        </tr>
                        <tr>
                            <td>B</td>
                            <td>81 - 90</td>
                            <td>Giỏi</td>
                        </tr>
                        <tr>
                            <td>C</td>
                            <td>71 - 80</td>
                            <td>Khá</td>
                        </tr>
                        <tr>
                            <td>D</td>
                            <td>65 - 70</td>
                            <td>Đạt</td>
                        </tr>
                        <tr>
                            <td>E</td>
                            <td>< 65</td>
                            <td>Không đạt</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <img src="landing-page/Tester/img/line.jpg" class="img img-responsive">
    </div>

    <div id="khuyenhoc" class="section10 section10_text">
        <div class="col-md-6 col-md-offset-3 table-responsive">
            <p>CHÍNH SÁCH HỖ TRỢ HỌC PHÍ</p>
            <table class="table table-bordered">
                <tr>
                    <th class="text-bold">KHÓA HỌC</th>
                    <th class="text-bold">CHÍNH SÁCH HỖ TRỢ</th>
                    <th class="text-bold" style="width: 160px">HỌC PHÍ CÒN LẠI</th>
                </tr>
                <tr>
                    <td class="color_red"> Kiểm thử phần mềm</td>
                    <td class="text-bold">Hỗ trợ <span class="color_red" id="uudai"></span> học phí khi đăng ký trước ngày <span id="handangky"></span> </td>
                    <td class="color_red" id="hocphi"></td>
                </tr>
            </table>
            <p class="text-right">HOTLINE: <span style="color: #0094c5; font-style: italic">0963 97 65 65​</span></p>
        </div>
        <div class="col-md-6 col-md-offset-3 table-responsive">
            <p>LỊCH HỌC</p>
            <table class="table table-bordered">
                <tr>
                    <th class="color_red text-bold" style="width: 150px">LỊCH HỌC</th>
                    <th class="color_red text-bold" style="width: 200px">GIỜ HỌC</th>
                    <th class="color_red text-bold">GHI CHÚ</th>
                </tr>
                <tr>
                    <td class="text-bold">
                        2 buổi / tuần <br>
                        <span style="font-weight: normal">(3 giờ / buổi)</span>
                    </td>
                    <td class="text-bold">
                        18h - 21h
                    </td>
                    <td class="text-bold">Ngày học linh động phù hợp cho học viên và giảng viên</td>
                </tr>
            </table>
        </div>
        <div class="col-md-12">
            <a href="http://itplus-academy.edu.vn/dang-ky.html" class="btn btn-dangky">ĐĂNG KÝ</a>
        </div>
    </div>

{{--    <div class="container section7">--}}
{{--        <div class="fb-comments" data-href="http://itplus-academy.edu.vn/Khoa-hoc-Kiem-thu-phan-mem-Tester.html" data-numposts="5" width="100%"></div>--}}
{{--    </div>--}}








    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(".chat_fb").click(function() {
                jQuery('.fchat2').toggle('slow');
            });
        });
    </script>
</div>



<div id="histats_counter" style="height:0px; position:absolute; left:-1000000px;"></div>


<script type="text/javascript">var _Hasync = _Hasync || [];
    _Hasync.push(['Histats.start', '1,4181741,4,6,200,40,00000001']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function () {
        var hs = document.createElement('script');
        hs.type = 'text/javascript';
        hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
<noscript><a href="/" target="_blank"><img src="//sstatic1.histats.com/0.gif?4181741&101" alt="" border="0"></a>
</noscript>

<style>
    .social{display:flex;justify-content:center;margin-top:3rem}.social .social__custom{display:flex;justify-content:center;align-items:center;padding:10px;border:1px solid #15489d;color:#15489d;border-radius:50%;margin:5px;width:50px;height:50px;transition:background-color .1s;text-decoration:none}.social .social__custom:hover{background-color:#102152}.social .social__custom i{font-size:2rem}#lienhe h3,#lienhe h4,#lienhe i,#lienhe strong{color:#16479c}
</style>
<div id="lienhe" style="padding-top: 30px;color: #16479c!important; background-color: #f1f1f1;border-top:8px solid rgb(21 72 157);">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="http://itplus-academy.edu.vn/">
                    <img style="width: 359px; height: 200px; max-width: 200%!important;" src="logo.jpg" class="img-responsive" alt="ITPLUS ACADEMY - VIỆN CÔNG NGHỆ THÔNG TIN - ĐẠI HỌC QUỐC GIA HÀ NỘI">
                </a>
                <div class="social">
                    <a href="https://www.facebook.com/HocVienITPlusAcademy" class="social__custom">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UC4BfQ_jkANjwtPmKK3TSq1w" class="social__custom">
                        <i class="fa fa-youtube"></i>
                    </a>
                    <a href="/cdn-cgi/l/email-protection#51383f373e113825213d24227c30323035343c287f3435247f273f" class="social__custom">
                        <i class="fa fa-envelope"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-8">
                <h3 style="text-transform:uppercase">Hệ thống cơ sở đào tạo</h3>
                <div class="row">
                    <div class="col-md-6">
                        <h4 style="border-bottom: 1px solid rgb(21 72 157); display: inline-block; padding-bottom: 5px;">Cơ sở Hà Nội</h4>
                        <p style="color:#16479c;"><strong>Cơ sở 1</strong>: Tầng 2, Trung tâm đào tạo CNTT và Truyền Thông, Số 1 Hoàng Đạo Thúy, Quận Thanh Xuân, Hà Nội.</p>
                        <p style="color:#16479c;"><strong>Cơ sở 2</strong>: Tầng 1, Nhà A2, Trường Đại học Sân Khấu - Điện Ảnh Hà Nội. Đường Hồ Tùng Mậu, Phường Mai Dịch, Quận Cầu Giấy, Hà Nội</p>
                        <p style="color:#16479c;"><strong>Cơ sở 3</strong>: Tầng 7, Số 51, Đường Lê Đại Hành, Phường Lê Đại Hành, Quận Hai Bà Trưng, Hà Nội</p>
                        <p style="color:#16479c;"><strong>Điện thoại</strong>: 024 3754 6732</p>
                        <p style="color:#16479c;"><strong>Hotline</strong>: 0963 97 65 65</p>
                        <p style="color:#16479c;"><strong>Email</strong>: <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="8fe6e1e9e0cfe6fbffe3fafca2eeeceeebeae2f6a1eaebfaa1f9e1">[email&#160;protected]</a></p>
                    </div>
                    <div class="col-md-6">
                        <h4 style="border-bottom: 1px solid rgb(21 72 157); display: inline-block; padding-bottom: 5px;">Cơ sở TP.HCM</h4>
                        <p style="color:#16479c;"><strong>Địa chỉ</strong>: Tầng Trệt, Tòa nhà M-H Building - Số 728-730 Võ Văn Kiệt, Phường 01, Quận 5, TP Hồ Chí Minh</p>
                        <p style="color:#16479c;"><strong>Điện thoại</strong>: 024 3754 6732</p>
                        <p style="color:#16479c;"><strong>Hotline</strong>: 0963 97 65 65</p>
                        <p style="color:#16479c;"><strong>Email</strong>: <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="94fcf7f9d4fde0e4f8e1e7b9f5f7f5f0f1f9edbaf1f0e1bae2fa">[email&#160;protected]</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyright" style="padding: 15px 0;background-color: #E5E6EC;border-top: 1px solid rgb(211 212 220);">
    <p class="text-center" style="padding: 0;margin: 0;color:#16479c;">Copyright © 2021 All Rights Reserved. Phát triển bởi ITPlus Academy</p>
</div>
<meta charset="utf-8">
<link rel="stylesheet" href="/facebook/facebook.css">




<div class="fb-livechat">
    <div class="ctrlq fb-overlay"></div>
{{--    <div class="fb-widget">--}}
{{--        <div class="ctrlq fb-close"></div>--}}
{{--        <div class="fb-page" data-href="https://www.facebook.com/HocVienITPlusAcademy" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"></div>--}}
{{--    </div>--}}
    <a style="margin-bottom:80px; padding:0; background-image: url('./public/images/phone_icon.png'); background-color:white;background-size:100%" href="tel:0963976565" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button">

    </a>
    <a href="https://m.me/HocVienITPlusAcademy" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button">
        <div class="bubble">1</div>

    </a></div>

<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript">
    var uniqueBrowserID;



    $(document).ready(function () {


        function detectmob() {
            if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
                return true;
            } else {
                return false;
            }
        }

        var t = {delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")};
        setTimeout(function () {
            $("div.fb-livechat").fadeIn()
        }, 8 * t.delay);
        if (!detectmob()) {
            $(".ctrlq").on("click", function (e) {
                e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({
                    bottom: 0,
                    opacity: 0
                }, 2 * t.delay, function () {
                    $(this).hide("slow"), t.button.show()
                })) : t.button.fadeOut("medium", function () {
                    t.widget.stop().show().animate({
                        bottom: "30px",
                        opacity: 1
                    }, 2 * t.delay), t.overlay.fadeIn(t.delay)
                })
            })
        }


        // Detect ios 11_x_x affected
        // NEED TO BE UPDATED if new versions are affected
        var ua = navigator.userAgent,
            iOS = /iPad|iPhone|iPod/.test(ua),
            iOS11 = /OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2|OS 11_2_1/.test(ua);

        // ios 11 bug caret position
        if (iOS && iOS11) {

            // Add CSS class to body
            $("body").addClass("iosBugFixCaret");

        }

        var auth = JSON.parse(localStorage.getItem("auth"));
        if (auth && auth.token) {
            handlePermission();
        }


    });
    // (function (d, s, id) {
    //     var js, fjs = d.getElementsByTagName(s)[0];
    //     if (d.getElementById(id)) return;
    //     js = d.createElement(s);
    //     js.id = id;
    //     js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1213750372079302";
    //     fjs.parentNode.insertBefore(js, fjs);
    // }(document, 'script', 'facebook-jssdk'));

    // facebook pixel
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1213750372079302');
    fbq('track', 'PageView');





    function removeStorage(name) {
        try {
            localStorage.removeItem(name);
            localStorage.removeItem(name + '_expiresIn');
        } catch (e) {
            console.log('removeStorage: Error removing key [' + key + '] from localStorage: ' + JSON.stringify(e));
            return false;
        }
        return true;
    }

    /*  getStorage: retrieves a key from localStorage previously set with setStorage().
        params:
            key <string> : localStorage key
        returns:
            <string> : value of localStorage key
            null : in case of expired key or failure
     */
    function getStorage(key) {

        var now = Date.now();  //epoch time, lets deal only with integer
        // set expiration for storage
        var expiresIn = localStorage.getItem(key + '_expiresIn');
        if (expiresIn === undefined || expiresIn === null) {
            expiresIn = 0;
        }

        if (expiresIn < now) {// Expired
            removeStorage(key);
            return null;
        } else {
            try {
                var value = localStorage.getItem(key);
                return value;
            } catch (e) {
                console.log('getStorage: Error reading key [' + key + '] from localStorage: ' + JSON.stringify(e));
                return null;
            }
        }
    }

    /*  setStorage: writes a key into localStorage setting a expire time
        params:
            key <string>     : localStorage key
            value <string>   : localStorage value
            expires <number> : number of seconds from now to expire the key
        returns:
            <boolean> : telling if operation succeeded
     */
    function setStorage(key, value, expires) {

        if (expires === undefined || expires === null) {
            expires = (24 * 60 * 60);  // default: seconds for 1 day
        } else {
            expires = Math.abs(expires); //make sure it's positive
        }

        var now = Date.now();  //millisecs since epoch time, lets deal only with integer
        var schedule = now + expires * 1000;
        try {
            localStorage.setItem(key, value);
            localStorage.setItem(key + '_expiresIn', schedule);
        } catch (e) {
            console.log('setStorage: Error setting key [' + key + '] in localStorage: ' + JSON.stringify(e));
            return false;
        }
        return true;
    }


    //react truyá»n call Ä‘á»ƒ má»Ÿ modal cv cá»§a react
    function openModalCV(callback) {
        $('#button-open-cv').click(function () {
            callback();
        });
    }

    function handlePermission() {
        navigator.permissions.query({name: 'geolocation'}).then(function (result) {
            if (result.state == 'granted') {
                report(result.state);
                navigator.geolocation.getCurrentPosition(showPosition);
            } else if (result.state == 'prompt') {
                report(result.state);
                navigator.geolocation.getCurrentPosition(showPosition);
            } else if (result.state == 'denied') {
                report(result.state);
            }
        });
    }

    function report(state) {
        console.log('Permission ' + state);
    }


    function showPosition(position) {
        var auth = JSON.parse(localStorage.getItem("auth"));
        if (auth && auth.token) {

            var url = "/api/v3/address-user-info?token=" + auth.token;
            axios
                .post(url, {
                    long: position.coords.longitude,
                    lat: position.coords.latitude
                })
                .then(
                    function () {
                    }
                )
        }

    }





</script><script>
    $(document).ready(function() {
        //thanghqcmtxoa Sinh nhat bac ho
        // $(".table-responsive").removeClass('table-responsive');//???ông nào viết
        $('#table-responsive').addClass('table-responsive');
    });
    $(document).ready(function() {
        if($('#uudai').length >0){
            console.log("TEST");
            $.ajax({
                url: '//itplus-academy.edu.vn/admin/Auth/API_hocphi_all/',
                type: 'GET',
                data: {url:'http://itplus-academy.edu.vn/Khoa-hoc-Kiem-thu-phan-mem-Tester.html',},
            })
                .done(function(rs) {
                    // console.log(rs);
                    // console.log(rs);
                    var response = JSON.parse(rs);
                    for (item of response)
                    {
                        switch (item.co_so) {
                            case '1':
                                $("#uudai").text(item.uudai);
                                $("#handangky").text(item.handangky);
                                $("#hocphi").text(item.hocphi + " VND");
                                break;
                            case '2':
                                $("#uudai_hcm").text(item.uudai);
                                $("#handangky_hcm").text(item.handangky);
                                $("#hocphi_hcm").text(item.hocphi + " VND");
                                break;
                        }
                    }
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    // console.log("complete");
                });
        }

    });

</script>


<script src="/public/owl/owl.carousel.min.js"></script>
<script>
    $(document).ready(function() {
        $(".owl-carousel-top").owlCarousel({
            loop: true,
            autoplay: true,
            items: 1,
            dots:false,
            autoplayTimeout:4000,
            autoplayHoverPause: true,
            animateOut: 'slideOutDown',
            animateIn: 'flipInX',
        });
    });
</script>
<script type="text/javascript">



    /* <![CDATA[ */

    var google_conversion_id = 1006975853;

    var google_custom_params = window.google_tag_params;

    var google_remarketing_only = true;

    /* ]]> */

</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">

</script>
<noscript>

    <div style="display:inline;">

        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1006975853/?guid=ON&amp;script=0"/>

    </div>

</noscript>

<script>

    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?

        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;

        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;

        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,

        document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '613420715479114'); // Insert your pixel ID here.

    fbq('track', 'PageView');

</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=613420715479114&ev=PageView&noscript=1"

    /></noscript>


</body>
</html>

@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row pb-5">
                <div class="col-md-12">
                    <form action="" enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update news detail</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputName">Headline</label>
                                    <input type="text" name="headline" class="form-control" value="{{ $new-> headline}}">
                                    @error('headline')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Image</label>
                                    <div>
                                        <input type="file" name="url_img" id="customFile">
                                        <img class="ml-2 pt-2" src="{{ asset('uploads/' . $new->url_img) }}" style="width: 100px; height: 100px">
                                    </div>
                                    @error('url_img')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="inputName">Summary</label>
                                    <input type="text" name="summary" class="form-control" value="{{ $new->summary }}">
                                    @error('summary')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Content</label>
                                    <textarea id="summernote1" name="content">
                                        {{ $new->content ?? '' }}
                                    </textarea>
                                    @error('content')
                                     <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <div class="row">
                            <div class="col-12">
                                <a href="{{route('news', ['id' => $new->id_news_category])}}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Update new" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Update news detail')

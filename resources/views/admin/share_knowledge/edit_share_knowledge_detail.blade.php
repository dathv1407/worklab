@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row pb-5">
                <div class="col-md-12">
                    <form action="{{route('submit-update-share-knowledge-detail')}}" enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update share knowledge detail</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputName">Headline</label>
                                    <input type="text" name="headline" class="form-control" value="{{ $knowledgeDetail-> headline}}">
                                    @error('headline')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Image</label>
                                    <div>
                                        <input type="file" name="url_img" id="customFile">
                                        <img class="ml-2 pt-2" src="{{ asset($knowledgeDetail->url_img) }}" style="width: 100px; height: 100px">
                                    </div>
                                    @error('url_img')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="inputName">Summary</label>
                                    <input type="text" name="summary" class="form-control" value="{{ $knowledgeDetail->summary }}">
                                    @error('summary')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Content</label>
                                    <textarea id="summernote1" name="content">
                                        {{ $knowledgeDetail->content ?? '' }}
                                    </textarea>
                                    @error('content')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="text" hidden value="{{$knowledgeDetail->id}}" name="id" class="form-control">
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <div class="row">
                            <div class="col-12">
                                <a href="{{route('list-share-knowledge-detail', ['id' => $knowledgeDetail->id_share_knowledge_category])}}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Update new" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Update share knowledge detail')

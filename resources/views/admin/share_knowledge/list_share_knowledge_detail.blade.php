@extends('admin.index')

@section('content')
    <!-- Main content -->
    <div class="pl-3 pb-3">
        <a href="{{ route('create-share-knowledge-detail', ['id' => $knowledgeCategory->id])}}">
            <button class="btn btn-primary"> Create new</button>
        </a>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 20%">
                                Headline
                            </th>
                            <th style="width: 20%">
                                Creator
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listDetail as $item)
                            <tr>
                                <td>
                                    #
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <a href="#">{{ $item->headline }}</a>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <span>{{$item->user->name}}</span>
                                    </ul>
                                </td>
                                <td class="project-actions">
                                    {{-- <a class="btn btn-primary btn-sm" href="news/{{$item->id}}">
                                        <i class="fas fa-folder">
                                        </i>
                                        View
                                    </a> --}}
                                    <a class="btn btn-info btn-sm" href="{{route('update-share-knowledge-detail', ['id' => $item->id])}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="#">
                                        <i class="fas fa-trash">
                                        </i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Danh sách tin '.$knowledgeCategory->name)

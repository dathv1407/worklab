@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Role
                                </th>
                                <th style="width: 20%">
                                    User Name
                                </th>
                                <th style="width: 8%" class="text-center">
                                    Status
                                </th>
                                <th style="width: 20%">
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listUser as $user)
                            <tr>
                                <td>
                                    #
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        @if ($user->level)
                                            <span>Admin</span>
                                        @else
                                            <span>User</span>
                                        @endif
                                    </ul>
                                </td>
                                <td>
                                    <a>
                                        {{ $user->name }}
                                    </a>
                                    <br>
                                    <small>
                                        {{ $user->create_at }}
                                    </small>
                                </td>
                                <td class="project-state">
                                    @if ($user->status)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-success">Stop</span>
                                    @endif
                                </td>
                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="#">
                                        <i class="fas fa-folder">
                                        </i>
                                        View
                                    </a>
                                    <a class="btn btn-info btn-sm" href="#">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="#">
                                        <i class="fas fa-trash">
                                        </i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'List Users')

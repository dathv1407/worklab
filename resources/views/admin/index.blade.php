@include('admin.layout.header')
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">

            <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown mr-4">
                <a href="logout">Logout</a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <span class="brand-text font-weight-light"><b>Admin WorkLab</b></span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="info">
                    <a href="#" class="d-block">Alexander Pierce</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item {{ Request::segment(2) === 'list-user' || Request::segment(2) === 'create-user' ? '  menu-is-opening menu-open' : '' }}">
                        <a href="{{route('route-list-user')}}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Users
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('route-list-user')}}" class="nav-link {{ Request::segment(2) === 'list-user' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List User</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('route-create-user')}}" class="nav-link {{ Request::segment(2) === 'create-user' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Create User</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'chuyen-de-dao-tao' || Request::segment(2) === 'luyen-chung-chi' ? '  menu-is-opening menu-open' : '' }}">
                        <a href="{{route('chuyen-de-dao-tao')}}" class="nav-link">
                            <i class="nav-icon fas fa-bars"></i>
                            <p>
                                Menu Client
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('chuyen-de-dao-tao')}}" class="nav-link {{ Request::segment(2) === 'chuyen-de-dao-tao' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Chuyên đề đào tạo</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('luyen-chung-chi')}}" class="nav-link {{ Request::segment(2) === 'luyen-chung-chi' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Luyện thi chứng chỉ</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'list-course' || Request::segment(2) === 'create-course' ? '  menu-is-opening menu-open' : '' }}">
                        <a href="{{route('list-course')}}" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                Course
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('list-course')}}" class="nav-link {{ Request::segment(2) === 'list-course' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List Course</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('create-course')}}" class="nav-link {{ Request::segment(2) === 'create-course' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Create Course</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'list-news-category' || Request::segment(2) === 'create-news-category' ? '  menu-is-opening menu-open' : '' }}">
                        <a href="{{route('list-news-category')}}" class="nav-link">
                            <i class="nav-icon fas fa-newspaper"></i>
                            <p>
                                News Category
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('list-news-category')}}" class="nav-link {{ Request::segment(2) === 'list-news-category' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List News Category</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('create-news-category')}}" class="nav-link {{ Request::segment(2) === 'create-news-category' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Create News Category</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'list-share-knowledge-category' || Request::segment(2) === 'create-share-knowledge-category' ? '  menu-is-opening menu-open' : '' }}">
                        <a href="{{route('list-news-category')}}" class="nav-link">
                            <i class="nav-icon far fa-plus-square"></i>
                            <p>
                                Share knowledge
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('list-share-knowledge-category')}}" class="nav-link {{ Request::segment(2) === 'list-share-knowledge-category' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>List Share knowledge</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('create-share-knowledge-category')}}" class="nav-link {{ Request::segment(2) === 'create-share-knowledge-category' ? ' active menu-open' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Create Share knowledge</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item {{ Request::segment(2) === 'list-message' ? 'active menu-open' : '' }}" >
                        <a href="{{route('list-message')}}" class="nav-link" >
                            <i class="fa fa-envelope nav-icon" aria-hidden="true"></i>
                            <p>
                                Messages
                            </p>
                            <span class="right badge badge-danger">{{$unreadMessage}}</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h1>@section('name')@show()</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        @section('content')
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-12">
                    <h1> this is content</h1>
                </div>
            </div>
        </section>
        <!-- /.content -->
        @show()

    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> 3.1.0
        </div>
        <strong>Copyright &copy; 2014-2021 <a href="#">AdminLTE.io</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('admin.layout.footer')

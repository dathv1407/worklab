@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 15%">
                                Title
                            </th>
                            <th style="width: 15%">
                                Name
                            </th>
                            <th style="width: 15%">
                                Email
                            </th>
                            <th style="width: 15%">
                                Phone
                            </th>
                            <th style="width: 15%">
                                Message
                            </th>
                            <th style="width: 15%">
                                Create
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listMessage as $item)
                            @if($item->status)
                                <tr>
                            @else
                                <tr style="background-color: white">
                            @endif
                                    <td>
                                        #
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$item->title}}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$item->name}}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$item->email}}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$item->phone}}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$item->content}}
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-inline">
                                            {{$item->created_at}}
                                        </ul>
                                    </td>
                                    <td class="project-actions">
                                        <a class="btn btn-info btn-sm" href="{{route('message-detail', ['id' => $item->id])}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            View
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Danh sach tin nhan')

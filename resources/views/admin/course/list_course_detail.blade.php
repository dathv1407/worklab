@extends('admin.index')

@section('content')
    <!-- Main content -->
    <div class="pl-3 pb-3">
        <a href="../create-course-detail/{{$course->id}}">
            <button class="btn btn-primary"> Create new Class</button>
        </a>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 20%">
                                Course Name
                            </th>
                            <th style="width: 15%">
                                Opening Schedule
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($list_course_detail as $item)
                            <tr>
                                <td>
                                    #
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <a href="{{route('list-main-content-course', ['id_course_detail' => $item->id])}}">{{ $item->name }}</a>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <a href="{{route('list-opening-schedule', ['id_course_detail' => $item->id])}}">List opening schedule</a>
                                    </ul>
                                </td>
                                <td class="project-actions">
                                    <a class="btn btn-info btn-sm" href="{{route('edit-course-detail', ['id' => $item->id])}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="#">
                                        <i class="fas fa-trash">
                                        </i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Danh sách '.$course->name)

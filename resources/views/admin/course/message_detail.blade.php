@extends('admin.index')

@section('content')
    <div class="pl-3 pb-3">
        <a href="{{route('list-message')}}">
            <button class="btn btn-primary"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button>
        </a>
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Title</th>
                            <th scope="col">{{$message->title}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Name</th>
                            <td>{{$message->name}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Email</th>
                            <td>{{$message->email}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Phone</th>
                            <td>{{$message->phone}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Message</th>
                            <td>{{$message->content}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Create</th>
                            <td>{{$message->created_at}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Chi tiet tin nhan')

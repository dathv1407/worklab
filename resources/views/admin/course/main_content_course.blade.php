@extends('admin.index')

@section('content')
    <!-- Main content -->
    <div class="pl-3 pb-3">
        <a href="{{route('create-main-course-content', ['id_course_detail' => $courseDetail->id])}}">
            <button class="btn btn-primary"> Create main content</button>
        </a>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 20%">
                                Title main content
                            </th>
                            <th style="width: 20%">
                                Add detail
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courseMainContent as $item)
                            <tr>
                                <td>
                                    #
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <a href="course/{{$item->id}}">{{ $item->title_content }}</a>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <a href="{{route('course-detail-content',['id_course_main_content' => $item->id])}}">Thêm nội dung</a>
                                    </ul>
                                </td>
                                <td class="project-actions">
                                    <a class="btn btn-info btn-sm" href="{{route('edit-course-main-content', ['id_course_main_content' => $item->id])}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="#">
                                        <i class="fas fa-trash">
                                        </i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Danh sách Main content: '.$courseDetail->name)

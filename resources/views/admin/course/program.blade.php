@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row pb-5">
                <div class="col-md-6">
                    <form action="submit-create-user" method="post">
                        {{ csrf_field() }}
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">General</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputName">User Name</label>
                                    <input type="text" name="txtUserName" id="inputName" class="form-control">
                                    @error('txtUserName')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Email</label>
                                    <input type="text" name="txtEmail" id="inputName" class="form-control">
                                    @error('txtEmail')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Password</label>
                                    <input type="password" name="txtPassword" id="inputName" class="form-control">
                                    @error('txtPassword')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Re-password</label>
                                    <input type="password" name="txtRepassword" id="inputName" class="form-control">
                                    @error('txtRepassword')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputStatus">Role</label>
                                    <select id="inputStatus" name="role" class="form-control custom-select">
                                        <option selected value="0">User</option>
                                        <option value="1">Admin</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputStatus">Status</label>
                                    <select id="inputStatus" name="status" class="form-control custom-select">
                                        <option selected value="1">Active</option>
                                        <option value="0">Stop</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <div class="row">
                            <div class="col-12">
                                <a href="list-user" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Create new User" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Khóa học lập trình')

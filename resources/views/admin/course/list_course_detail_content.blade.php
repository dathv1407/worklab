@extends('admin.index')

@section('content')
    <!-- Main content -->
    <div class="pl-3 pb-3">
        <a href="{{route('add-course-detail-content', ['id_course_main_content' => $courseMainContent->id])}}">
            <button class="btn btn-primary"> Create detail content</button>
        </a>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 20%">
                                Buổi học số
                            </th>
                            <th style="width: 20%">
                                Nội dung chính
                            </th>
                            <th style="width: 20%">
                                Nội dung chi tiết
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courseDetailContent as $key => $item)
                            <tr>
                                <td>
                                    {{ $key+1 }}
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        {{ $item->main_content }}
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        <span style="white-space: pre-wrap;">{{ $item->detail_content }}</span>
                                    </ul>
                                </td>
                                <td class="project-actions">
                                    <a class="btn btn-info btn-sm" href="{{route('edit-course-detail-content', ['id_course_detail_content' => $item->id])}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="#">
                                        <i class="fas fa-trash">
                                        </i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Nội dung chi tiết: '.$courseMainContent->title_content)

@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row pb-5">
                <div class="col-md-6">
                    <form action="{{route('submit-edit-opening-schedule')}}" method="post">
                        {{ csrf_field() }}
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">General</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputName">Start Date</label>
                                    <input type="date" value="{{$item->start_date}}" name="start_date" class="form-control">
                                    @error('start_date')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Local</label>
                                    <input type="text" value="{{$item->local}}" name="local" class="form-control">
                                    @error('local')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Time (18h-20h)</label>
                                    <input type="text" value="{{$item->time}}" name="time" class="form-control">
                                    @error('times')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Schedule (Thứ 2 - Thứ 6)</label>
                                    <input type="text" value="{{$item->schedule}}" name="schedule" class="form-control">
                                    @error('schedule')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Original Tuition</label>
                                    <input type="text" value="{{$item->original_tuition}}" name="original_tuition" class="form-control">
                                    @error('original_tuition')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Discount(%)</label>
                                    <input type="text" value="{{$item->discount}}" name="discount" class="form-control">
                                    @error('discount')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Expire date discount</label>
                                    <input type="date" value="{{$item->expire_date}}" name="expire_date" class="form-control">
                                    @error('expire_date')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Note</label>
                                    <textarea type="text" name="note" class="form-control">{{$item->note}}</textarea>
                                    @error('note')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="text" hidden value="{{$item->id}}" name="id" class="form-control">
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <div class="row">
                            <div class="col-12">
                                <a href="{{route('list-opening-schedule', ['id_course_detail' => $item->id_course_detail])}}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Update" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Tạo khóa học '.$item->name)

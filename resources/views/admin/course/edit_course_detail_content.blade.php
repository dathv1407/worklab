@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 pb-5">
                    <form action="{{route('submit-edit-course-detail-content', ['id_course_detail_content' => $courseDetailContent->id ])}}" method="post">
                        {{ csrf_field() }}
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Cập nhật nội dung chi tiết</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputName">Buổi số {{$courseDetailContent->id}}</label>
                                    <input type="text" hidden value="{{$courseDetailContent->id}}" name="id" id="inputName" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Nội dung chính</label>
                                    <input type="text" value="{{$courseDetailContent->main_content}}" name="main_content" id="inputName" class="form-control">
                                    @error('main_content')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Nội dung chi tiết</label>
                                    <textarea type="text" name="detail_content" id="inputName" class="form-control">{{$courseDetailContent->detail_content}}</textarea>
                                    @error('detail_content')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <div class="row">
                            <div class="col-12">
                                <a href="{{route('course-detail-content', ['id_course_main_content' => $courseDetailContent->id_course_main_content])}}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Create" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', $courseMainContent->title_content)

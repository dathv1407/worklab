@extends('admin.index')

@section('content')
    <div class="pl-3 pb-3">
        <a href="{{route('list-register', ['id' => $register->id])}}">
            <button class="btn btn-primary"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button>
        </a>
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">{{$register->student_name}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">Email</th>
                            <td>{{$register->email}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Phone</th>
                            <td>{{$register->phone}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Message</th>
                            <td>{{$register->message}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Create</th>
                            <td>{{$register->created_at}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Khoa hoc '.$courseDetail->name.' khai giang '.$openSchedule->start_date)

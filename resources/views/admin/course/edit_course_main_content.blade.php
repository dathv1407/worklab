@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 pb-5">
                    <form action="{{route('submit-edit-course-main-content')}}" enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Chỉnh sửa nội dung</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputName">Image of Content</label>
                                    <div>
                                        <input type="file" name="image_content" id="customFile">
                                    </div>
                                    <img class="ml-2 pt-2" src="{{asset($courseMainContent->url_image_content)}}" style="width: 100px; height: 100px">
                                    @error('image_content')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Title content</label>
                                    <input type="text" value="{{$courseMainContent->title_content}}" name="title_content" id="inputName" class="form-control">
                                    @error('title_content')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="text" hidden value="{{$courseMainContent->id}}" name="id_course_main_content" id="inputName" class="form-control">
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <div class="row">
                            <div class="col-12">
                                <a href="{{route('list-main-content-course', ['id_course_detail' => $courseMainContent->id_course_detail])}}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Update" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', $courseDetail->name)

@extends('admin.index')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row pb-5">
            <div class="col-md-6">
                <form action="{{route('submit-edit-course-detail', ['id' => $courseDetail->id])}}" enctype="multipart/form-data" method="post">
                    {{ csrf_field() }}
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Info 1</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Course Name</label>
                                <input type="text" value="{{$courseDetail->name}}" name="courseName" class="form-control">
                                @error('courseName')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputName">Information Summary</label>
                                <textarea id="summernote1" name="information">
                                    {{$courseDetail->information}}
                                </textarea>
                                @error('information')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputName">Teacher</label>
                                <textarea id="summernote2" name="teacher">
                                        {{$courseDetail->teacher}}
                                </textarea>
                                @error('teacher')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputName">Note</label>
                                <textarea type="text" name="note" class="form-control">{{$courseDetail->note}}</textarea>
                                @error('note')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputName">Document</label>
                                <textarea id="summernote3" name="document" class="form-control">
                                    {{$courseDetail->document}}
                                </textarea>
                                @error('document')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputName">Homepage Avatar</label>
                                <div>
                                    <input type="file" name="homepage_avatar" id="customFile">
                                </div>
                                <img class="ml-2 pt-2" src="{{asset($courseDetail->url_homepage_avatar)}}" style="width: 100px; height: 100px">
                                @error('homepage_avatar')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputName">Image top landing page</label>
                                <div>
                                    <input type="file" name="image_top" id="customFile">
                                </div>
                                <img class="ml-2 pt-2"  src="{{asset($courseDetail->url_image_top)}}" style="width: 100px; height: 100px">
                                @error('image_top')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                                <label for="inputName">Image before information</label>
                                <div>
                                    <input type="file" name="image_information" id="customFile">
                                </div>
                                <img class="ml-2 pt-2" src="{{asset($courseDetail->image_information)}}" style="width: 100px; height: 100px">
                                @error('image_information')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div> --}}
                            <div class="form-group">
                                <label for="inputName">Number student</label>
                                <input type="text" class="form-control" name="number_student" value="{{$courseDetail->number_student}}">
                                @error('number_student')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputName">Total day study</label>
                                <input type="text" class="form-control" name="day_study" value="{{$courseDetail->day_study}}">
                                @error('day_study')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputName">Total hour study</label>
                                <input type="text" class="form-control" name="hour_study" value="{{$courseDetail->hour_study}}">
                                </textarea>
                                @error('hour_study')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <div class="row">
                        <div class="col-12">
                            <a href="{{route('course', ['id' => $courseDetail->id_course])}}" class="btn btn-secondary">Cancel</a>
                            <input type="submit" value="Update" class="btn btn-success float-right">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('name', 'Edit khóa học  '.$courseDetail->name)

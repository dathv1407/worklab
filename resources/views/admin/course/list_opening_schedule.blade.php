@extends('admin.index')

@section('content')
    <!-- Main content -->
    <div class="pl-3 pb-3">
        <a href="{{route('add-opening-schedule',['id_course_detail' => $courseDetail->id])}}">
            <button class="btn btn-primary"> Create new Class</button>
        </a>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 15%">
                                Start date
                            </th>
                            <th style="width: 15%">
                                Local
                            </th>
                            <th style="width: 15%">
                                Time
                            </th>
                            <th style="width: 15%">
                                Schedule
                            </th>
                            <th style="width: 15%">
                                Original tuition
                            </th>
                            <th style="width: 5%">
                                Discount
                            </th>
                            <th style="width: 5%">
                                List student
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listOpeningSchedule as $item)
                            <tr>
                                <td>
                                    #
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        {{$item->start_date}}
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        {{$item->local}}
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        {{$item->time}}
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        {{$item->schedule}}
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        {{$item->original_tuition}}
                                    </ul>
                                </td>
                                <td>
                                    <ul class="list-inline">
                                        {{$item->discount}}
                                    </ul>
                                </td>
                                <td>
                                    <a href="{{route('list-register',['id' => $item->id])}}">View</a>
                                </td>
                                <td class="project-actions">
                                    <a class="btn btn-info btn-sm" href="{{route('edit-opening-schedule', ['id_opening_schedule' => $item->id])}}">
                                        <i class="fas fa-pencil-alt">
                                        </i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="#">
                                        <i class="fas fa-trash">
                                        </i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Danh sách '.$courseDetail->name)

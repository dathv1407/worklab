@extends('admin.index')

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row pb-5">
                <div class="col-md-6">
                    <form action="{{route('submit-create-course-detail')}}" enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">General</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputName">Course Name</label>
                                    <input type="text" name="courseName" class="form-control" value="{{ old('courseName') }}">
                                    @error('courseName')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Homepage Avatar</label>
                                    <div>
                                        <input type="file" name="homepage_avatar" id="customFile">
                                    </div>
                                    @error('homepage_avatar')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Image top landing page</label>
                                    <div>
                                        <input type="file" name="image_top" id="customFile">
                                    </div>
                                    @error('image_top')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                {{-- <div class="form-group">
                                    <label for="inputName">Image before information</label>
                                    <div>
                                        <input type="file" name="image_information" id="customFile">
                                    </div>
                                    @error('image_information')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div> --}}
                                <div class="form-group">
                                    <label for="inputName">Information Summary</label>
                                    <textarea id="summernote1" name="information">
                                        {{old('information') }}
                                    </textarea>
{{--                                    <textarea type="text" name="information" class="form-control"></textarea>--}}
                                    @error('information')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Teacher</label>
                                    <textarea id="summernote2" name="teacher" >
                                        {{old('teacher') }}
                                    </textarea>
                                    @error('teacher')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Note</label>
                                    <textarea type="text" name="note" class="form-control"> {{old('note') }}</textarea>
                                    @error('note')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Document</label>
                                    <textarea id="summernote3" name="document" >
                                        {{ old('document') }}
                                    </textarea>
                                    @error('document')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Number student</label>
                                    <input type="text" name="number_student" class="form-control" value="{{ old('number_student') }}">
                                    @error('number_student')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Total day study</label>
                                    <input type="text" name="day_study" class="form-control" value="{{ old('day_study') }}">
                                    @error('day_study')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Total hour study</label>
                                    <input type="text" name="hour_study" class="form-control" value="{{ old('hour_study') }}">
                                    </textarea>
                                    @error('hour_study')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="text" name="course" hidden value="{{$course->id}}" class="form-control">
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <div class="row">
                            <div class="col-12">
                                <a href="{{route('course', ['id' => $course->id])}}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Create new" class="btn btn-success float-right">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('name', 'Tạo khóa học '.$course->name)

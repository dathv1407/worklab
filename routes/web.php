<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([ 'namespace' => 'Client'], function() {
// Route client
    Route::get('/', 'HomeController@index')->name('homepage');
    Route::get('/gioi-thieu', 'HomeController@introduce')->name('client.introduce');
    Route::get('dao-tao-chuyen-de/{id}', 'HomeController@daotaochuyende')
    ->name('dao-tao-chuyen-de');
    Route::get('/luyen-chung-chi', function () {
        return view('client/daotaochuyende');
    });
    Route::get('/php-training', function () {
        return view('client/phpcourse');
    })->name('php-training');
    Route::get('chi-tiet-khoa-hoc/{id}/{name}', 'HomeController@detailCourse')
        ->name('chi-tiet-khoa-hoc');
    Route::get('/test-training', function () {
        return view('client/testcourse');
    })->name('test-training');
    Route::get('lich-khai-giang-moi-nhat', 'HomeController@lichKhaiGiang')
        ->name('lich-khai-giang-moi-nhat');

    Route::get('/lien-he', 'HomeController@contact')->name('client.contact');

    // New route
    Route::post('course-register', 'HomeController@courseRegister')
        ->name('course-register');
    Route::post('send-message', 'HomeController@sendMessage')
        ->name('send-message');

    Route::get('list-news', 'HomeController@getListNewsLatest')
        ->name('technology-news');
    Route::get('list-news/{id}/{slug}', 'HomeController@getListNews')
        ->name('list-news');
    Route::get('detail-news/{id}/{slug}', 'HomeController@getDetailNews')
        ->name('detail-news');
    Route::get('list-knowledge', 'HomeController@getListKnowledgeLatest')
        ->name('list-knowledge');
    Route::get('list-knowledge/{id}/{slug}', 'HomeController@getListKnowledgeCategory')
        ->name('list-knowledge-category');
    Route::get('detail-share-knowledge/{id}/{slug}', 'HomeController@getShareKnowledgeDetail')
        ->name('detail-share-knowledge');

});

//Route admin
Route::get('admin/login', 'Admin\LoginController@getLogin');
Route::post('admin/checkLogin', 'Admin\LoginController@checkLogin');
Route::get('admin/logout', 'Admin\LoginController@logout');

Route::group(['middleware' => 'authAdmin', 'prefix' => 'admin', 'namespace' => 'Admin'], function() {

    //  User
    Route::get('list-user', 'UserController@getUser')->name('route-list-user');
    Route::get('create-user', function() {
        return view('admin.user.create_user');
    })->name('route-create-user');
    Route::post('submit-create-user', 'UserController@createUser');

    //Menu
    Route::get('chuyen-de-dao-tao', 'MenuClientController@getTraining')
        ->name('chuyen-de-dao-tao');
    Route::post('submit-create-theme', 'SubMenuController@createTheme')
        ->name('submit-create-theme');
    Route::get('luyen-chung-chi', 'MenuClientController@getCertification')
        ->name('luyen-chung-chi');
    Route::post('submit-create-certification', 'SubMenuController@createCertification')
        ->name('submit-create-certification');
    // edit chuyen de dao tao
    Route::get('edit-chuyen-de-dao-tao/{id}', 'SubMenuController@getViewEdit')
        ->name('edit-chuyen-de-dao-tao');
    Route::post('submit-edit-chuyen-de/{id}', 'SubMenuController@editChuyenDe')
        ->name('submit-edit-chuyen-de');

    //Course
    Route::get('list-course', 'CourseController@getCourse')
        ->name('list-course');
    Route::get('create-course', function() {
        return view('admin.course.create_course');
    })->name('create-course');
    Route::get('detail-course', function() {
        return view('admin.course.list_course_detail');
    })->name('detail-course');
    Route::post('submit-create-course', 'CourseController@createCourse');
    Route::get('course/{id}', 'CourseController@getListCourseDetail')
    ->name('course');
    Route::get('create-course-detail/{id}', 'CourseController@getViewCourseDetail');
    Route::post('submit-create-course-detail', 'CourseController@createCourseDetail')
    ->name('submit-create-course-detail');

    // edit course detail
    Route::get('edit-course-detail/{id}', 'CourseController@getViewEditCourseDetail')
        ->name('edit-course-detail');
    Route::post('submit-edit-course-detail/{id}', 'CourseController@updateCourseDetail')
        ->name('submit-edit-course-detail');
    // content of course
    Route::get('list-main-content-course/{id_course_detail}', 'CourseController@getListMainContentCourseDetail')
        ->name('list-main-content-course');
    Route::get('create-main-course-content/{id_course_detail}', 'CourseController@getViewCreateCourseMainContent')
        ->name('create-main-course-content');
    Route::post('submit-create-course-content', 'CourseController@createCourseMainContent')
        ->name('submit-create-course-content');
    Route::get('edit-course-main-content/{id_course_main_content}', 'CourseController@getViewEditCourseMainContent')
        ->name('edit-course-main-content');
    Route::post('submit-edit-course-main-content', 'CourseController@editCourseMainContent')
        ->name('submit-edit-course-main-content');
    Route::get('course-detail-content/{id_course_main_content}', 'CourseController@getViewCourseDetailContent')
        ->name('course-detail-content');
    Route::get('add-course-detail-content/{id_course_main_content}', 'CourseController@getViewAddCourseDetailContent')
        ->name('add-course-detail-content');
    Route::post('submit-add-course-detail-content', 'CourseController@addCourseDetailContent')
        ->name('submit-add-course-detail-content');
    Route::get('edit-course-detail-content/{id_course_detail_content}', 'CourseController@getViewEditCourseDetailContent')
        ->name('edit-course-detail-content');
    Route::post('submit-edit-course-detail-content/{id_course_detail_content}', 'CourseController@editCourseDetailContent')
        ->name('submit-edit-course-detail-content');
    Route::get('list-opening-schedule/{id_course_detail}', 'CourseController@getListOpeningSchedule')
        ->name('list-opening-schedule');
    Route::get('add-opening-schedule/{id_course_detail}', 'CourseController@getViewAddOpeningSchedule')
        ->name('add-opening-schedule');
    Route::post('submit-add-opening-schedule', 'CourseController@addOpeningSchedule')
        ->name('submit-add-opening-schedule');
    Route::get('edit-opening-schedule/{id_opening_schedule}', 'CourseController@getViewEditOpeningSchedule')
        ->name('edit-opening-schedule');
    Route::post('submit-edit-opening-schedule', 'CourseController@editOpeningSchedule')
        ->name('submit-edit-opening-schedule');
    Route::get('list-register/{id}', 'CourseController@listRegister')
        ->name('list-register');
    Route::get('register-info/{id}', 'CourseController@getRegister')
        ->name('register-info');
    Route::get('list-message', 'MessageController@listMessage')
        ->name('list-message');
    Route::get('message-detail/{id}', 'MessageController@messageDetail')
        ->name('message-detail');

    //News
    Route::get('list-news-category', 'NewsController@getNewsCategory')
    ->name('list-news-category');

    Route::get('edit-new-category/{id}', 'NewsController@editNewCategory')->name('edit-new-category');
    Route::post('edit-new-category/{id}', 'NewsController@updateNewCategory');

    Route::get('delete-new-category/{id}', 'NewsController@deleteNewCategory')->name('delete-new-category');

    Route::get('create-news-category', function() {
        return view('admin.news.create_news_category');
    })->name('create-news-category');

    Route::post('submit-create-news-category', 'NewsController@createNewsCategory');
    Route::get('news/{id}', 'NewsController@getListNews')
    ->name('news');

    Route::get('create-news/{id}', 'NewsController@getViewCreateNews')->name('create-news');
    Route::post('submit-new-course-detail', 'NewsController@createNewDetail')
    ->name('submit-create-new-detail');

    Route::get('edit-new/{id}', 'NewsController@editNew')->name('edit-new');
    Route::post('edit-new/{id}', 'NewsController@updateNew');

    // chia sẻ kiến thức
    Route::get('list-share-knowledge-category', 'ShareKnowledgeController@getShareKnowledgeCategory')
    ->name('list-share-knowledge-category');
    Route::get('create-share-knowledge-category', function() {
        return view('admin.share_knowledge.create_share_knowledge_category');
    })->name('create-share-knowledge-category');

    Route::post('create-share-knowledge-category', 'ShareKnowledgeController@createShareKnowledgeCategory');

    Route::get('edit-share-knowledge-category/{id}', 'ShareKnowledgeController@editShareKnowledgeCategory')->name('edit-share-knowledge-category');

    Route::get('delete-share-knowledge-category/{id}', 'ShareKnowledgeController@deleteShareKnowledgeCategory')->name('delete-share-knowledge-category');

    Route::get('list-share-knowledge-detail/{id}', 'ShareKnowledgeController@getShareKnowledgeDetail')
        ->name('list-share-knowledge-detail');
    Route::get('create-share-knowledge-detail/{id}', 'ShareKnowledgeController@getViewCreateShareKnowledgeDetail')
        ->name('create-share-knowledge-detail');
    Route::post('submit-create-share-knowledge-detail', 'ShareKnowledgeController@createShareKnowledgeDetail')
        ->name('submit-create-share-knowledge-detail');
    Route::get('update-share-knowledge-detail/{id}', 'ShareKnowledgeController@viewUpdateShareKnowledgeDetail')
        ->name('update-share-knowledge-detail');
    Route::post('submit-update-share-knowledge-detail', 'ShareKnowledgeController@updateShareKnowledgeDetail')
        ->name('submit-update-share-knowledge-detail');
});

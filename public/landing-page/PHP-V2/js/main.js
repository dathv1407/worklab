/***********************
 *Create by Pham Phuong *
 ************************/
const base_url = "//itplus-academy.edu.vn";

function showMore(num) {
    var dots = document.getElementById(`dots_${num}`);
    var moreText = document.getElementById(`more_${num}`);
    var btnText = document.getElementById(`btnShowMore_${num}`);

    if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Xem thêm <i class='icon-angle-down'></i>";
        moreText.style.display = "none";
    } else {
        dots.style.display = "none";
        btnText.innerHTML = "Thu gọn <i class='icon-angle-up'></i>";
        moreText.style.display = "inline";
    }
}

function addLink() {
    //Get the selected text and append the extra info
    var selection = window.getSelection(),
        pagelink = '<br /><br /> Xem thêm tại: ' + document.location.href,
        copytext = selection + pagelink,
        newdiv = document.createElement('div');

    //hide the newly created container
    newdiv.style.position = 'absolute';
    newdiv.style.left = '-99999px';

    //insert the container, fill it with the extended text, and define the new selection
    document.body.appendChild(newdiv);
    newdiv.innerHTML = copytext;
    selection.selectAllChildren(newdiv);

    window.setTimeout(function () {
        document.body.removeChild(newdiv);
    }, 100);
}

(function (AOS, $) {
    AOS.init({
        duration: 800,
        easing: 'slide',
        once: false
    });
    //add link
    document.addEventListener('copy', addLink);
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos <= 0 || prevScrollpos > currentScrollPos) {
            document.getElementsByClassName("js-sticky-header")[0].style.top = "0";
        } else {
            document.getElementsByClassName("js-sticky-header")[0].style.top = "-120px";
        }
        prevScrollpos = currentScrollPos;
    }
    $(document).ready(function(){
        // $('[data-toggle="popover"]').popover({
        //     trigger: 'focus',
        //     placement: 'bottom'
        // });
        $('[data-toggle="popover"]').popover({
            trigger: 'focus',
            placement: 'bottom'
        }).click(function(e) {
            $(this).popover('toggle');
        });
        if($('#uudai').length >0){
            // console.log("TEST");
            $.ajax({
                url: '//itplus-academy.edu.vn/admin/Auth/API_hocphi_all/',
                type: 'GET',
                data: {url:'http://itplus-academy.edu.vn/Khoa-hoc-thiet-ke-va-lap-trinh-website-PHP-chuyen-nghiep.html',},
            })
                .done(function(rs) {

                    // console.log(rs);
                    var response=JSON.parse(rs);
                    console.log(response);
                    for (item of response)
                    {
                        switch (item.co_so) {
                            case '1':
                                $("#uudai").text(item.uudai);
                                $("#handangky").text(item.handangky);
                                $("#hocphi").text(item.hocphi + " VND");
                                break;
                            case '2':
                                $("#uudai_hcm").text(item.uudai);
                                $("#handangky_hcm").text(item.handangky);
                                $("#hocphi_hcm").text(item.hocphi + " VND");
                                break;
                        }
                    }
                    // $("#uudai").text(response.uudai);
                    // $("#handangky").text(response.handangky);
                    // $("#hocphi").text(response.hocphi+ " VND");
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    // console.log("complete");
                });
        }
    });
    //fancybox
    $("img.fancybox").fancybox();
    $(document).ready(function(){
        $('.pp-slick-doanhnghiep-wrap').slick({
            dots: true,
            // infinite: false,
            // speed: 300,
            slidesToShow: 3,
            autoplay: true,
            slidesToScroll: 1,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
    //vue
    var vm = new Vue({

        el: "#app",
        data() {
            return {
                base: '//itplus-academy.edu.vn/',
                sanphams: [],
                htmlCss: [
                    {
                        title: 'Giới thiệu ngôn ngữ HTML',
                        content: `
                        <ul>
                            <li>Các khái niệm cơ bản Webpage, Website, Internet, HTML</li>
                            <li>Cấu trúc trang HTML:
                                <ul>
                                <li>Thẻ định dạng văn bản, thẻ định dạng khối, thuộc tính Style</li>
                                <li>Thẻ liên kết (a), địa chỉ tương đối – tuyệt đối và các loại liên kết</li>
                                <li>Thẻ chèn ảnh (img). Thẻ tạo danh sách: ol, ul</li>
                                </ul>
                            </li>
                        </ul>
                        `,
                        class: 'pp-blue'
                    },
                    {
                        title: 'Làm việc với Bảng và Form',
                        content: `
                        <ul><li>Cấu trúc và thuộc tính thẻ tạo bảng. Kỹ thuật thao tác trên bảng</li> <li>Thẻ form và các thuộc tính</li> <li>Các thành phần trong thẻ form: Thẻ input và các loại type:text, checkbox, radio, button, submit, reset...select, textarea, button…</li></ul>
                       `,
                        class: 'pp-blue'
                    },
                    {
                        title: 'Giới thiệu về HTML5',
                        content: `
                        <ul><li>Giới thiệu HTML5. Các thẻ mới trong HTML5</li> <li>Chèn video – audio với HTML5</li> <li>Giới thiệu thẻ canvas</li></ul>
                       `,
                        class: 'pp-green'
                    },
                    {
                        title: 'Giới thiệu CSS',
                        content: `
                            <ul>
                                <li>Giới thiệu CSS3: Khái niệm, tác dụng của CSS</li>
                                <li>Bộ chọn trong CSS</li>
                                <li>Các loại bảng định kiểu & thứ tự ưu tiên</li>
                                <li>Thuộc tính định dạng font: color, font-family, font-size, font-style, font-weight, text-decoration, text-transform...</li>
                                <li>Thuộc tính định dạng khối: width, height, border, background, margin, padding</li>
                                <li>Thuộc tính hiệu ứng: display, position, float, z-index</li>
                            </ul>
                       `,
                        class: 'pp-green'
                    },
                    {
                        title: 'Giới thiệu CSS3',
                        content: `
                            <ul>
                                <li>Giới thiệu CSS3 và một số thuộc tính css3:  border-radius, box-sizing, box-shadow, text-shadow,  transform, transition, resize</li>
                                <li>Tìm hiểu RGBA, gradient</li>
                                <li>Multi Column</li>
                            </ul>
                       `,
                        class: 'pp-orange'
                    },
                    {
                        title: 'Giới thiệu về JavaScript',
                        content: `
                            <ul>
                                <li>Giới thiệu JavaScript, công cụ debug trên trình duyệt web</li>
                                <li>Cú pháp lệnh cơ bản, biến, kiểu dữ liệu</li>
                                <li>Toán tử</li>
                                <li>Lệnh điều khiển rẽ nhánh, lệnh lặp</li>
                            </ul>
                       `,
                        class: 'pp-orange'
                    },
                    {
                        title: 'Hàm và Đối tượng trong Javascript',
                        content: `
                            <ul>
                                <li>Hàm có sẵn và định nghĩa hàm</li>
                                <li>Tạo và sử dụng đối tượng</li>
                            </ul>
                       `,
                        class: 'pp-yellow'
                    },
                    {
                        title: 'HTML DOM',
                        content: `
                            <ul>
                                <li>Giới thiệu HTML DOM</li>
                                <li>Biểu thức quy tắc</li>
                                <li>Kiểm tra hợp lệ dữ liệu trên form</li>
                            </ul>
                       `,
                        class: 'pp-yellow'
                    },
                    {
                        title: 'Giới thiệu Jquery',
                        content: `
                            <ul>
                                <li>Giới thiệu về Jquery, cách sử dụng jquery</li>
                                <li>Giới thiệu các loại bộ chọn sử dụng với Jquery:  Jquery CSS Selector, các selector riêng của jQuery</li>
                                <li>Các phương thức điều khiển thuộc tính thẻ html</li>
                                <li>Các sự kiện tương tác thẻ html</li>
                                <li>Các hiệu ứng với jquery</li>
                                <li>Sử dụng ajax post – get trong jquery</li>
                            </ul>
                       `,
                        class: 'pp-red'
                    },
                    {
                        title: 'Giới thiệu bootstrap',
                        content: `
                            <ul>
                                <li>Giới thiệu về bootstrap: giới thiệu, cài đặt bootstrap, cấu trúc thư mục bootstrap, hướng dẫn tra cứu bootstrap document</li>
                                <li>Làm việc với các Component: Alert, badge, Form, Collapse, nav, pagination, list, text, background, table, dropdown, Modal</li>
                            </ul>
                       `,
                        class: 'pp-red'
                    },
                    {
                        title: 'Responsive',
                        content: `
                            <ul>
                                <li>Responsive</li>
                                <li>Làm việc với Layout</li>
                            </ul>
                       `,
                        class: 'pp-pink'
                    },
                    {
                        title: 'Dựng giao diện HTML – CSS từ bản thiết kế',
                        content: `
                            <ul>
                                <li>Giới thiệu các thành phần cơ bản trong Photoshop</li>
                                <li>Hướng dẫn dựng giao diện với HTML – CSS</li>
                            </ul>
                       `,
                        class: 'pp-pink'
                    }
                ],
                phpBasic: [
                    {
                        title: 'Giới thiệu PHP và cấu trúc điều khiển',
                        content: `<ul><li>Giới thiệu về PHP, cài đặt môi trường webserver</li> <li>Làm việc với biến, hằng, kiểu dữ liệu, toán tử, cấu trúc điều khiển rẽ nhánh</li></ul>`,
                        class: 'pp-blue'
                    },
                    {
                        title: 'Lệnh lặp và Mảng',
                        content: `
                            <ul><li>Lệnh lặp: for, foreach, while, do..while</li> <li>Giới thiệu mảng trong php</li> <li>Phân loại mảng</li> <li>Làm việc với mảng 1 chiều</li></ul>
                        `,
                        class: 'pp-blue'
                    },
                    {
                        title: 'Mảng (tiếp)',
                        content: `
                        <ul><li>Làm việc với mảng nhiều chiều</li> <li>Các hàm làm việc với mảng</li> <li>Các phương thức truyền dữ liệu: POST, GET, REQUEST</li></ul>
                       `,
                        class: 'pp-green'
                    },
                    {
                        title: 'Hàm',
                        content: `
                            <ul><li>Cách viết hàm</li> <li>Truyền tham số cho hàm</li> <li>Phạm vi của biến</li></ul>
                       `,
                        class: 'pp-green'
                    },
                    {
                        title: 'Quản lý phiên làm việc',
                        content: `
                            <ul><li>$_SESSION</li> <li>$_COOKIE</li> <li>$_SERVER</li></ul>
                       `,
                        class: 'pp-orange'
                    },
                    {
                        title: 'Thao tác với File',
                        content: `
                            <ul><li>Include – Require</li> <li>Đọc ghi file text</li> <li>Upload file (file upload, multi file upload)</li></ul>
                       `,
                        class: 'pp-orange'
                    },
                    {
                        title: 'Hệ quản trị CSQL MySQL',
                        content: `
                            <ul><li>Hệ quản trị CSDL MySQL</li> <li>Tổng quan hệ quản trị CSDL MySQL</li> <li>Định nghĩa Bảng, các trường và kiểu dữ liệu trong bảng</li> <li>Cách phân tích và tạo bảng lưu trữ dữ liệu cho website từ yêu cầu chức năng</li> <li>Tạo CSDL và bảng trong MySQL</li> <li>Tổng quan về ngôn ngữ truy vấn SQL: câu lệnh thêm, sửa, xóa dữ liệu</li></ul>
                       `,
                        class: 'pp-yellow'
                    },
                    {
                        title: 'Lệnh truy vấn dữ liệu',
                        content: `
                            <ul><li>Cấu trúc lệnh SELECT</li> <li>Mệnh đề WHERE và các toán tử</li> <li>Mệnh đề GROUP BY</li> <li>Các mệnh đề khác: ORDER BY, LIMIT, OFFSET</li> <li>Một số hàm cơ bản trong MySQL</li></ul>
                       `,
                        class: 'pp-yellow'
                    },
                    {
                        title: 'PHP tương tác MySQL P1',
                        content: `
                            <ul><li>Kết nối tới CSDL MySQL từ website sử dụng ngôn ngữ PHP</li> <li>Thực hiện thao tác thêm, sửa, xóa dữ liệu</li> <li>Thực hiện thao tác truy vấn dữ liệu phục vụ chức năng hiển thị dữ liệu</li></ul>
                       `,
                        class: 'pp-red'
                    },
                    {
                        title: 'PHP tương tác MySQL P2',
                        content: `
                            <ul><li>Tìm kiếm và phân trang</li> <li>Phân tích và thiết kế website bán hàng
                            <ul><li>Phân tích và thiết kế giao diện</li> <li>Phân tích và thiết kế CSDL</li></ul></li></ul>
                       `,
                        class: 'pp-red'
                    },
                    {
                        title: 'PHP tương tác MySQL P3',
                        content: `
                            <ul><li>Tổ chức code cho ứng dụng website bán hàng</li> <li>Ôn tập</li></ul>
                       `,
                        class: 'pp-pink'
                    }
                ],
                phpAdvanced: [
                    {
                        title: 'Lập trình hướng đối tượng',
                        content: `
                            <ul><li>Giới thiệu lập trình hướng đối tượng</li><li>Class và Object</li><li>Thuộc tính và phương thức</li><li>Hàm khởi tạo</li></ul>
                        `,
                        class: 'pp-blue'
                    },
                    {
                        title: 'Lập trình hướng đối tượng (tiếp)',
                        content: `
                            <ul><li>Extends, abstract, interface</li> <li>Namespace, autoload</li></ul>
                        `,
                        class: 'pp-blue'
                    },
                    {
                        title: 'Kiến trúc MVC',
                        content: `
                            <ul><li>Kiến trúc MVC và tổ chức code</li> <li>Giới thiệu XML</li> <li>JSON</li> <li>Ajax trong PHP</li></ul>
                       `,
                        class: 'pp-green'
                    },
                    {
                        title: 'Tối ưu website',
                        content: `
                            <ul><li>Tăng tốc website bằng kỹ thuật tạo cache</li> <li>Tối ưu tốc độ truy vấn csdl</li> <li>Các kỹ thuật tối ưu code để tăng tốc website và tăng cường bảo mật</li> <li>Giới thiệu các vấn đề bảo mật website: SQL injection, htaccesss, rewrite url, ddos...</li> <li>Triển khai ứng dụng web lên host dịch vụ</li></ul>
                       `,
                        class: 'pp-green'
                    },
                    {
                        title: 'Laravel Framework P1',
                        content: `
                            <ul><li>Giới thiệu chung
                            <ul><li>PHP Framework &amp; kiến trúc hệ thống</li> <li>Cài đặt môi trường sử dụng</li> <li>Làm quen với Composer và Artisan</li> <li>Giới thiệu &amp; Cài đặt Laravel</li> <li>Cấu trúc thư mục Laravel3</li> <li>Cấu hình cơ bản của Laravel</li> <li>Routing</li> <li>Controller, những nguyên tắc và quy ước trong controller</li> <li>View</li></ul></li></ul>
                       `,
                        class: 'pp-orange'
                    },
                    {
                        title: 'Laravel Framework P2',
                        content: `
                            <ul><li>ORM – Query builder</li> <li>Làm việc với layout và template
                            <ul><li>Blade template và các cú pháp lệnh</li> <li>Layout</li> <li>Form và các điều khiển sử dụng thư viện</li> <li>CSRF Protection</li></ul></li></ul>
                       `,
                        class: 'pp-orange'
                    },
                    {
                        title: 'Laravel Framework P3',
                        content: `
                            <ul><li>Kiểm tra tính hợp của dữ liệu trong controller</li> <li>Sử dụng Session trong Laravel</li> <li>Middleware</li></ul>
                       `,
                        class: 'pp-yellow'
                    },
                    {
                        title: 'Laravel Framework P4',
                        content: `
                            <ul><li>Laravel Framework P4</li> <li>Phân quyền với Gate</li> <li>Ôn tập</li></ul>
                       `,
                        class: 'pp-yellow'
                    }
                ],
                endProject: [
                    {
                        title: 'Buổi 1',
                        content: `
                            <ul><li>Lựa chọn đề tài, đánh giá hàm lượng kiến thức và kỹ thuật áp dụng đối với đề tài</li> <li>Hướng dẫn phân tích chức năng, xây dựng sitemap và thiết kế giao diện bằng photoshop</li> <li>Hướng dẫn Học viên viết tài liệu, tất cả các phần việc đều phải viết tài liệu, các buổi học tiếp theo sẽ cập nhật vào tài liệu</li></ul>
                        `,
                        class: 'pp-blue'
                    },
                    {
                        title: 'Buổi 2',
                        content: `
                            <ul><li>Buổi 2</li> <li>Phân tích và thiết kế CSDL, khởi tạo dữ liệu demo</li></ul>
                        `,
                        class: 'pp-blue'
                    },
                    {
                        title: 'Buổi 3',
                        content: `
                            <ul><li>Đánh giá sự tương xứng CSDL với chức năng và giao diện</li> <li>Tổ chức code và lập trình phần backend</li> <li>Các chức năng: Đăng nhập, Đăng ký, Đổi password</li></ul>
                       `,
                        class: 'pp-green'
                    },
                    {
                        title: 'Buổi 4',
                        content: `
                            <ul><li>Viết code các trang quản trị: Quản lý tài khoản, Quản lý phân quyền</li> <li>Viết code trang chức năng chuyên dụng</li> <li>Đối với web tin tức thì quản lý bài viết, phân chia nhóm bài viết...</li> <li>Đối với web bán hàng thì quản lý và phân loại sản phẩm....</li></ul>
                       `,
                        class: 'pp-green'
                    },
                    {
                        title: 'Buổi 5',
                        content: `
                            <ul><li>Viết code các trang phần FRONT END (Tùy theo đề tài cụ thể)</li> <li>Hoàn thiện tài liệu </li> <li>Quyển báo cáo đồ án: Được trích rút từ tài liệu phân tích thiết kế làm từ buổi học 1</li> <li>Slide bảo vệ đồ án</li></ul>
                       `,
                        class: 'pp-orange'
                    },
                    {
                        title: 'Buổi 6',
                        content: `
                            <ul><li>Đánh giá lại toàn bộ sản phẩm (là buổi bảo vệ thử)</li></ul>
                       `,
                        class: 'pp-orange'
                    },
                    {
                        title: 'Buổi 7: Bảo vệ đồ án',
                        content: '',
                        class: 'pp-yellow'
                    }
                ],
            }
        },
        methods: {
            getSP() {
                $.get(this.base + 'admin/api/apiSanPhamLapTrinhWeb')
                    .then(res => {
                        this.sanphams = (res.sort((x, y) => x.id-y.id));
                    })
                    .catch(err => {
                        console.error(err);
                    });
            }
        },

        created() {
            this.getSP();
        },
        mounted() {

        },
        updated(){
            $('.sp-slider').slick({
                dots: true,
                // infinite: false,
                // speed: 300,
                slidesToShow: 4,
                autoplay: false,
                slidesToScroll: 4,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            prevArrow: false,
                            nextArrow: false
                        }
                    },
                    {
                        breakpoint: 321,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            prevArrow: false,
                            nextArrow: false
                        }
                    }
                ]
            });
        }
    });
})(AOS, $);
